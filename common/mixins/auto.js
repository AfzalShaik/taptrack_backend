var server = require('../../server/server');
module.exports = function (Model, options) {
    var modelName = server.models.Transactions;     //Get the Model Name from Model Instance
    Model.observe('before save', function (ctx, next) {
        if (!ctx.isNewInstance) {
            next();
        } else {
            Model.getDataSource().connector.connect(function (err, db) {
                // console.log('dddddddddddddddddd ', err, db);
                // var collection = db.collection('transaction');
                var modelName = server.models.Transactions;
                // modelName.find({name: modelName}, [['id', 'asc']], {$inc: {value: 1}}, { new: true, upsert: true }, function (err, rec) {
                modelName.find({ order: 'transactionId DESC', limit: 1 }, function (err, rec) {
                    
                    if (rec.length == 0) {
                        ctx.instance['transactionId'] = 1;
                    } else if(rec.length > 0) {
                        if (ctx.instance) {
                            ctx.instance['transactionId'] = parseInt(rec[0].transactionId) + 1;
                        } else {
                            ctx.data['transactionId'] = parseInt(rec[0].transactionId) + 1;
                        }
                    } else {
                        transactionErr('Error while creating transaction', next);
                    }


                    // ctx.instance['transactionId'] = parseInt(rec[0].transactionId) + 1;
                    // console.log('rrrrrrrrrrrrrrrrrrrrrrrrr ', ctx.instance, rec[0].transactionId);
                    // if (err) {
                    //     console.err(err);
                    //     next();
                    // } else {
                    //     getPrimaryKeyFromModel(Model, function(primaryKey){
                    //         if (ctx.instance) {
                    //             ctx.instance[primaryKey]   = rec.value.value;
                    //         } else {
                    //             ctx.data[primaryKey]  = rec.value.value;
                    //         }
                    next();
                    //     });
                    // }

                });
            });
        }
    });
}



//Get the Primary Key from Model
var getPrimaryKeyFromModel = function (Model, cb) {
    var properties = Model.definition.rawProperties;
    Object.keys(properties).forEach(function (key) {
        if (properties[key].id === true) {
            cb(key);
        }
    });
}

var transactionErr = function (error, next) {
    var transErr = new Error(error);
    transErr.statusCode = 500;
    transErr.requestStatus = false;
    next(transErr, null);
  };