'use strict';

var server = require('../../server/server')
var async = require('async')
var winston = require('../../winstonsockets');
// exporting function to use it in another modules if requires
module.exports = function (UserType) {
    /** poendinggetusersCount method : will return list of pending users count for given companyId 
     * @constructor
     * cb - callback will return count
    */
    UserType.poendinggetusersCount = function (input, cb) {
        server.models.Users.count({ companyId: input.companyId, userApproved: 0 }, function (err, resp) {
            if (err) {
                winston.GenerateLog(0, 'poendinggetusersCount : Error while querying poendinggetusersCount  : ' + err + ' ');
            } else {
                winston.GenerateLog(2, 'poendinggetusersCount : list of poendinggetusersCount  : ' + resp + ' ');
                cb(null, resp)
            }
        });
    }
    /** getusersCount method : will give you the count of users which are approved and for given companyId */
    UserType.getusersCount = function (input, cb) {
        server.models.Users.count({ companyId: input.companyId, userApproved: 1 }, cb);
    };
    /** getusers method : will return list of users for given companyId
     * @constructor
     * cb - callback to return response
     */
    UserType.getusers = function (input, cb) {
        var orderBy = 'userStatus DESC';
        server.models.Users.find(
            {
                where: { companyId: input.companyId, userApproved: 1 },
                include: [{
                    relation: 'UserType',
                }],
                skip: input.skip,
                limit: input.limit,
                'order': 'userStatus DESC'
            }, function (err, out) {
                if (err) {
                    winston.GenerateLog(0, 'Errro while fetching records from user model in getusers method : ' + err + ' ');
                    cb(err, null)
                } else {
                    winston.GenerateLog(2, 'getUsers Method : list of users for given companyId  : ' + out.length + ' ');
                    cb(null, out)
                }

            });
    };
    /** getpendingusers method : will return the list of pending users for given company
     * @constructor
     * cb- callback will return the final output
     */
    UserType.getpendingusers = function (input, cb) {
        /** find method will return the list of users for given companyId and userApproved will be 0 */
        server.models.Users.find(
            {
                where: { and: [{ companyId: input.companyId }, { userApproved: 0 }] },
                skip: input.skip,
                limit: input.limit
            }, cb);
    };
    /** updateUsersData method : method will update the record if exist or it inserts the new record
     * @constructor
     * cb - callback will return the final output
     */
    UserType.updateUsersData = function (userData, cb) {
        userData = JSON.parse(JSON.stringify(userData))
        /** async.each method will check each object */
        async.each(userData, function (user, subCb) {
            /** upsert method will check the record if exist it will update the record otherwise it will insert new record */
            server.models.Users.upsert(JSON.parse(JSON.stringify(user)), subCb);
        }, function (err, result) {
            if (err) cb(null, 'Success');
            else cb(null, 'Success')
        })
    };
    var getscarchusers2 = function (input, cb) {
        var filter = {
            where: {

            }
        }
        if (input.searchQuery.username.value) {
            if (input.searchQuery.username.type == 'startsWith') {
                filter.where[input.searchQuery.username.field] = { like: input.searchQuery.username.value + '%' }
            } else if (input.searchQuery.username.type == 'endsWith') {
                filter.where[input.searchQuery.username.field] = { like: '%' + input.searchQuery.username.value }
            } else if (input.searchQuery.username.type == 'exactly') {
                filter.where[input.searchQuery.username.field] = { like: input.searchQuery.username.value }
            } else {
                filter.where[input.searchQuery.username.field] = { like: '%' + input.searchQuery.username.value + '%' }
            }
        }
        if (input.searchQuery.userFirstName.value) {
            if (input.searchQuery.userFirstName.type == 'startsWith') {
                filter.where[input.searchQuery.userFirstName.field] = { like: input.searchQuery.userFirstName.value + '%' }
            } else if (input.searchQuery.userFirstName.type == 'endsWith') {
                filter.where[input.searchQuery.userFirstName.field] = { like: '%' + input.searchQuery.userFirstName.value }
            } else if (input.searchQuery.userFirstName.type == 'exactly') {
                filter.where[input.searchQuery.userFirstName.field] = { like: input.searchQuery.userFirstName.value }
            } else {
                filter.where[input.searchQuery.userFirstName.field] = { like: '%' + input.searchQuery.userFirstName.value + '%' }
            }
        }
        if (input.searchQuery.userLastName.value) {
            if (input.searchQuery.userLastName.type == 'startsWith') {
                filter.where[input.searchQuery.userLastName.field] = { like: input.searchQuery.userLastName.value + '%' }
            } else if (input.searchQuery.userLastName.type == 'endsWith') {
                filter.where[input.searchQuery.userLastName.field] = { like: '%' + input.searchQuery.userLastName.value }
            } else if (input.searchQuery.userLastName.type == 'exactly') {
                filter.where[input.searchQuery.userLastName.field] = { like: input.searchQuery.userLastName.value }
            } else {
                filter.where[input.searchQuery.userLastName.field] = { like: '%' + input.searchQuery.userLastName.value + '%' }
            }
        }
        server.models.Users.find(filter, function (err, response) {
            // server.models.Student.find({ where: { firstName: { like: text } } }, function (err, response) {
            if (err) cb(err);
            else {
                cb(null, response)
            }
        })
    }
    var getscarchuserspending2 = function (input, cb) {
        var filter = {
            where: {

            }
        }
        if (input.searchQuery.username.value) {
            if (input.searchQuery.username.type == 'startsWith') {
                filter.where[input.searchQuery.username.field] = { like: input.searchQuery.username.value + '%' }
            } else if (input.searchQuery.username.type == 'endsWith') {
                filter.where[input.searchQuery.username.field] = { like: '%' + input.searchQuery.username.value }
            } else if (input.searchQuery.username.type == 'exactly') {
                filter.where[input.searchQuery.username.field] = { like: input.searchQuery.username.value }
            } else {
                filter.where[input.searchQuery.username.field] = { like: '%' + input.searchQuery.username.value + '%' }
            }
        }
        if (input.searchQuery.userFirstName.value) {
            if (input.searchQuery.userFirstName.type == 'startsWith') {
                filter.where[input.searchQuery.userFirstName.field] = { like: input.searchQuery.userFirstName.value + '%' }
            } else if (input.searchQuery.userFirstName.type == 'endsWith') {
                filter.where[input.searchQuery.userFirstName.field] = { like: '%' + input.searchQuery.userFirstName.value }
            } else if (input.searchQuery.userFirstName.type == 'exactly') {
                filter.where[input.searchQuery.userFirstName.field] = { like: input.searchQuery.userFirstName.value }
            } else {
                filter.where[input.searchQuery.userFirstName.field] = { like: '%' + input.searchQuery.userFirstName.value + '%' }
            }
        }
        if (input.searchQuery.userLastName.value) {
            if (input.searchQuery.userLastName.type == 'startsWith') {
                filter.where[input.searchQuery.userLastName.field] = { like: input.searchQuery.userLastName.value + '%' }
            } else if (input.searchQuery.userLastName.type == 'endsWith') {
                filter.where[input.searchQuery.userLastName.field] = { like: '%' + input.searchQuery.userLastName.value }
            } else if (input.searchQuery.userLastName.type == 'exactly') {
                filter.where[input.searchQuery.userLastName.field] = { like: input.searchQuery.userLastName.value }
            } else {
                filter.where[input.searchQuery.userLastName.field] = { like: '%' + input.searchQuery.userLastName.value + '%' }
            }
        }
        server.models.Users.find(filter, function (err, response) {
            if (err) cb(err);
            else {
                cb(null, response)
            }
        })
    }
    /** getscarchusers method : will give the list of users for given input.
     * @constructor
     * @param {object} filter - object wich forms for search
     * cb - callback will return the marched results for given input
     */
    UserType.getscarchusers = function (obj, cb) {
        getscarchusers2(obj, function (err, sIds) {
            if (err) cb(err);
            else {
                var filter = {
                    where: {
                        and: [{
                            id: {
                                inq: sIds.map(a => a.id)
                            }
                        },
                        {
                            companyId: obj.companyId
                        },
                        {
                            userApproved: 1
                        }
                        ]
                    },
                    skip: obj.skip,
                    limit: obj.limit,

                }
                server.models.Users.find(filter, cb)
            }
        })
    }
    /** getscarchuserspending method : return the list of pending users for given input
     * @constructor
     * @param {object} filter - query for search
     * cb - which handles the final output.
     */
    UserType.getscarchuserspending = function (obj, cb) {
        console.log(JSON.stringify(obj))
        getscarchuserspending2(obj, function (err, sIds) {
            if (err) cb(err);
            else {
                var filter = {
                    where: {
                        and: [{
                            id: {
                                inq: sIds.map(a => a.id)
                            }
                        },
                        {
                            companyId: obj.companyId
                        }, { userApproved: 0 }
                        ]
                    },
                    skip: obj.skip,
                    limit: obj.limit,

                }
                server.models.Users.find(filter, cb)
            }
        })
    }
    /** getusersCount method declaration */
    UserType.remoteMethod("getusersCount", {
        description: "get record count by companyId for pagination",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getusersCount",
            verb: "POST"
        }
    });
    /** poendinggetusersCount method declaration */
    UserType.remoteMethod("poendinggetusersCount", {
        description: "get record count by companyId for pagination",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/poendinggetusersCount",
            verb: "POST"
        }
    });
    /** getusers method declaration */
    UserType.remoteMethod("getusers", {
        description: "To getusers",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getusers",
            verb: "POST"
        }
    });
    /** getpendingusers method declaration  */
    UserType.remoteMethod("getpendingusers", {
        description: "To getpendingusers",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getpendingusers",
            verb: "POST"
        }
    });
    /** getscarchusers method declaration */
    UserType.remoteMethod("getscarchusers", {
        description: "To getscearcusers",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getscarchusers",
            verb: "POST"
        }
    });
    /** getscarchuserspending method declaration */
    UserType.remoteMethod("getscarchuserspending", {
        description: "To getscarchuserspending",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getscarchuserspending",
            verb: "POST"
        }
    });
    /** updateUsersData method declaration */
    UserType.remoteMethod("updateUsersData", {
        description: "updateUsersData update",
        returns: {
            arg: 'data',
            type: 'string'
        },
        accepts: [
            {
                arg: "data",
                type: "array",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/updateUsersData",
            verb: "POST"
        }
    });
    UserType.GetTrancationsReports = function (input, cb) {

        var transactions = server.models.Transactions;
        transactions.find({
            where: {
                sender: input.UserId,
                companyId: input.companyId,
                order: 'transSucceedTime DESC',
            },
            include: {
                relation: 'senderx', // include the owner object
                scope: { // further filter the owner object
                    include: [{ // include orders for the owner
                        relation: 'senderx',
                        scope: { // further filter the owner object
                            fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
                        } // only show two fields
                    }]
                }
            },
        }, function (err, out) {
            cb(null, out)
        })
    }


    UserType.remoteMethod('GetTrancationsReports', {
        description: 'GetTranscations Reports',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/GetTrancationsReports',
            verb: 'POST',
        },
    });

};
