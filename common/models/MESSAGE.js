'use strict';
var async = require('async');

var winston = require('../../winstonsockets');
// exporting function to use it in another modules if requires
module.exports = function (Message) {
    /** before save method */
    Message.observe('before save', function (ctx, next) {
        /** validate if given input is new object */
        if (ctx.isNewInstance) {
            //ctx.instance.superAdminId = ctx.instance.userId;
            // ctx.instance.userId = ctx.instance.userId;
            /** adding status as 1 and statusName as enabled with addTime and statusMessage */
            ctx.instance.status = 1;
            ctx.instance.statusName = 'Enabled';
            ctx.instance.addTime = new Date();
            ctx.instance.statusMessage = 'Success';

            next();

        } else {
            /** for update */
            ctx.data.addTime = new Date();
            if (ctx.data.status == 0) {
                /** updates statusName as disabled if status is 0 */
                ctx.data.statusName = 'Disabled';
            }
            else {
                ctx.data.statusName = 'Enabled';
            }
            ctx.data.statusMessage = 'Success';
            next();
        }
    });

    /** getCompanyMessages method 
         *  this method will return messages
         * @constructor
         * @param {function} callBc - deals with response
         */
    Message.getCompanyMessages = function (input, cb) {
        /** check if companyId is exist */
        if (input.companyId != null) {
            /** find method will fetch all the records which matches given companyId and status */
            Message.find({
                'where': { 'and': [{ 'companyId': input.companyId }, { 'status': input.status }] }, skip: input.skip,
                limit: input.limit, order: "addTime DESC"
            }, function (companyErr, companyOut) {
                /** callback will return output */
                winston.GenerateLog(2, ' getCompanyMessages Method : The list of messages for company with Id  : ' + input.companyId + " are: " + companyOut.length + ' ');
                cb(null, companyOut)
            });

        } else {
            /** callback throws an error if companyId does not exist */
            cb('CompanyId is Mandotory', null)
        }
    }
    /** getCompanyMessages method declaration */
    Message.remoteMethod('getCompanyMessages', {
        description: 'getCompanyMessages',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getCompanyMessages',
            verb: 'POST',
        },
    });

    /** getCompanyMessagesCount method : 
     * @constructor
     * @param {function} callBc - deals with response
     */

    Message.getCompanyMessagesCount = function (input, cb) {

        if (input.companyId) {
            // Message.count({ 'where':{'and':[{ 'companyId': input.companyId },{'status':input.status}]}},cb);
            /** count method will give count for given companyId and status 1 */
            Message.count({ companyId: input.companyId }, { 'status': input.status }, cb);
        } else {
            /** error will get returned. */
            cb(new Error("required feilds missed"));
        }
    };

    /** getCompanyMessagesCount method declaration */

    Message.remoteMethod("getCompanyMessagesCount", {
        description: "getCompanyMessages by companyId for pagination",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getCompanyMessagesCount",
            verb: "POST"
        }
    });


    // Message.updateMessageStatus = function (input, cb) {
    //     if(input != null)
    //     {
    //         let data = input;
    //         updateAllSync(0);
    //         function updateAllSync(i) {
    //             if (i < data.length) {
    //                 const filter = {
    //                     where: { messageId: data[i].messageId },
    //                 };

    //                 Message.findOne(filter, (err, newdata) => {

    //                     if (!err) {
    //                         newdata.updateAttributes({"status":data[i].status}, function (err, updated) {
    //                             if (!err) {
    //                                 if (data.length === i) {

    //                                     updateAllSync(i+1);
    //                                 }
    //                             } else {


    //                                 cb(err, null);
    //                             }
    //                         })
    //                     } else {
    //                         cb(err, null);
    //                     }
    //                 });
    //             }else{
    //                 cb(null,i); // finished updating all docs sync
    //             }
    //         }
    //     }
    // }

    /** updateMessageStatus method
     *  this method will update message status
     * @constructor
     * @param {array} successobj - stores success objects
     * @param {array} failuerobj - failuerobj failed objects
     * @param {function} callBc - deals with response
     */
    Message.updateMessageStatus = function (input, cb) {
        if (input.length > 0) {
            var successobj = [];
            var failuerobj = [];
            /** async.map will perform async of each object and update the status in subment method */
            async.map(input, submenu, function (asyncErr, asyncOut) {
                /** final output will return here */

                cb(null, { "successobj": successobj, "failuerobj": failuerobj })
            });
            function submenu(objsub, callBack) {
                /** findOne method will fetch one record with messageId */
                Message.findOne({ 'where': { 'messageId': objsub.messageId } }, function (companyErr, companyOut) {


                    if (companyOut != null) {
                        /** status will update here */
                        companyOut.updateAttributes({ "status": objsub.status }, function (err, updated) {
                            if (err) {

                            }
                            /** all success objects will push here with status updated */
                            successobj.push({ "id": objsub, "status": "updated" })
                            callBack(null, updated);
                        });
                    }
                    else {
                        callBack(null, 'Error');
                    }
                });

            };
        }
    }
    /** updateMessageStatus method declaration */
    Message.remoteMethod('updateMessageStatus', {
        description: 'updateMessageStatus',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'array',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/updateMessageStatus',
            verb: 'POST',
        },
    });


};
