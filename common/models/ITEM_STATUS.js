'use strict';


// exporting function to use it in another modules if requires
module.exports = function (ItemStatus) {
    //getItemStatusList method : will return itemstatus other than pending
    ItemStatus.getItemStatusList = function (input, cb) {
        if (input.type == 'filter') {
            // find method will return all records except pending status
            ItemStatus.find({ where: { itemStatusId: { neq: 5 } } }, function (statusErr, statusOut) {
                cb(null, statusOut);
            });
        }
        else {
            // will return all the statuses
            ItemStatus.find(function (statusErr, statusOut) {
                // pushing new object with itemStatusId All.
                var data =
                {
                    "itemStatusId": 'All',
                    "description": "All",
                    "shortDesc": "All"
                }
                
                statusOut.push(data);
                cb(null, statusOut);
            });
        }
    };

    //getItemStatusList method declaration
    ItemStatus.remoteMethod('getItemStatusList', {
        description: 'getItemStatusList',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: false,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getItemStatusList',
            verb: 'POST',
        },
    });

};
