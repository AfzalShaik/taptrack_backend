'use strict';


// exporting function to use it in another modules if requires
var async = require("async");
var server = require("../../server/server");
var winston = require('../../winstonsockets');
module.exports = function (ItemType) {
  // validating regexformat is unique for itemtype table.
  ItemType.validatesUniquenessOf('regexFormat')
  // before save method
  ItemType.observe('before save', function (ctx, next) {
    // validate if it is a new instance
    if (ctx.isNewInstance) {
      // creates new objects if it is a new object
      winston.GenerateLog(2, 'Input for ItemType is ' + ctx.instance.toString());
      next();
    }
    else {
      // updates if already object(id) already exist
      next();
    }

  });
  // getItemType method : will fetch all itemtypes for given companyId
  ItemType.getItemType = function (input, cb) {
    // find method will fetch  all records for given companyId
    ItemType.find({ 'where': { 'companyId': input.companyId } }, function (err, out) {
      if (err) {
        // callback will return error 
        winston.GenerateLog(0, 'getItemType Method : Error while querying ItemType' + err + ' ');
        cb(err, null)
      } else {
        // callback will return output.
        winston.GenerateLog(2, 'getItemType Method : List of Items for given company are ' + out.length + ' ');
        cb(null, out)
      }

    })
  }
  // declaration of getItemType method
  ItemType.remoteMethod('getItemType', {
    description: 'To getItemType',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getItemType',
      verb: 'post',
    },
  });


  ItemType.mailRoomReport = function (input, cb) {
    var int = {};
    if (input.companyId) {
      if (input.fromDate && input.toDate) {
        int = {
          or: [
            {
              addItem: {
                between: [input.fromDate, input.toDate]
              },
              companyId: input.companyId,

            }
          ]
        }
        ItemType.find({
          'where': { companyId: input.companyId },
          include: {
            relation: 'itemTypeRel', // include the owner object
            scope: { // further filter the owner object
              where: int,
              fields: ['itemId'], // only show two fields
            }
          }
        }, function (itemTypeErr, itemTypeOut) {
          var finalArr = []
          if (itemTypeOut.length > 0) {
            async.each(itemTypeOut, function (object, callBC) {
              var itemTypeObj = {} = JSON.stringify(object);
              var itemTypeObject = JSON.parse(itemTypeObj);
              if (itemTypeObject['itemTypeRel'].length > 0) {
                finalArr.push({
                  "itemType": itemTypeObject.itemType,
                  "itemsList": itemTypeObject['itemTypeRel'].length
                })
              }
              callBC();
            });
            cb(null, finalArr);
          } else {
            cb(null, []);
          }

        });
      } else {
        cb('fromDate and toDate are mandotory', null)
      }
    } else {
      cb('CompanyId is mandotory', null)
    }
  }

  // Mailroom report : will return report for given companyId
  var xyz = {}
  ItemType.mailRoomReport1 = function (input, cb) {
    // checking companyId is present
    if (input.companyId) {
      // assigning input to global object to use it for other functions
      xyz = input;
      // below methood will return all itemtypes for given companyId
      ItemType.find({ 'where': { 'companyId': input.companyId } }, function (itemTypeErr, itemTypeOut) {
        if (itemTypeErr) {
          /*  Error while fetching ItemTypes in mailroomreport */
          winston.GenerateLog(0, 'mailRoomReport Method: Error while fetching ItemTypes in mailroomreport ' + itemTypeErr + ' ');
          cb(itemTypeErr, null)
        }
        if (itemTypeOut.length > 0) {
          /* if itemtypes are exist it will come here */
          winston.GenerateLog(2, 'mailRoomReport Method : List of ItemTypes for  companyId' + input.companyId + ' are ' + itemTypeOut.length + ' ');
          /* async map will call getItems method  */
          async.map(itemTypeOut, getItems, function (err, out) {
            if (out.length > 0) {
              /*  callback will return mailroomreports if exist*/
              winston.GenerateLog(2, 'mailRoomReport Method : List of Mailroomreports are ' + out.length + ' ');
              cb(null, cleanArray(out));
            } else {
              if (err) {
                /** callback will return error */
                winston.GenerateLog(0, 'mailRoomReport Method : Error while retrieving Mailroomreports ' + err + ' ');
                cb('Error while retrieving Mailroomreports', null)
              } else {
                /** callback will return null if no records exists */
                cb(null, null)
              }

            }
          })
        } else {
          /* callback will return null if no records exist */
          cb(null, null)
        }
      })
    }
    // it will throw an error if companyId does not exist
    else {
      cb('CompanyId is mandotory', null)
    }

  }
  // cleanArray function will remove nulls from an array.
  function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
    }
    return newArray;
  }

  /** getItemsBetweenDates method will return the records if addTime exist in between the dates */
  function getItemsBetweenDates(input, dCB) {

    var Difference_In_Time;
    var Difference_In_Days
    var date1 = new Date(xyz.fromDate);
    var date2 = new Date(xyz.toDate);
    var addItem = new Date(input.addItem);
    /** find the difference */
    var firstDiff = date1.getTime() - addItem.getTime();
    var secondDiff = date2.getTime() - addItem.getTime();

    if (firstDiff <= 0 && secondDiff >= 0) {
      dCB(null, input)
    } else {
      dCB(null, null)
    }

  }
  /** getItems method will fetch matched items for given itemtypes */
  function getItems(obj, callBC) {
    var items = server.models.Item;
    /** find method will return matched records for given companyId and itemTypeId */
    items.find({ 'where': { 'and': [{ 'itemTypeId': obj.itemTypeId }, { 'companyId': obj.companyId }] } }, function (itemsErr, itemsOut) {
      if (itemsOut.length > 0) {
        winston.GenerateLog(2, 'getItemsBetweenDates Method : List of Items for given itemTypeId ' + obj.itemTypeId + ' are ' + itemsOut.length + ' ');
        /** getItemsBetweenDates method will return the records if addTime exist in between the dates */
        async.map(itemsOut, getItemsBetweenDates, function (err, output1) {
          var out = cleanArray(output1).length;
          var output = {};
          output = {
            'itemType': obj.itemType,
            'itemsList': out
          }
          /** callback will return output */
          callBC(null, output)
        })
      } else {
        // callback will return null if records not exist
        if (itemsErr) {
          winston.GenerateLog(0, 'getItemsBetweenDates Method : Error while querying itemTypeId ' + obj.itemTypeId + itemsErr + ' ');
        }

        callBC(null, null)
      }


    })
  }

  //mailRoomReport method declaration
  ItemType.remoteMethod('mailRoomReport', {
    description: 'mailRoomReport',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/mailRoomReport',
      verb: 'post',
    },
  });

  ItemType.itemTypeReport = function (input, cb) {
    var int = {};
    if (input.companyId) {
      int = {
        companyId: input.companyId,

      }
      ItemType.find({
        'where': { companyId: input.companyId },
        include: {
          relation: 'itemTypeRel', // include the owner object
          scope: { // further filter the owner object
            where: int,
            fields: ['itemId', 'itemStatusId'], // only show two fields
          }
        },
        skip: input.skip,
        limit: input.limit,
      }, function (itemTypeErr, itemTypeOut) {
        var totalItems = [];
        var closedItems = [];
        var trackedItems = [];
        var finalArr = [];
        if (itemTypeOut.length > 0) {
          async.each(itemTypeOut, function (object, callBC) {
            var itemTypeObj = {} = JSON.stringify(object);
            var itemTypeObject = JSON.parse(itemTypeObj);
            totalItems = itemTypeObject['itemTypeRel'].length;
            if (itemTypeObject['itemTypeRel'].length > 0) {
              async.each(itemTypeObject['itemTypeRel'], function (obj, CBC) {
                if (obj.itemStatusId == 6) {
                  closedItems.push(obj)
                }
                /** return the items which has status 2 or 5 (tracking records) */
                if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                  trackedItems.push(obj)
                }

                CBC();
              });
            }
            var finalObj = {} = itemTypeObject;
            finalObj['totalItems'] = totalItems;
            finalObj['closedItems'] = closedItems.length;
            finalObj['trackedItems'] = trackedItems.length;
            delete finalObj['itemTypeRel']
            totalItems = [];
            closedItems = [];
            trackedItems = [];
            finalArr.push(finalObj)
            callBC();
          });
          cb(null, finalArr);
        } else {
          cb(null, []);
        }

      });
    } else {
      cb('CompanyId is mandotory', null)
    }
  }

  // Item Type Report

  ItemType.itemTypeReport1 = function (input, cb) {
    /** find method will return itemtypes for given companyId with given limit */
    ItemType.find({
      'where': { 'companyId': input.companyId }, skip: input.skip,
      limit: input.limit,
    }, function (typeErr, typeOut) {
      if (typeOut.length > 0) {
        /** async.map with getItemDetails method */
        async.map(typeOut, getItemDetails, function (itemErr, itemOut) {
          winston.GenerateLog(2, 'itemTypeReport Method : List of itemTypeReports ' + itemOut.length + ' ');
          cb(null, itemOut)
        })
      } else {
        /** callback return error if exist */
        if (typeErr) {
          winston.GenerateLog(0, 'itemTypeReport Method : Error while querying itemTypeReport ' + typeErr + ' ');
          cb('Error while querying itemTypeReport', null)
        } else {
          /** callback will return null if no records found */
          cb(null, null)
        }

      }

    })
  }
  /** getItemDetails method  */
  function getItemDetails(obj, callBC) {
    var items = server.models.Item;
    /** find method will return items for given itemTypeId */
    items.find({ 'where': { 'itemTypeId': obj.itemTypeId } }, function (itemErr, itemOut) {
      var closedArr = [];
      var trackedArr = [];
      if (itemOut.length > 0) {
        winston.GenerateLog(2, 'getItemDetails Method : List of Items for ItemTypeId ' + obj.itemTypeId + ' are ' + itemOut.length + ' ');
        /** getParams method will return items with statusId 6 and 2 or 5 */
        itemOut.find(getParams);

        function getParams(prm) {
          /** return the records if itemstatusid = 6. that is closed */
          if (prm.itemStatusId == 6) {
            closedArr.push(prm)
            return closedArr;
          }
          /** return the items which has status 2 or 5 (tracking records) */
          if (prm.itemStatusId == 2 || prm.itemStatusId == 5) {
            trackedArr.push(prm)
            return trackedArr;
          }
        }
      }


      var objj = {};
      objj = obj;
      /** totalItems */
      objj['totalItems'] = itemOut.length;
      /** closed Items */
      objj['closedItems'] = closedArr.length;
      /** tracking items */
      objj['trackedItems'] = trackedArr.length;
      winston.GenerateLog(2, 'getItemDetails Method : closedItems for ItemTypeId ' + obj.itemTypeId + ' are ' + closedArr.length + ' ');
      winston.GenerateLog(2, 'getItemDetails Method : trackedItems for ItemTypeId ' + obj.itemTypeId + ' are ' + trackedArr.length + ' ');
      callBC(null, objj)

    })
  }
  /** getusersCount method */
  ItemType.getusersCount = function (input, cb) {
    /** count method will return list of users for given companyId */
    server.models.Users.count({ companyId: input.companyId }, function (err, resp) {
      if (err) {
        /** callback will return error */
        winston.GenerateLog(0, 'getusersCount Method : Error while querying getusersCount  : ' + err + ' ');
        cb(err, null)
      } else {
        /** callback will return output */
        winston.GenerateLog(2, 'getusersCount Method : list of getusersCount  : ' + resp + ' ');
        cb(null, resp)
      }
    });
  };
  /** itemTypeCount method: */
  ItemType.itemTypeCount = function (input, cb) {
    /** model will return count from itemtype model for given companyId */
    server.models.ItemType.count({ companyId: input.companyId }, function (err, resp) {
      if (err) {
        winston.GenerateLog(0, 'itemTypeCount Method : Error while querying itemTypeCount  : ' + err + ' ');
        /** callback will return error if exist */
        cb(err, null)
      } else {
        winston.GenerateLog(2, 'itemTypeCount Method : list of itemTypeCount  : ' + resp + ' ');
        /** callback will return output */
        cb(null, resp)
      }
    });
  };
  /** itemTypeCount method declaration */
  ItemType.remoteMethod("itemTypeCount", {
    description: "get record count by companyId for pagination",
    returns: {
      type: "object",
      root: true
    },
    accepts: [
      {
        arg: "data",
        type: "object",
        http: {
          source: "body"
        }
      }
    ],
    http: {
      path: "/itemTypeCount",
      verb: "POST"
    }
  });

  /** getusersCount method declaration */
  ItemType.remoteMethod("getusersCount", {
    description: "get record count by companyId for pagination",
    returns: {
      type: "object",
      root: true
    },
    accepts: [
      {
        arg: "data",
        type: "object",
        http: {
          source: "body"
        }
      }
    ],
    http: {
      path: "/getusersCount",
      verb: "POST"
    }
  });
  /** itemTypeReport method declaration */
  ItemType.remoteMethod('itemTypeReport', {
    description: 'itemTypeReport',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/itemTypeReport',
      verb: 'post',
    },
  });



};
