'use strict';
var async = require('async');
var server = require('../../server/server');
var winston = require('../../winstonsockets');
var moment = require('moment');

// exporting function to use it in another modules if requires
module.exports = function (Transactions) {

    Transactions.observe('before save', function (ctx, next) {
      if (ctx.isNewInstance) {
            //convert the Date format to required DB date format 
            // 12/9/2019, 1:26:52 AM - Newyork format 
            // 2019-12-09 00:27:34 - required format 
            var datetime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
            var dateStr = moment(datetime, 'MM/DD/YYYY, HH:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
            ctx.instance.transactionTime = dateStr.toString();
            ctx.instance.transSucceedTime = dateStr.toString();
            next();

        }else {
            // ctx.data['transSucceedTime'] = new Date();
            next();
        }
    });


    Transactions.observe('after save', function (ctx, next) {
        if (ctx.isNewInstance) {
            var items = server.models.Item;
            items.findById(ctx.instance.itemId, function (err, item) {
                if (err) {
                    next(new Error(err));

                } else {
                    // console.log('............................. ');
                    var objj;
                    if (ctx.instance.transComment == 'Item closed  by user') {

                        objj = { "transactionId": ctx.instance.transactionId, "currentOwner": "", "itemStatusId": 6 };
                    }
                    else if (ctx.instance.transComment == 'Item checked out by user') {

                        objj = { "transactionId": ctx.instance.transactionId, "currentOwner": ctx.instance.reciever, "itemStatusId": 2 };
                    }
                    else if (ctx.instance.transComment == 'Item Assigned to other by user') {

                        objj = { "transactionId": ctx.instance.transactionId, "currentOwner": ctx.instance.reciever, "itemStatusId": 2 };
                    }
                    // else if (ctx.instance.transStatusId == 3) {
                    //     objj = { "transactionId": ctx.instance.transactionId }

                    // }
                    else {
                        objj = {
                            "transactionId": ctx.instance.transactionId, "currentOwner": ctx.instance.reciever, "userId": ctx.instance.sender,
                            "updatedDate": new Date(),
                            "updatedBy": ctx.instance.sender
                        }
                    }

                    objj["updatedDate"] = new Date();
                    objj["updatedBy"] = ctx.instance.sender

                    item.updateAttributes(objj, function (err, count) {
                        // console.log('000000000000');
                        winston.GenerateLog(2, 'Successfully Item got upated with new transactionId ' + objj.transactionId);
                        if (err) {
                            winston.GenerateLog(0, 'Error while updating Item with new transactionId ' + objj.transactionId);
                            console.error(err);
                        }


                    })

                    //////////////////////////
                    Transactions.find({ "where": { "itemId": ctx.instance.itemId }, "order": "transactionTime DESC" }, function (err, item) {
                        if (err) {
                            next(new Error(err));
                        } else {


                            if (item.length > 1) {
                                item[1].updateAttributes({ "nextTransactionId": ctx.instance.transactionId, "deletedFlag": 1, "updatedDate": new Date(), "updatedBy": ctx.instance.sender, "userId": ctx.instance.sender }, function (err, count) {
                                    // console.log('11111/111111111111');
                                    winston.GenerateLog(2, 'Successfully Item got upated with new transactionId ' + ctx.instance.transactionId);
                                    if (err) {
                                        winston.GenerateLog(0, 'Error while updating Item with new transactionId ' + ctx.instance.transactionId);
                                        console.error(err);
                                    }


                                })

                            }
                            next();
                        }
                    });
                }
            });



        } else {
            next();
        }

    });


    function rollBack(itemId, transactionId, cb) {
        if (transactionId) {
            Transactions.destroyById(transactionId, function (transErr, transOut) {
                if (transErr) {
                    cb(transErr, null)
                } else {
                    var item = server.models.Item;
                    item.destroyById(itemId, function (itemErr, itemOut) {
                        if (itemErr) {
                            cb(itemErr, null)
                        } else {
                            cb(null, itemOut)
                        }
                    })
                }
            })
        }
        else {
            var item = server.models.Item;
            item.destroyById(itemId, function (itemErr, itemOut) {
                if (itemErr) {
                    cb(itemErr, null)
                } else {
                    cb(null, itemOut)
                }
            })
        }
    }

    Transactions.createTransaction = function (input, cb) {
        if (input.itemIds.length > 0) {

            winston.GenerateLog(2, 'createTransaction Method: recieved Item Unique Ids' + input.itemIds.toString());

            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];
            // var uniqueNames = getUnique(input.itemIds);
            const uniArr = input.itemIds;
            const uniSet = new Set(uniArr);
            const backUniArr = [...uniSet];

            async.map(backUniArr, submenu, function (asyncErr, asyncOut) {
                if (asyncErr) {
                    cb('No Transaction have been created', null)
                    winston.GenerateLog(0, 'createTransaction Method: No Transaction processed for easy scanning ');
                } else {
                    winston.GenerateLog(2, 'createTransaction Method: Successfully ' + successobj.length + ' transaction got created for given UniqueIds ');
                    cb(null, { "successobj": successobj, "failuerobj": failuerobj })
                }


            })
            //cb(null, "Created Successfully")


            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var companysDetails = server.models.Company;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;
                winston.GenerateLog(2, 'createTransaction Method:  validating item Unique ID');
                // var rr = (/(\d{3})-(\d{3})(\d{4})/);
                var rr = (/\d{3}[\s.-]\d{7}$/);

                // if (objsub.length == 11)
              if (rr.test(objsub)) {
                winston.GenerateLog(2, 'createTransaction Method: valid format item Unique ID' + objsub.toString());

                items.findOne({ 'where': { 'itemUniqueId': objsub, "companyId": input.companyId } }, function (companyErr, companyOut) {

                    if (companyErr) {
                        failuerobj.push({ "id": objsub, "status": "invalid" });
                        winston.GenerateLog(0, 'createTransaction Method: Error while finding Items for given uniqueId ' + objsub.toString());
                        // callBack('Error while finding Items for given uniqueId ' + objsub, null)
                        callBack(null, objsub);
                    }


                    if (companyOut != null) {
                        winston.GenerateLog(2, 'createTransaction Method: valid item ' + companyOut.itemId.toString());

                        Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                            if (!transactionErr) {
                                //   if (transactionOut == null) {
                                let data = {
                                    "reciever": input.reciever,
                                    "sender": input.sender,
                                    "nextTransactionId": "",
                                    "transStatusId": "2",
                                    "itemId": companyOut.itemId,
                                    "transMailFlag": "0",
                                    "deletedFlag": "0",
                                    "addedByAdminFlag": input.addedByAdminFlag,
                                    "transComment": "Transaction during item status change by admin."

                                }


                                Transactions.create(data, function (err1, resp) {
                                    if (err1) {
                                        failuerobj.push({ "id": objsub, "status": "invalid" });
                                        winston.GenerateLog(0, 'createTransaction Method: Error while creating transactions for given itemId ' + companyOut.itemId);
                                        callBack(null, objsub);
                                        // callBack('Error while creating transactions for given itemId ' + companyOut.itemId, null)

                                    } else {
                                        successobj.push({ "id": objsub, "status": "create" })
                                        winston.GenerateLog(2, 'createTransaction Method: User with Id ' + input.sender + 'creating transasction to  user with Id' + input.reciever);
                                        winston.GenerateLog(2, 'createTransaction Method: Generated Transaction for item ' + companyOut.itemId.toString());

                                        callBack(null, resp);
                                    }


                                })


                            }
                            else if (transactionErr) {
                                failuerobj.push({ "id": objsub, "status": "invalid" });
                                winston.GenerateLog(0, 'createTransaction Method: Error while querying Transactions for given itemId ' + companyOut.itemId);
                                // callBack('Error while querying Transactions for given itemId  ' + companyOut.itemId, null)
                                callBack(null, objsub);

                            } else {
                                callBack(null, null)
                            }



                        });



                    } else {
                        winston.GenerateLog(2, 'createTransaction Method: No Items found for given ItemUniqueId ');
                        // tags.findOne({ 'where': { 'tagName': 'DEFAULT' } }, function (tagErr, tagOut) {
                        companysDetails.findOne({ 'where': { 'companyId': input.companyId } }, function (tagErr, tagOut) {


                            // });
                            itemTypes.findOne({ 'where': { 'itemType': 'Default Type', companyId: input.companyId } }, function (itemTypeErr, itemTypeOut) {

                              //convert the Date format to required DB date format 
                              // 12/9/2019, 1:26:52 AM - Newyork format 
                              // 2019-12-09 00:27:34 - required format 
                              var datetime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
                              console.log('NEWYORK TIME ZONE ' + datetime);
                              var dateStr = moment(datetime, 'MM/DD/YYYY, HH:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
                                // });

                                let data = {
                                    "currentOwner": input.reciever,
                                    "userId": input.sender,
                                    "tagId": tagOut.defaultTag,
                                    "itemTypeId": itemTypeOut.itemTypeId,
                                    "transactionId": "",
                                    "initTransactionId": "0",
                                    "companyId": input.companyId,
                                    "itemStatusId": 1,
                                    "addItem": dateStr,
                                    "itemUniqueId": objsub,
                                    "updatedDate": new Date().toLocaleString("en-US", {timeZone: "America/New_York"}),
                                    "updatedBy": input.sender

                                }

                                items.create(data, function (err1, resp) {
                                    if (err1) {
                                        failuerobj.push({ "id": objsub, "status": "invalid" });
                                        winston.GenerateLog(0, 'createTransaction Method: Error while creating item for given uniqueId ' + err1);
                                        // callBack('Error while creating item for given uniqueId ' + err1, null)
                                        callBack(null, objsub);
                                    } else {
                                        successobj.push({ "id": objsub, "status": "Itemcreated" })
                                        winston.GenerateLog(2, 'createTransaction Method: Generated new item :' + objsub.toString());
                                        winston.GenerateLog(2, 'createTransaction Method: Generated new item for company with Id:' + input.companyId);

                                        let data1 = {
                                            "reciever": input.reciever,
                                            "sender": input.sender,
                                            "nextTransactionId": "",
                                            "transStatusId": "2",
                                            "itemId": resp.itemId,
                                            "transMailFlag": "0",
                                            "deletedFlag": "0",
                                            "addedByAdminFlag": input.addedByAdminFlag,
                                            "transComment": "Transaction during item status change by admin."


                                        }

                                        Transactions.create(data1, function (err1, resp1) {
                                            if (err1) {
                                                failuerobj.push({ "id": objsub, "status": "invalid" });
                                                winston.GenerateLog(0, 'createTransaction Method: Error while creating item for given uniqueId ' + err1);
                                                // callBack('Error while creating item for given uniqueId ' + objsub, null)
                                                rollBack(resp.itemId, null, function (rollErr, rollOut) {
                                                    callBack(null, objsub);
                                                })

                                            }
                                            else {
                                                successobj.push({ "id": objsub, "status": "Transactioncreate" });

                                                winston.GenerateLog(2, 'createTransaction Method: User with Id ' + input.sender + 'creating transasction to  user with Id' + input.reciever);
                                                winston.GenerateLog(2, 'createTransaction Method: Created transaction for item  ' + objsub.toString());


                                                items.findById(resp.itemId, function (err, item) {
                                                    if (err) {
                                                        callBack(new Error(err), null);
                                                    } else {

                                                        var obj12 = {
                                                            "transactionId": resp1.transactionId,
                                                            "itemStatusId": 2,
                                                            "initTransactionId": resp1.transactionId,
                                                            "updatedDate": new Date(),
                                                            "updatedBy": item.sender
                                                        };
                                                        item.updateAttributes(obj12, function (err, count) {

                                                            if (err) {
                                                                failuerobj.push({ "id": objsub, "status": "invalid" });
                                                                winston.GenerateLog(0, 'createTransaction Method: createTransaction Method: Error while updating item with transactionId ' + resp1.transactionId + err);
                                                                // callBack('Error while creating updating for with transactionId ' + err, null)
                                                                rollBack(resp.itemId, resp1.transactionId, function (rollErr, rollOut) {
                                                                    callBack(null, objsub);
                                                                })
                                                            } else {
                                                                winston.GenerateLog(2, 'createTransaction Method: update Transaction for item ' + resp.itemId.toString());

                                                                callBack(null, resp);
                                                            }


                                                        });
                                                    }
                                                });

                                            }

                                        })
                                    }
                                });
                            });
                        });

                        // failuerobj.push({"id":objsub,"status":"no item"});
                    }

                });

              }
              else {
                     failuerobj.push({ "id": objsub, "status": "invalid" });
                     winston.GenerateLog(0, 'createTransaction Method: invalid item Unique ID' + objsub.toString());

                     callBack(null, objsub);
                 }

            }

        } else {
            cb("Atleast One UniqueId should be present", null);
            winston.GenerateLog(0, 'createTransaction Method: Atleast One UniqueId should be present');

        }
    }
    // Transactions.GetTrancationsReports = function (input, cb) {

    //     input.FromDate = new Date(input.FromDate);
    //     input.ToDate = new Date(input.ToDate);
    //     var year = input.FromDate.getFullYear()
    //     var month = input.FromDate.getMonth() + 1
    //     var day = input.FromDate.getDate()
    //     if (day < 9) {
    //         day = '0' + day
    //     }
    //     if (month < 9) {
    //         month = '0' + month
    //     }
    //     var _fromdate = year + '-' + month + '-' + day
    //     _fromdate = _fromdate + 'T00:00:00.000Z'
    //     var _ToDate = new Date(input.ToDate)
    //     Transactions.find({
    //         where: {
    //             and: [{
    //               sender: input.UserId
    //             }]
    //         },

    //     }, function (err, data) {

    //         if (err) cb(err);
    //         else cb(null, data)
    //     })

    // }

    // Items sent by user report

    function getItemsBetweenDates(input, dCB) {

        var Difference_In_Time;
        var Difference_In_Days
        var date1 = new Date(binderInput.fromDate);
        var date2 = new Date(binderInput.toDate);
        var addItem = new Date(input.transactionTime);
        var firstDiff = date1.getTime() - addItem.getTime();
        var secondDiff = date2.getTime() - addItem.getTime();

        if (firstDiff <= 0 && secondDiff >= 0) {
            dCB(null, input)
        } else {
            dCB(null, null)
        }

    }
    Transactions.GetTrancationsReports = function (input, cb) {
        var app = require('../../server/server.js');
        if (input.fromDate && input.toDate) {
            var query = "select transaction_time as transactionTime, sender, username as userName, user_first_name as userFirstName, user_last_name as userLastName, count(*) as binder, CONCAT('', user_first_name, ' ',user_last_name, ' (', username , ' )') as name  from user u,transaction t where u.id=t.sender and t.sender = " + input.UserId + " and (transaction_time) >=" + "'" + input.fromDate + "'" + " and (transaction_time) <=" + "'" + input.toDate + "'" + " group by transaction_time order by transaction_time desc;"
        } else {
            var query = "select transaction_time as transactionTime, sender, username as userName, user_first_name as userFirstName, user_last_name as userLastName, count(*) as binder, CONCAT('', user_first_name, ' ',user_last_name, ' (', username , ' )') as name  from user u,transaction t where u.id=t.sender and t.sender = " + input.UserId + "  group by transaction_time order by transaction_time desc;"
        }
        var connector = app.dataSources.taptrack.connector;
        connector.execute(query, undefined, function (err, result) {
            if (err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }

    function cleanArray(actual) {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
            if (actual[i]) {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }
    var searchItemReceived2 = function (userId, text, feild, cb) {
        var feildQuery = {}
        if (feild) {
            feild = feild
            feildQuery[feild] = {
                like: text,
                options: "i"
            }
        }

        if (text == 'null' || text == undefined)
            var filter = {
                where: {
                    and: [{ userId: userId }]

                },
                order: feild + ' ASC',
                fields: {
                    itemId: true,
                    tagId: true,
                    itemTypeId: true,
                    itemUniqueId: true,
                    companyId: true,
                    currentOwner: true
                }
            }
        else {
            var filter = {
                where: {
                    and: [{ userId: userId }]
                },
                fields: {
                    itemId: true,
                    tagId: true,
                    itemTypeId: true,
                    itemUniqueId: true,
                    companyId: true,
                    currentOwner: true
                }
            }
            filter.where.and.push(feildQuery)
        }
        var items = server.models.Item;
        items.find(filter, function (err, response) {
            // server.models.Student.find({ where: { firstName: { like: text } } }, function (err, response) {
            if (err) cb(err);
            else {
                cb(null, response)
            }
        })
    }
    Transactions.searchItemReceived = function (obj, cb) {
        searchItemReceived2(obj.userId, obj.text, obj.feild, function (err, sIds) {
            if (err) cb(err);
            else {
                Transactions.find({
                    where: {
                        and: [{
                            itemId: {
                                inq: sIds.map(a => a.itemId)
                            }
                        }, { reciever: obj.userId }, { transStatusId: 1 }, { deletedFlag: 0 }, { addedByAdminFlag: 0 }]
                    }, skip: obj.skip,
                    limit: obj.limit, order: "transactionTime DESC"
                }, function (err, out) {
                    async.map(out, subMenu, function (asyncErr, asyncOut) {
                        cb(null, asyncOut);
                    });
                });
                function subMenu(sub, cb) {
                    var userInfo = server.models.Users;

                    var items = server.models.Item;
                    items.findOne({ where: { itemId: sub.itemId } }, function (err3, out3) {
                        userInfo.findOne({ where: { id: sub.sender } }, function (err1, out1) {
                            userInfo.findOne({ where: { id: sub.reciever } }, function (err2, out2) {
                                var data = sub;
                                data.senderName = out1.userName;
                                data.receiverName = out2.userName;
                                data.itemUniqueId = out3.itemUniqueId;
                                data.itemStatusName = 'Ready';
                                cb(null, data);
                            });
                        });
                    });
                };
            }
        })
    }
    var getItemSentsearch2 = function (userId, text, feild, cb) {
        var feildQuery = {}
        if (feild) {
            feild = feild
            feildQuery[feild] = {
                like: text,
                options: "i"
            }
        }

        if (text == 'null' || text == undefined)
            var filter = {
                where: {
                    and: [{ reciever: userId }]

                },
                order: feild + ' ASC',
                fields: {
                    itemId: true,
                    tagId: true,
                    itemTypeId: true,
                    itemUniqueId: true,
                    companyId: true,
                    userId: true,
                    transactionId: true,

                    currentOwner: true
                }
            }
        else {
            var filter = {
                where: {
                    and: [{ reciever: userId }]
                },
                fields: {
                    itemId: true,
                    tagId: true,
                    itemTypeId: true,
                    itemUniqueId: true,
                    companyId: true,
                    userId: true,
                    transactionId: true,
                    currentOwner: true
                }
            }
            filter.where.and.push(feildQuery)
        }
        var items = server.models.Item;
        items.find(filter, function (err, response) {
            // server.models.Student.find({ where: { firstName: { like: text } } }, function (err, response) {
            if (err) cb(err);
            else {
                cb(null, response)
            }
        })
    }
    Transactions.getItemSentsearch = function (obj, cb) {
        getItemSentsearch2(obj.userId, obj.text, obj.feild, function (err, sIds) {
            if (err) cb(err);
            else {
                Transactions.find({
                    where: {
                        and: [{
                            transactionId: {
                                inq: sIds.map(a => a.transactionId)
                            }
                        }, { sender: obj.userId }, { transStatusId: 1 }, { deletedFlag: 0 }, { addedByAdminFlag: 0 }]
                    }, skip: obj.skip,
                    limit: obj.limit, order: "transactionTime DESC"
                }, function (err, out) {
                    async.map(out, subMenu, function (asyncErr, asyncOut) {

                        cb(null, asyncOut);
                    });
                });

                function subMenu(sub, cb) {
                    var userInfo = server.models.Users;
                    var items = server.models.Item;
                    items.findOne({ where: { itemId: sub.itemId } }, function (err3, out3) {
                        userInfo.findOne({ where: { id: sub.sender } }, function (err1, out1) {
                            userInfo.findOne({ where: { id: sub.reciever } }, function (err2, out2) {
                                var itemStatusNew;
                                if (sub.itemStatusId == 1) {
                                    itemStatusNew = "Ready";
                                }
                                if (sub.itemStatusId == 2) {
                                    itemStatusNew = "Success";
                                }
                                var data = sub;
                                data.senderName = out1.userName;
                                data.receiverName = out2.userName;
                                data.itemUniqueId = out3.itemUniqueId;
                                data.itemStatusName = itemStatusNew;
                                cb(null, data);
                            });
                        });
                    });
                };
            }
        })
    }
    Transactions.remoteMethod('searchItemReceived', {
        description: 'searchItemReceived for dashboard',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/searchItemReceived',
            verb: 'POST',
        },
    });
    Transactions.remoteMethod('getItemSentsearch', {
        description: 'getItemSentsearch for dashboard',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getItemSentsearch',
            verb: 'POST',
        },
    });
    Transactions.remoteMethod('createTransaction', {
        description: 'createTransaction',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/createTransaction',
            verb: 'POST',
        },
    });

    Transactions.remoteMethod('GetTrancationsReports', {
        description: 'GetTranscations Reports',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/GetTrancationsReports',
            verb: 'POST',
        },
    });

    Transactions.remoteMethod('getdatausingtagid', {
        description: 'getdatausingtagid for printbarcode',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getdatausingtagid',
            verb: 'POST',
        },
    });
    Transactions.getdatausingtagid = function (obj, cb) {
        var items = server.models.Item;
        items.find({
          where: {
            and: [
              { tagId: obj.tagId },
              { userId: obj.userId },
              { companyId: obj.companyId }
            ]
          }
        }, function (err, data) {
            if (err) {
            winston.GenerateLog(0, 'getdatausingtagid   : ' + 'error while fetching data from items '+err);
                cb(err, null)
            }
            else {
            winston.GenerateLog(2, 'getdatausingtagid   : ' + 'list of recorrds for given itemTd '+data.length);
                cb(null, data)}
        })

    }
    Transactions.getItemReceived = function (input, cb) {
        if (input.userId != null) {



            // Transactions.find({ where:{and: [{ reciever: input.userId},{itemStatusId:1},{deletedFlag:0},{addedByAdminFlag:1}] },order: "transactionTime DESC"}, function( err,out){
            Transactions.find({
                where: { and: [{ reciever: input.userId }, { transStatusId: 1 }, { deletedFlag: 0 }, { addedByAdminFlag: 0 }] }, skip: input.skip,
                limit: input.limit, order: "transactionTime DESC"
            }, function (err, out) {
                async.map(out, subMenu, function (asyncErr, asyncOut) {

                    cb(null, asyncOut);
                });


                // cb(null,out);


            });

            function subMenu(sub, cb) {
                var userInfo = server.models.Users;



                var items = server.models.Item;
                // var senderName =

                items.findOne({ where: { itemId: sub.itemId } }, function (err3, out3) {

                    userInfo.findOne({ where: { id: sub.sender } }, function (err1, out1) {

                        var testNa2;
                        if (out1) {
                            testNa2 = out1.userName;
                        }
                        else {
                            testNa2 = null;
                        }


                        userInfo.findOne({ where: { id: sub.reciever } }, function (err2, out2) {
                            var testNa;
                            if (out2) {
                                testNa = out2.userName;
                            }
                            else {
                                testNa = null;
                            }

                            var data = sub;
                            data.senderName = out1.userName;
                            // data.senderName = out2.userName;
                            // data.receiverName = out2.userName;
                            data.receiverName = testNa;
                            data.itemUniqueId = out3.itemUniqueId;
                            data.itemStatusName = 'Ready';
                            //   data.push({'senderName':out1.userName,'receiverName':out2.userName});

                            cb(null, data);
                        });
                    });
                });
            };


            // Transactions.find({ where:{and: [{ reciever: input.userId},{itemStatusId:1},{deletedFlag:0},{addedByAdminFlag:1}] },skip: input.skip,
            //     limit: input.limit,order: "transactionTime DESC"}, function( err,out){
        }
        else {
            cb("UserId Required", null)
        }

    };




    Transactions.remoteMethod("getItemReceived", {
        description: "To getItemReceived",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getItemReceived",
            verb: "POST"
        }
    });


    Transactions.getItemReceivedCount = function (input, cb) {
        if (input.userId != null) {

            Transactions.count({ reciever: input.userId, transStatusId: 1, deletedFlag: 0, addedByAdminFlag: 0 }, cb);
        }
        else {
            cb(null, "UserId Required")
        }

    };

    Transactions.remoteMethod("getItemReceivedCount", {
        description: "To getItemReceivedCount",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getItemReceivedCount",
            verb: "POST"
        }
    });


    Transactions.getItemSent = function (input, cb) {
        if (input.userId) {

            // Transactions.find({ where:{and: [{ sender: input.userId},{deletedFlag:0},{addedByAdminFlag:1}] },order: "transactionTime DESC"}, function( err,out){
            // cb(null,out);
            Transactions.find({
                where: { and: [{ sender: input.userId }, { deletedFlag: 0 }, { addedByAdminFlag: 0 }, { transStatusId: { neq: 3 } }] }, skip: input.skip,
                limit: input.limit, order: "transactionTime DESC"
            }, function (err, out) {

                // this line is removed from above query { transStatusId: { neq: 2 }  }
                async.map(out, subMenu, function (asyncErr, asyncOut) {

                    cb(null, asyncOut);
                });
                // Transactions.find({ where:{and: [{ sender: input.userId},{deletedFlag:0},{addedByAdminFlag:1}] },skip: input.skip,
                //     limit: input.limit,order: "transactionTime DESC"}, function( err,out){
                //     cb(null,out);


            });

            function subMenu(sub, cb) {
                var userInfo = server.models.Users;
                var items = server.models.Item;

                // var senderName =

                items.findOne({ where: { itemId: sub.itemId } }, function (err3, out3) {
                    userInfo.findOne({ where: { id: sub.sender } }, function (err1, out1) {


                        userInfo.findOne({ where: { id: sub.reciever } }, function (err2, out2) {

                            var itemStatusNew;
                            if (out3.itemStatusId == 1) {
                                itemStatusNew = "Pending";
                            }
                            if (out3.itemStatusId == 2) {
                                itemStatusNew = "Success";
                            }

                            var data = sub;
                            data.senderName = out1.userName;
                            data.receiverName = out2 ? out2.userName : '';
                            data.itemUniqueId = out3.itemUniqueId;
                            data.itemStatusName = itemStatusNew;
                            //   data.push({'senderName':out1.userName,'receiverName':out2.userName});


                            cb(null, data);
                        });
                    });
                });
            };

        } else {
            cb('UserId is required', null)
        }
    };

    Transactions.getItemSentCount = function (input, cb) {
        Transactions.count({ sender: input.userId, deletedFlag: 0, addedByAdminFlag: 0, transStatusId: { neq: 3 } }, cb);
        // this line is removed from above query , transStatusId: { neq: 2 }
    };
    Transactions.remoteMethod("getItemSentCount", {
        description: "To getItemSentCount",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getItemSentCount",
            verb: "POST"
        }
    });
    Transactions.remoteMethod("getItemSent", {
        description: "To getItemSent",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getItemSent",
            verb: "POST"
        }
    });

    //   Transactions.cancelSentItem = function(input, cb) {

    //     // Transactions.find({ where:{and: [{ sender: input.userId},{deletedFlag:0},{addedByAdminFlag:1}] },order: "transactionTime DESC"}, function( err,out){
    //     //     cb(null,out);


    //     // });

    //     if (input.length > 0) {
    //         var successobj=[];
    //         var failuerobj=[];
    //         async.map(input, submenu, function (asyncErr, asyncOut) {

    //             cb(null,{"successobj":successobj,"failuerobj":failuerobj})
    //        });
    //        function submenu(objsub, callBack) {

    //         Transactions.findOne({ 'where': { 'transactionId': objsub.transactionId } }, function (companyErr, companyOut) {


    //         if (companyOut != null) {
    //             companyOut.updateAttributes({"transStatusId":3}, function (err, updated) {
    //                 if (err) {
    //                     console.error(err);
    //                   }


    //                   successobj.push({"id":objsub,"status":"Cancelled"})
    //                   callBack (null,updated);
    //             });
    //         }
    //         else
    //         {
    //             callBack (null,'Error');
    //         }
    //        });

    //        };
    //     }


    //   };

    Transactions.cancelSentItem = function (input, cb) {
        if (input.length > 0) {
            var successobj = [];
            var failuerobj = [];
            async.map(input, submenu, function (asyncErr, asyncOut) {
                winston.GenerateLog(2, 'Successfully canceled list of sent items are :  ' + input.length + ' ');
                cb(null, { "successobj": successobj, "failuerobj": failuerobj })
            });
            function submenu(objsub, callBack) {

                Transactions.findOne({ 'where': { 'transactionId': objsub.transactionId } }, function (companyErr, companyOut) {


                    if (companyOut != null) {
                        var objj = {};
                        var items = server.models.Item;

                        // objj = companyOut;
                        objj['transStatusId'] = 3;
                        objj['reciever'] = objsub.sender;
                        companyOut.updateAttributes(objj, function (err, updated) {

                            if (err) {
                                winston.GenerateLog(0, 'Error while updating transaction with statusId 3 :  ' + err + ' ');
                                console.error(err);
                            }
                            winston.GenerateLog(2, 'Successfully transaction got updated with status 3 for Id :  ' + updated.transactionId + ' ');
                            items.findById(companyOut.itemId, function (err, item) {
                                if (err) {
                                    cb(new Error(err));
                                } else {

                                    var objjIt = {
                                        "transactionId": companyOut.transactionId, "currentOwner": companyOut.reciever, "itemStatusId": 2, "updatedDate": new Date(),
                                        "updatedBy": companyOut.sender
                                    };

                                    item.updateAttributes(objjIt, function (err, count) {
                                        if (err) {
                                            winston.GenerateLog(0, 'Error while updating Item with statusId 2 :  ' + err + ' ');
                                            console.error(err);
                                        }
                                        winston.GenerateLog(2, 'update Transaction for item ' + companyOut.itemId.toString());



                                    });
                                }
                            });



                            successobj.push({ "id": objsub, "status": "updated" })
                            callBack(null, updated);
                        });

                    }
                    else {
                        callBack(null, 'Error');
                    }
                });

            };
        }
        else {
            cb(null, 'no data');
        }
    }

    Transactions.remoteMethod("cancelSentItem", {
        description: "To cancelSentItem",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "array",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/cancelSentItem",
            verb: "POST"
        }
    });

    Transactions.acknowledgeRecieveItem = function (input, cb) {

        async.map(input, itemSub, function (asyncErr, asyncOut) {
            winston.GenerateLog(2, 'Successfully ' + asyncOut.length + ' items got acknowledged. ' + ' ');
            cb(null, asyncOut);
        });

        function itemSub(sub, cb) {
            var items = server.models.Item;
            // var senderName =
            Transactions.findOne({ 'where': { 'transactionId': sub.transactionId } }, function (companyErr, companyOut) {


                if (companyOut != null) {

                    companyOut.updateAttribute("transStatusId", 2, function (err, updated) {
                        // console.log('tttttttttttttttttttttt ', err, updated);
                        if (err) {
                            winston.GenerateLog(0, 'error while updating with statusid 2 for transactions ' + err + ' ');
                            console.error(err);
                        }
                        winston.GenerateLog(2, 'successfully transaction updated with statusId 2  and transactionId :  ' + updated.transactionId + ' ');

                        //   successobj.push({"id":objsub,"status":"updated"})
                        //   callBack (null,updated);
                    });

                    items.findOne({ where: { 'itemId': companyOut.itemId } }, function (err2, out2) {
                        if (out2 != null) {
                            out2.updateAttributes({ "itemStatusId": 2 }, { "currentOwner": sub.userId, "updatedDate": new Date(), "updatedBy": companyOut.sender }, function (err, updated) {
                                // console.log('iiiiiiiiiiiiiiiiiiiiiiiiii ', err2, out2);
                                if (err) {
                                    winston.GenerateLog(0, 'error while updating with statusid 2 for item in acknowledge item ' + err + ' ');
                                    console.error(err);
                                }

                                winston.GenerateLog(2, 'successfully Items updated with statusId 2  and ItemId :  ' + out2.itemId + ' ');

                                // successobj.push({"id":objsub,"status":"updated"})
                                cb(null, updated);
                            });

                        }
                        else {
                            winston.GenerateLog(0, 'error while fetching item in acknowledge item ' + err2 + ' ');
                            cb(null, 'Error');
                        }
                    });


                }
            });

        };
    };

    /** acknowledgeRecieveItem method declaration */
    Transactions.remoteMethod("acknowledgeRecieveItem", {
        description: "To acknowledgeRecieveItem",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "array",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/acknowledgeRecieveItem",
            verb: "POST"
        }
    });



    /** sendItemTransaction method : to deal with sending items
     * @constructor
     * @callback - to deal with response
     */
    Transactions.sendItemTransaction = function (input, cb) {

        if (input.itemIds.length > 0) {


            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];


            async.map(input.itemIds, submenu, function (asyncErr, asyncOut) {

                cb(null, { "successobj": successobj, "failuerobj": failuerobj })
            });
            //cb(null, "Created Successfully")





            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;

                var rr = (/\d{3}[\s.-]\d{7}$/);

                // if (objsub.length == 11)

                // if (rr.test(objsub)) {
                if (input.transComments[input.itemIds.indexOf(objsub)]) {
                    /** check if item exist for given itemUniqueId for given companyId */
                    items.findOne({ 'where': { 'itemUniqueId': objsub, companyId: input.companyId } }, function (companyErr, companyOut) {
                        // console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii ', companyOut, objsub);
                        if (companyOut != null) {
                            /** find transaction for given itemId */
                            Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                                if (!transactionErr) {
                                    //   if (transactionOut == null) {


                                    let data = {
                                        "reciever": input.reciever,
                                        "sender": input.sender,
                                        "nextTransactionId": "",
                                        "transStatusId": "1",
                                        "itemId": companyOut.itemId,
                                        "transMailFlag": "0",
                                        "deletedFlag": "0",
                                        "addedByAdminFlag": input.addedByAdminFlag,
                                        "transComment": input.transComments[input.itemIds.indexOf(objsub)]

                                    }
                                    /** create transaction for given item */
                                    Transactions.create(data, function (err1, resp) {
                                        successobj.push({ "id": objsub, "status": "create" })

                                        // console.log('tttttttttttttttttttttttt ', err1,data, resp);
                                        items.findById(resp.itemId, function (err, item) {
                                            if (err) {
                                                cb(new Error(err));
                                            } else {

                                                /** update item to pending status */
                                                item.updateAttributes({ "itemStatusId": 1, "updatedDate": new Date(), "updatedBy": resp.sender }, function (err, count) {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                    winston.GenerateLog(2, 'update Transaction for item with status to ready ' + resp.itemId.toString());



                                                });
                                            }
                                        });


                                        callBack(null, resp);

                                    })


                                }
                                else {
                                    failuerobj.push({ "id": objsub, "status": "no trans" })
                                    callBack(null, resp);





                                    //

                                    // var perTransId = tr

                                }



                            });



                        }
                        else {
                            failuerobj.push({ "id": objsub, "status": "no Id" })
                            callBack(null, resp);
                            // cb('no data',null)
                        }
                    });
                    //need to add here
                }
                else {
                    cb('Comments Required for' + objsub, null);
                }
                // };
            };

        }
        else {
            cb('Ids Required', null);
        }
    };


    /** sendItemTransaction method declaration */
    Transactions.remoteMethod('sendItemTransaction', {
        description: 'sendItemTransaction',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/sendItemTransaction',
            verb: 'POST',
        },
    });

/** closeSelectedItems method : close selected items
 * @constructor
 * @callback - callback to deal with response
 */
    Transactions.closeSelectedItems = function (input, cb) {

        if (input.itemIds.length > 0) {


            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];


            async.map(input.itemIds, submenu, function (asyncErr, asyncOut) {

                cb(null, { "successobj": successobj, "failuerobj": failuerobj })
            });

            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;
                var rr = (/\d{3}[\s.-]\d{7}$/);

                // var rr = (/(\d{3})-(\d{3})(\d{4})/);

                // if (objsub.length == 11)
                /** findOne method to fetch item record for given itemUniqueId and companyId for given owner */
                items.findOne({ 'where': { 'and': [{ 'itemUniqueId': objsub }, { 'companyId': input.companyId }, { 'currentOwner': input.reciever }, { 'itemStatusId': { 'neq': 6 } }, { 'itemStatusId': 2 }] } }, function (companyErr, companyOut) {


                    if (companyOut != null) {
                        Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                            if (!transactionErr) {
                                //   if (transactionOut == null) {
                                let data = {
                                    "reciever": input.reciever,
                                    "sender": input.sender,
                                    "nextTransactionId": "",
                                    "transStatusId": "2",
                                    "itemId": companyOut.itemId,
                                    "transMailFlag": "0",
                                    "deletedFlag": "0",
                                    "addedByAdminFlag": input.addedByAdminFlag,
                                    // "transComment": input.transComments[input.itemIds.indexOf(objsub)]
                                    "transComment": 'Item closed  by user'

                                }
                                /** transaction will get created */
                                Transactions.create(data, function (err1, resp) {
                                    successobj.push({ "id": objsub, "status": "create" })
                                    callBack(null, resp);

                                })


                            }
                            else {
                                failuerobj.push({ "id": objsub, "status": "no trans" })
                                callBack(null, resp);





                                //

                                // var perTransId = tr

                            }



                        });



                    }
                    else {
                        cb('Item already closed', null);
                    }
                });
            };
        };
    };

    /** closeSelectedItems method declaration */
    Transactions.remoteMethod("closeSelectedItems", {
        description: "To closeSelectedItems",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/closeSelectedItems",
            verb: "POST"
        }
    });

    /** checkoutSelectedItems method : selected items will get checkout
     * @constructor
     * @callback - to handle response
     */
    Transactions.checkoutSelectedItems = function (input, cb) {
        // var rr = (/\d{3}[\s.-]\d{7}$/);

        if (input.itemIds.length > 0) {


            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];


            async.map(input.itemIds, submenu, function (asyncErr, asyncOut) {
                winston.GenerateLog(2, 'checkoutSelectedItems Method :  successfully list of items checkout ' + successobj.length)
                cb(null, { "successobj": successobj, "failuerobj": failuerobj })
            });

            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;

                var rr = (/(\d{3})-(\d{3})(\d{4})/);

                // if (objsub.length == 11)
                // if (rr.test(objsub)) {
                // items.findOne({ 'where': {'and': [{ 'itemUniqueId': objsub },{'itemStatusId':{'neq':6}}] }}, function (companyErr, companyOut) {


                /** findOne method to check whether itemUniqueId is exist for given company */
                items.findOne({ 'where': { 'and': [{ 'itemUniqueId': objsub }, { 'companyId': input.companyId }, { 'itemStatusId': 1 }] } }, function (companyErr, companyOut) {


                    if (companyOut != null) {
                        /** if exist it will look for associated transaction */
                        Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                            if (!transactionErr) {
                                //   if (transactionOut == null) {
                                let data = {
                                    "reciever": input.reciever,
                                    "sender": input.sender,
                                    "nextTransactionId": "",
                                    "transStatusId": "2",
                                    "itemId": companyOut.itemId,
                                    "transMailFlag": "0",
                                    "deletedFlag": "0",
                                    "addedByAdminFlag": input.addedByAdminFlag,
                                    // "transComment": input.transComments[input.itemIds.indexOf(objsub)]
                                    "transComment": 'Item checked out by user'

                                }
                                /** if not it will create transaction for given itemId */
                                Transactions.create(data, function (err1, resp) {
                                    successobj.push({ "id": objsub, "status": "create" })
                                    callBack(null, resp);

                                })


                            }
                            else {
                                failuerobj.push({ "id": objsub, "status": "no trans" })
                                callBack(null, resp);





                                //

                                // var perTransId = tr

                            }



                        });



                    }
                    else {
                        cb('Item already checked out', null);
                    }
                });

                // }
                // else {
                //     cb('Sorry the Item does not exists', null)
                // }
            };
        };
    };
    /** checkoutSelectedItems method declaration */
    Transactions.remoteMethod("checkoutSelectedItems", {
        description: "To checkoutSelectedItems",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                required: true,
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/checkoutSelectedItems",
            verb: "POST"
        }
    });


    /** assignItemTransaction method to create transaction for assigned items
     * @constructor
     * @callback - to handle response
    */

    Transactions.assignItemTransaction = function (input, cb) {

        if (input.itemIds.length > 0) {


            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];


            async.map(input.itemIds, submenu, function (asyncErr, asyncOut) {
                if (asyncErr) {
                    winston.GenerateLog(0, 'assignItemTransaction Method : Error while assining the items ');
                    cb('Error while assining the items', null)
                } else {
                    winston.GenerateLog(2, 'assignItemTransaction Method : List of assigned item transaction ' + successobj.length);
                    cb(null, { "successobjCount": input.itemIds.length, "successobj": successobj, "failuerobj": failuerobj })
                }


            });
            //cb(null, "Created Successfully")




            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;

                var rr = (/\d{3}[\s.-]\d{7}$/);

                // if (objsub.length == 11)
                // if (rr.test(objsub)) {
                    /** check itemUniqueId exist for given company */
                items.findOne({ 'where': { 'itemUniqueId': objsub, companyId: input.companyId } }, function (companyErr, companyOut) {


                    if (companyOut != null) {
                        /** find transaction for given itemId */
                        Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                            if (!transactionErr) {
                                //   if (transactionOut == null) {


                                let data = {
                                    "reciever": input.reciever,
                                    "sender": input.sender,
                                    "nextTransactionId": "",
                                    "transStatusId": "2",
                                    "itemId": companyOut.itemId,
                                    "transMailFlag": "0",
                                    "deletedFlag": "0",
                                    "addedByAdminFlag": input.addedByAdminFlag,
                                    "transComment": 'Item Assigned to other by user'

                                }
                                /** create transaction for given itemId */
                                Transactions.create(data, function (err1, resp) {
                                    successobj.push({ "id": objsub, "status": "create" })
                                    winston.GenerateLog(2, 'assignItemTransaction Method : successfully transaction got created ');
                                    callBack(null, resp);

                                })


                            }
                            else {
                                failuerobj.push({ "id": objsub, "status": "no trans" })
                                callBack(null, resp);





                                //

                                // var perTransId = tr

                            }



                        });



                    }
                    else {
                        cb(null, 'no data')
                    }
                });
                // };
            };

        };
    };

/** assignItemTransaction method declaration */
    Transactions.remoteMethod('assignItemTransaction', {
        description: 'assignItemTransaction',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/assignItemTransaction',
            verb: 'POST',
        },
    });

    /** changeItemStatusTransaction method : to change the item status and creating transaction for given itemUniqueId's
     * @constructor
     * @param {Array} successobj - array with success objects
     * @param {Array} failuerobj - array with failed objects
     * cb - to handle the reponse
     */
    Transactions.changeItemStatusTransaction = function (input, cb) {

        if (input.itemIds.length > 0) {


            //levelssave(input, cb)
            var successobj = [];
            var failuerobj = [];


            async.map(input.itemIds, submenu, function (asyncErr, asyncOut) {
                if (asyncErr) {
                    winston.GenerateLog(0, 'changeItemStatusTransaction Method : Error while changing the status of items ')
                    cb('Error while changing the status of items', null)
                } else {
                    winston.GenerateLog(2, 'changeItemStatusTransaction Method : list of items got changed ' + successobj.length)
                    cb(null, { "successobj": successobj, "failuerobj": failuerobj })

                }


            });
            //cb(null, "Created Successfully")





            function submenu(objsub, callBack) {

                // if (objsub.test(Number))
                var items = server.models.Item;
                var tags = server.models.Tag;
                var itemTypes = server.models.ItemType;
                var itemStatus = server.models.ItemStatus;

                var rr = (/\d{3}[\s.-]\d{7}$/);

                // if (objsub.length == 11)
                // if (rr.test(objsub)) {
                    /** findOne method to check whether itemUniqueId is exist for given companny */
                items.findOne({ 'where': { 'itemUniqueId': objsub, companyId: input.companyId } }, function (companyErr, companyOut) {
                    if (companyOut != null) {
                        /** if record found it will check for transaction */
                        Transactions.find({ 'where': { 'itemId': companyOut.itemId } }, function (transactionErr, transactionOut) {


                            if (!transactionErr) {
                                //   if (transactionOut == null) {


                                let data = {
                                    "reciever": input.reciever,
                                    "sender": input.sender,
                                    "nextTransactionId": "",
                                    "transStatusId": "2",
                                    "itemId": companyOut.itemId,
                                    "transMailFlag": "0",
                                    "deletedFlag": "0",
                                    "addedByAdminFlag": input.addedByAdminFlag,
                                    "transComment": input.transComment

                                }
                                /** transaction to create */
                                Transactions.create(data, function (err1, resp) {
                                    successobj.push({ "id": objsub, "status": "create" })

                                    // var  objj = {"itemStatusId":input.itemStatusId};
                                    items.findById(resp.itemId, function (err, item) {
                                        if (err) {
                                            cb(new Error(err));
                                        } else {
                                            /** update the item statusId for given item */
                                            item.updateAttributes({ "itemStatusId": input.itemStatusId, "updatedDate": new Date(), "updatedBy": resp.sender }, function (err, count) {
                                                if (err) {
                                                    console.error(err);
                                                }
                                                winston.GenerateLog(2, 'changeItemStatusTransaction Method ; update Transaction for item ' + resp.itemId.toString());



                                            });
                                        }
                                    });

                                    callBack(null, resp);

                                })


                            }
                            else {
                                failuerobj.push({ "id": objsub, "status": "no trans" })
                                callBack(null, resp);





                                //

                                // var perTransId = tr

                            }



                        });



                    }
                    else {
                        cb(null, 'no data')
                    }
                });
            };
            // };

        };
    };

/** changeItemStatusTransaction method declaration */
    Transactions.remoteMethod('changeItemStatusTransaction', {
        description: 'changeItemStatusTransaction',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/changeItemStatusTransaction',
            verb: 'POST',
        },
    });




    
/** changeItemStatusTransaction method declaration */

Transactions.bindersProcessed = function(input, cb) {
    var filter = {
        where: {
          or: [
            {
              transSucceedTime: {
                between: [input.fromDate, input.toDate]
              },
            }
          ]
        }
      };
      Transactions.find({
        where: filter.where,
        // order: 'itemId DESC & transSucceedTime DESC',
        "order": ["itemId DESC", "transSucceedTime DESC"],
      include: [{ // include orders for the owner
        relation: 'recieverx',
        scope: { // further filter the owner object
          fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
        } // only show two fields
      }, { // include orders for the owner
        relation: 'senderx',
        scope: { // further filter the owner object
          fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
        } // only show two fields
      },
      {relation: 'itemRelations', // include the owner object
            scope: { // further filter the owner object
             // order: 'itemId DESC',
              where: {
                companyId: input.companyId,
                itemStatusId: 2,
              },
              }}],
    }, function(err, out) {
        if(out.length > 0) {
            var finalArr = [];
            var binderArr = [];
            var bindersArr = [];
            var arr = [] = JSON.stringify(out);
            var xyz = [] = JSON.parse(arr);
            var bindersLen = [];
            winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'got total records of  '+xyz.length);
            if (xyz.length > 0) {
                
                // async.each(xyz, function (object, callBC) {
                for(var i=0;i<xyz.length;i++) {
                    let object = xyz[i];
                var itemTypeObj = {} = JSON.stringify(object);
                var itemTypeObject = JSON.parse(itemTypeObj);
                if(itemTypeObject.itemRelations) {
                    var obj = {};
                    var datetime = itemTypeObject.transSucceedTime;
                    var reqDate = new Date(datetime).toISOString().replace('Z', '').replace('T', '  ');
                    var date = new Date(itemTypeObject.transSucceedTime);
                    var month = (date.getMonth() + 1);
                    var datee = date.getDate();
                    var addDate = new Date(itemTypeObject.itemRelations.addItem);
                    bindersLen.push(itemTypeObject.itemRelations.itemId);
                    obj['transactionId'] = itemTypeObject.transactionId;
                    obj['time'] = date.getTime();
                    obj['addDate'] = addDate.getTime();
                    obj['itemId'] = itemTypeObject.itemRelations.itemId;
                    obj['itemUniqueId'] = itemTypeObject.itemRelations.itemUniqueId;
                    obj['senderName'] = (itemTypeObject.senderx.locationFlag == 0) ? itemTypeObject.senderx.userFirstName + ' ' + itemTypeObject.senderx.userLastName + ' (' + itemTypeObject.senderx.username + ')' : itemTypeObject.senderx.location + ' (' + itemTypeObject.senderx.username + ')';
                    obj['transactionSuccessTime'] = reqDate.toString();
                    obj['recieverName'] = (itemTypeObject.recieverx.locationFlag == 0) ? itemTypeObject.recieverx.userFirstName + ' ' + itemTypeObject.recieverx.userLastName + ' (' + itemTypeObject.recieverx.username + ')' : itemTypeObject.recieverx.location + ' (' + itemTypeObject.recieverx.username + ')';
                    if(i==0) {
                    bindersArr.push(obj)
                    } else if (i>0) {
                        let object1 ={}= xyz[i-1];
                        let object2= {}= xyz[i];
                        var itemTypeObj1 = {} = JSON.stringify(object1);
                        var itemTypeObject1 = JSON.parse(itemTypeObj1);
                        var itemTypeObj2 = {} = JSON.stringify(object2);
                        var itemTypeObject2 = JSON.parse(itemTypeObj2);
                        var datetime1 = itemTypeObject1.transSucceedTime;
                        var reqDate1 = new Date(datetime1).toISOString().replace('Z', '').replace('T', '  ');
                        let previous = new Date(reqDate1.toString()).getTime();
                        let diff = previous- new Date(obj['transactionSuccessTime']).getTime();
                        // console.log('00000000000000000000000000000000000000 ', obj);
                        if(i>0 && itemTypeObject1['itemId']==itemTypeObject2['itemId']&&(Math.abs(diff/ 1000) < 50)) {
                            // obj['transId'] = 'extra';
                            // console.log('ooooooooooooooooo ', obj);
                            // bindersArr.push(obj)
                        } 
                        else {
                            bindersArr.push(obj);
                        }
                    }
                    
                }
                // callBC();
                // });
            }
               var binderslength = getUnique(bindersLen);
               bindersArr[0].bindersLength = binderslength.length;
            //    compareArr1(bindersArr, function (err, finalResp) {
            //        console.log(finalResp.length);
                cb(null, bindersArr)
            //   });
                
            } else {
             cb(null, [])
            }
            } else {
            cb(null, [])
            }
        
    })
    };

    function getUnique(array){
        var uniqueArray = [];
        
        // Loop through array values
        for(var value of array){
            if(uniqueArray.indexOf(value) === -1){
                uniqueArray.push(value);
            }
        }
        return uniqueArray;
    }
function compareArr1(finalArr, callB) {
    var one = [] = finalArr;
    var two = [] = finalArr;
    var three = [] = finalArr;
    one.forEach((e1) => two.forEach((e2) => {
        var firstDate = new Date(e1['transactionSuccessTime']);
        var secondDate = new Date(e2['transactionSuccessTime']);
        var diffMs = (firstDate - secondDate);
        var diff = (firstDate.getTime() - secondDate.getTime()) / 1000;
        // console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ',diff, e2['transactionId'], e1['transactionId']);
        if ((Math.abs(diff) < 50) && e2['transactionId'] != e1['transactionId'] && e2['itemUniqueId'] == e1['itemUniqueId']) {
            // if (e2['i'] == 0 && e1['i']!=0) {
            //   removeByAttr(three, 'i', e1['i'])
            // } else {
              removeByAttr(three, 'transactionId', e2['transactionId'])
            // }
            
        }
        // console.log(three.length);
    }))
    
    callB(null, three)
}
var removeByAttr = function (arr, attr, value) {
    var j = arr.length;
    while (j--) {
      if (arr[j]
        && arr[j].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[j][attr] === value)) {
        arr.splice(j, 1);

      }
    }
    return arr;
  }
Transactions.remoteMethod('bindersProcessed', {
    description: 'bindersProcessed',
    returns: {
        type: 'object',
        root: true,
    },
    accepts: [{
        arg: 'data',
        type: 'object',
        required: true,
        http: {
            source: 'body',
        },
    }],
    http: {
        path: '/bindersProcessed',
        verb: 'POST',
    },
});


    function getUnique(array) {
        var uniqueArray = [];

        // Loop through array values
        for (let i = 0; i < array.length; i++) {
            if (uniqueArray.indexOf(array[i]) === -1) {
                uniqueArray.push(array[i]);
            }
        }
        return uniqueArray;
    }

};