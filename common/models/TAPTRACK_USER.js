
'use strict';
var server = require('../../server/server');
var config = require('../../server/config.json');
var winston = require('../../winstonsockets');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var config = require('../../server/config');
// Nodejs encryption with CTR
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
module.exports = function (USER) {
  delete USER.validations.email;
  delete USER.validations.username;

  USER.hashPassword = function (plain) {
    return plain; // your hashing algo will come here.
  };

  USER.prototype.hasPassword = function (plain, fn) {
    fn = fn || utils.createPromiseCallback();
    if (this.password && plain) {
      const isMatch = this.password === plain // same hashing algo to come here.
      fn(null, isMatch)
    } else {
      fn(null, false);
    }
    return fn.promise;
  };

  USER.observe('before save', function (ctx, next) {

    if (ctx.isNewInstance) {

      USER.findOne({ 'where': { 'and': [{ 'companyId': ctx.instance.companyId }, { 'username': ctx.instance.username }] } }, function (userErr, userOut) {
        if (userOut) {
          winston.GenerateLog(0, ' Username with same company already exist! : ' + ' ');
          unauthorizedRes('Username with same company already exist!', next);
        } else {

          next();
        }
      });

    } else {
      next();
    }
  });



  function encrypt(text) {
    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'taptrack')
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
  }

  function decrypt(text) {
    var crypto = require('crypto');
    var decipher = crypto.createDecipher('aes-256-cbc', 'taptrack')
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
  }

  USER.afterRemote('create', function (context, userInstance, next) {
    if (userInstance) {
      /** mail content after user creation */
      var space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      var space1 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
      var options = {
        type: 'email',
        to: userInstance.email,
        from: config.emailConfig.user,
        subject: 'TAPTRACK Account Registration Detail.',
        html: 'Dear ' + userInstance.userFirstName + ' ' + userInstance.userLastName + ',' + '<br>' + '<br>' +
          'Below is your TAPTRACK account information. Please keep this email for future reference.' + '<br>' +
          'Kindly remember given Company Unique ID while login Once you are approved.' + '<br>' + '<br>' +
          '<b>' + 'First Name' + '</b>' + space + userInstance.userFirstName + '<br>' +
          '<b>' + 'Last Name' + '</b>' + space + userInstance.userLastName + '<br>' +
          '<b>' + 'User Name' + '</b>' + space + userInstance.username + '<br>' +
          '<b>' + 'Password' + '</b>' + '&nbsp;' + space + userInstance.password + '<br>' +
          '<b>' + 'User Type' + '</b>' + space + userInstance.userType + '<br>' +
          '<b>' + 'Street' + '</b>' + '&nbsp;' + '&nbsp;' + '&nbsp;' + '&nbsp;&nbsp;&nbsp;&nbsp;' + space + userInstance.UserStreet1 + '<br>' +
          '<b>' + 'City' + '</b>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;&nbsp;' + space + userInstance.userCity + '<br>' +
          '<b>' + 'Zip Code' + '</b>' + '&nbsp;' + space + userInstance.userZip + '<br>' +
          '<b>' + 'Mobile ' + '</b>' + '&nbsp;&nbsp;&nbsp; ' + space + userInstance.userMobile + '<br>' +
          '<b>' + 'Email ' + '</b>' + '&nbsp;&nbsp;&nbsp;&nbsp; ' + space + userInstance.email + '<br>' +
          '<b>' + 'Company Name ' + '</b>' + space1 + userInstance.companyName + '<br>' + '<br>' +
          // '<b>' + 'Company Unique ID' + '</b>' + space1 + userInstance.uniqueId + '<br>' + '<br>' +
          '<b>' + '<p>Thanks &amp; Regards,</p> \n <p>TAPTRACK Team</p> <br>'
      };
      winston.GenerateLog(2, ' Successfully username : ' + userInstance.username + ' created and email sent to ' + userInstance.email + ' ');
      /** sendMail method will send mail with above input */
      transporter.sendMail(options);
      next();
    }
    else {
      userProfile('Profile not created', next())
    }

  });
  var transporter = nodemailer.createTransport(smtpTransport({
    host: "smtp.office365.com",
    port: 587,
    secure: false,

    auth: {
      user: config.emailConfig.user,
      pass: config.emailConfig.pass
    }

  }));


  var unauthorizedRes = function (error, next) {
    var adminAuthorizationError = new Error(error);
    adminAuthorizationError.statusCode = 422;
    adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };
  var successMethod = function (message, next) {
    next(null, message);
  };

  /** userProfile method will send error with statuscode 500 */
  var userProfile = function (error, next) {
    var adminAuthorizationError = new Error(error);
    adminAuthorizationError.statusCode = 500;
    adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };


  // remote method to get getLocationOrUserCode
  /** getLocationOrUserCode method will return users or locations depends on the input
   * @constructor
   * @param {String} orderBy - is a filter
   * @param {String} company - is a variable for assigning company model 
    * @param {function} callBc - deals with response
   */
  USER.getLocationOrUserCode = function (input, cb) {
    var company = server.models.Company;
    /** findOne method will fetch one record from company table based on uniqueId */
    company.findOne({ 'where': { 'uniqueId': input.uniqueId } }, function (companyErr, companyOut) {

      if (companyOut != null) {
        /** orderBy is a variable is used as a query for userLastName and userFirstName */
        var orderBy = 'userLastName,userFirstName';
        /** if location data has to fetch query for username */
        if (input.locationFlag === 1) {
          orderBy = 'username';
        }
        /** find method will return records for given locationFlag and companyId which has approved user status with order flag */
        USER.find({
          'where': { 'and': [{ 'locationFlag': input.locationFlag }, { 'companyId': companyOut.companyId }, { 'userStatus': '1' }, { 'userApproved': '1' }] },
          'order': orderBy
        }, function (err, out) {
          winston.GenerateLog(2, 'getLocationOrUserCode Method: List of Users/Locations for given companyId are ' + out.length);
          cb(null, out);
        })

      } else {
        winston.GenerateLog(0, 'getLocationOrUserCode Method: Company ID Doesnot exist');
        cb('Company ID doesnot exist', null);
      }

    })

  }
  /** getLocationOrUserCode method declaration */
  USER.remoteMethod('getLocationOrUserCode', {
    description: 'To getLocationOrUserCode',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getLocationOrUserCode',
      verb: 'POST',
    },
  });


  /** get list of users method : 
   * @constructor : will return list of users depends on given companyId
   *  @param {function} callBc - deals with response
   */

  USER.getListOfUsers = function (input, cb) {
    // if (input.companyId)
    // {
    // USER.find({ 'where': {and:[{ 'locationFlag': 0 },{'companyId':input.companyId}]} }, function (err, out) {
      /** find method will return list of records for given companyId and user status and approved flag should be 1 and locationFlag will be 0 */
    USER.find({ 'where': { and: [{ 'locationFlag': 0 }, { 'companyId': input.companyId }, { 'userApproved': 1 }, { 'userStatus': 1 }] } }, function (err, out) {
      if (err) {
        winston.GenerateLog(0, ' getListOfUsers Method : Error while calling service getListOfUsers : ' + err + ' ');
      } else {
        winston.GenerateLog(2, 'getListOfUsers with companyId : ' + input.companyId + " are: " + out.length + ' ');
      }

      cb(null, out);
    });
    // }
    // else
    // {
    //   cb('invalid',null);
    // }
  }

  /** getListOfUsers method declaration */
  USER.remoteMethod('getListOfUsers', {
    description: 'To getListOfUsers',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getListOfUsers',
      verb: 'post',
    },
  });

  /** 
   * remote method to get user Names
   * @constructor
   * This method will give userName for given username and companyId
   * @param {String}  company - will access company Model
   * cb - deals with final response
   */
  USER.getUserName = function (input, cb) {
    
    var company = server.models.Company;
    company.findOne({ 'where': { 'uniqueId': input.uniqueId } }, function (companyErr, companyOut) {
      if (companyOut != null) {
        //There is no validation on the location flag as binders can be moved between users and locations
        USER.findOne({ 'where': { 'and': [{ 'username': { "regexp": '^' + input.username + '$' } }, { 'companyId': companyOut.companyId }] } }, function (err, out) {
          // USER.findOne({ 'where': { 'and': [{ 'userName':  input.username  }, { 'companyId': companyOut.companyId },{ 'locationFlag':0 }] } }, function (err, out) {
          if (out != null) {
            cb(null, out);
          } else {
            winston.GenerateLog(0, 'getUserName : Username is not valid');
            /** callback will return error if username is not valid */
            cb('Username is not valid', null);
          }
        })
      } else {
        winston.GenerateLog(0, 'getUserName : Company Id does not exist');
        /** callback will return error if companyId does not exist */
        cb('Company Id does not exist', null);
      }

    })
  }
  /** getUserName method declaration */
  USER.remoteMethod('getUserName', {
    description: 'To getUserName',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getUserName',
      verb: 'post',
    },
  });

  /** getPendingUsers method : will return pending userss for given companyId
   * @constructor
   * cb - callback to return final output
   */
  USER.getPendingUsers = function (input, cb) {
    /** find method will return users for given companyId and userApproved flag to be 0 */
    USER.find({ 'where': { 'and': [{ 'userApproved': '0' }, { 'companyId': input.companyId }] } }, function (err, out) {
      cb(null, out);
    });

  }

/** getPendingUsers method declaration */
  USER.remoteMethod('getPendingUsers', {
    description: 'To getPendingUsers',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getPendingUsers',
      verb: 'post',
    },
  });

/** getSuperiorGridUsers : will return list of users for given companyId
 * @constructor
 * cb will return final output.
 */
  USER.getSuperiorGridUsers = function (input, cb) {
    /** find method will return list of users for given companyId */
    USER.find({ 'where': { 'companyId': input.companyId } }, function (err, out) {
      cb(null, out);
    });

  }
/** getSuperiorGridUsers method declaration */
  USER.remoteMethod('getSuperiorGridUsers', {
    description: 'To getSuperiorGridUsers',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getSuperiorGridUsers',
      verb: 'post',
    },
  });

/** searchUsers method will return list of users for given searchText
 * @constructor
 * cb will return final output for given input
 */
  USER.searchUsers = function (input, cb) {

    USER.find({ 'where': { and: [{ 'userName': { "regexp": input.searchText.toLowerCase() } }, { "locationFlag": 0 }] }, 'fields': { id: 1, userName: 1, companyId: 1 } }, function (err, out) {

      cb(null, out);
    });

  }
/** searchUsers method declaration */
  USER.remoteMethod('searchUsers', {
    description: 'To searchUsers',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/searchUsers',
      verb: 'post',
    },
  });

/** forgotPassword method : will send an email to given username with password
 * @constructor
 * @param {object} options - will form an object for an email
 * cb will return final output
 */
  USER.forgotPassword = function (input, cb) {
/** findOne method will return single object from user table for given companyId and username */
    USER.findOne({ 'where': { 'and': [{ 'companyId': input.companyId }, { 'username': input.username }, { 'userApproved': 1 }, { 'userStatus': 1 }] } }, function (userErr, userOut) {
      if (userOut) {
        /** objecr for email sent */
        var options = {
          type: 'email',
          to: userOut.email,
          from: config.emailConfig.user,
          subject: 'TAPTRACK Account Details.',
          html: 'Dear ' + userOut.userFirstName + ' ' + userOut.userLastName + ',' + '<br>' + '<br>' +
            'Below is your TAPTRACK account information. Please keep this email for future reference.' + '<br>' + '<br>' +
            '<b>' + 'User Name : ' + input.username + '<br>' +
            '<b>' + 'Password : ' + (userOut.password) + '<br>' + '<br>' + '<br>' +
            '<b>' + '<p>Thanks &amp; Regards,</p> \n <p>TAPTRACK Team</p> <br>'
        }
        // decrypt(userOut.password)
        /** sendMail object will send email for given email */
        transporter.sendMail(options);

        userOut.successMsg = 'mail sent successfully';
        /** cb will return finalout */
        cb(null, userOut)
      }
      else {
        /** if any error cb will return error */
        var errMsg = 'no user found';
        cb(errMsg, null);
      }
    })

  }
  /** forgotPassword method declaration */
  USER.remoteMethod('forgotPassword', {
    description: 'To forgotPassword',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/forgotPassword',
      verb: 'post',
    },
  });

  // Email Invite from superviser module
  /** inviteEmail method : - will send an email for inviting users for registration
   * @constructor
   * @param {object} userDetails - stores users data
   * @param {object} mailOptions - input for email
   * @param {String} name - forms userFirstName and userLastName
   * cb - callback will handle the response
   */
  USER.inviteEmail = function (input, cb) {
    var userInfo = server.models.Users;
    var company = server.models.Company;
    var userDetails;
    var companyDetails;

    /** findOne method will return one user for given userId */
    userInfo.findOne({ 'where': { 'id': input.userId } }, function (err1, out1) {
      userDetails = out1;
      /** will combine as one string of firstname and lastname */
      var name = userDetails.userFirstName + ' ' + userDetails.userLastName;
      company.findOne({ 'where': { 'companyId': out1.companyId } }, function (err2, out2) {
        companyDetails = out2;
        var mailOptions = {
          type: 'email',
          from: config.emailConfig.user,
          to: input.toWhom,
          subject: name + ' has invited you to open a new Tap Track account in his company ' + out2.companyName,
          html: `<p>Dear Friend,</p>\n\n<p>${name} has invited you to open a new TAPTRACK account in his company &quot;${out2.companyName}&quot;.<br />\n<br />\nTo accept this invitation and register for your account, visit Here<br />\n<a data-saferedirecturl=\"https://www.google.com/url?q=${config.emailConfig.uiUrl}/#/inviteUserRegistration?companyUniqueId%3D${out2.uniqueId}&amp;source=gmail&amp;ust=1571985017034000&amp;usg=AFQjCNEcJBwlewUrBfgiQJ3iimGQHjTQkQ\" href=\"${config.emailConfig.uiUrl}/#/inviteUserRegistration?companyUniqueId=${out2.uniqueId}\" target=\"_blank\">https://taptrac.com/<wbr />inviteUserRegistration?<wbr />companyUniqueId=${out2.uniqueId}</a></p>\n\n<p><br />\nOnce you create your account, ${userDetails.userName} will be notified with your new TAPTRACK account so he/she can approve you.<br />\n&nbsp;</p>\n\n<p>Thanks &amp; Regards,</p>\n\n<p>TAPTRACK Team</p>\n\n`
        };
        /** sendMail will send an email to given user */
        transporter.sendMail(mailOptions);
        winston.GenerateLog(2, 'inviteEmail : user with Id' + input.userId + ' Invited user with emailId ' + input.toWhom + ' ');
        input.createdDate = new Date();

        //   ctx.instance.toWhom = "test" ;
        input.status = 1;
        cb(null, input)
      });

    });
  }
/** inviteEmail method declaration */
  USER.remoteMethod('inviteEmail', {
    description: 'To inviteEmail',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/inviteEmail',
      verb: 'post',
    },
  });



};
