'use strict';

var server = require("../../server/server");
var winston = require('../../winstonsockets');
// exporting function to use it in another modules if requires
module.exports = function (COMPANY) {
//getCompanyDetails method : to extract company details like companyName and companyId with given uniqueId
    COMPANY.getCompanyDetails = function (input, cb) {
        // findOne method will get one record from company table for given uniqueId
        COMPANY.findOne({ 'where': { 'uniqueId': input.uniqueId } }, function (err, out) {
            // cb is a callback which will return output 
            cb(null, out)
        })
    }
    //getCompanyDetails remote method declaration
    COMPANY.remoteMethod("getCompanyDetails", {
        description: "getCompanyDetails",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/getCompanyDetails",
            verb: "POST" 
        }
    });

    //itemValidation method : it will validate itemUniqueId is exist in a given companyId and it should be unique for that company.
    COMPANY.itemValidation = function (input, cb) {
        // declaring item variable and accessing Item model.
        var item = server.models.Item;
       // findOne method will fetch one record from item model for given itemUniqueId and companyId
        item.findOne({ 'where':{'and' : [ { itemUniqueId: input.itemUniqueId }, { companyId: input.companyId } ]}}, function (err, out) {
            if (out) {
                // will give out object if itemUniqueId exists.
                winston.GenerateLog(0, 'itemValidation Method : UniqueId '+ input.itemUniqueId +' is already exist for the same company ' + input.companyId+ ' ');
                // callback will return error if itemUniqueId is exist.
                cb('UniqueId is already exist for the same company', null)
            } else {
                // callback will return null if itemUniqueId does not exist for given companyId
                cb(null, null)
            }
        })
    }
    // itemValidation method declaration
    COMPANY.remoteMethod("itemValidation", {
        description: "itemValidation",
        returns: {
            type: "object",
            root: true
        },
        accepts: [
            {
                arg: "data",
                type: "object",
                http: {
                    source: "body"
                }
            }
        ],
        http: {
            path: "/itemValidation",
            verb: "POST"
        }
    });
};
