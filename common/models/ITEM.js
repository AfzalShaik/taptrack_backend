"use strict";
var async = require("async");
var server = require("../../server/server");
var async = require("async");
const fs = require("fs");
const path = require("path");
var enteredUId;
var winston = require('../../winstonsockets');
var moment = require('moment');

// exporting function to use it in another modules if requires
module.exports = function (Item) {
  // Item.validatesUniquenessOf('itemUniqueId')
  /** findUniqueItems method : find the item for given uniqueId
   * @constructor
   * @param {String} check - input of itemUniqueId
   */

  Item.observe('before save', function (ctx, next) {
    var datetime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
    var dateStr = moment(datetime, 'MM/DD/YYYY, HH:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
    if (ctx.isNewInstance) {
          //convert the Date format to required DB date format 
          // 12/9/2019, 1:26:52 AM - Newyork format 
          // 2019-12-09 00:27:34 - required format 
          // var datetime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
          // var dateStr = moment(datetime, 'MM/DD/YYYY, HH:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
          ctx.instance.updatedDate = dateStr.toString();
          ctx.instance.addItem = dateStr.toString();
          next();

      }else {
        ctx.data['addItem'] = dateStr.toString();
        ctx.data['updatedDate'] = dateStr.toString();
          // ctx.data['transSucceedTime'] = new Date();
          next();
      }
  });
  Item.findUniqueItems = function (input, cb) {
    if (input.itemUniqueId != null) {
      if (input.userId != null) {
        Item.findOne({
          where: {
            and: [{
              itemUniqueId: input.itemUniqueId
            }, {
              currentOwner: input.userId
            },
            { itemStatusId: 2 }]
          }
        }, function (
          companyErr,
          companyOut
        ) {
            if (companyOut != null) {
              var check = companyOut.itemUniqueId.replace(/[-]/g, "");
              cb(null, {
                itemUniqueId: companyOut.itemUniqueId,
                itemUniqueIdWithoutD: check,
                details: companyOut,
                status: true
              });

            } else {
              cb('No Item Exists with this ID', null)
            }
          });
      } else {
        Item.findOne({
          where: {
            and: [
              { itemUniqueId: input.itemUniqueId },
              { itemStatusId: 2 }
            ]
          }
        }, function (
          companyErr,
          companyOut
        ) {
            if (companyOut != null) {
              cb(null, {
                itemUniqueId: companyOut.itemUniqueId,
                details: companyOut,
                status: true
              });
            } else {
              cb(null, {
                itemUniqueId: input.itemUniqueId,
                details: "back to login",
                status: false
              });
            }
          });
      }
    } else {
      cb('Id is required')
    }
  };

  /** findUniqueItems method declaration */
  Item.remoteMethod("findUniqueItems", {
    description: "findUniqueItems",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/findUniqueItems",
      verb: "POST"
    }
  });


  /** findUniqueItemsByCompany method : it will validate the given itemUniqueId for given companyId
   * @constructor
   * cb- callback will return the output if it matches the input criteria
   */
  Item.findUniqueItemsByCompany = function (input, cb) {
    if (input.itemUniqueId && input.companyId) {
      /**  findOne method will validate the results if input criteria matches*/
      Item.findOne({
        where: {
          and: [
            { itemUniqueId: input.itemUniqueId },
            { companyId: input.companyId }
          ]
        }
      }, function (
        companyErr,
        companyOut
      ) {
          if (companyOut) {
            winston.GenerateLog(2, 'findUniqueItemsByCompany Method :  Given ItemUnique Id ' + input.itemUniqueId + ' is valid for companyId : ' + input.companyId + ' ');
            cb(null, companyOut)
          }
          else {
            winston.GenerateLog(0, ' findUniqueItemsByCompany Method : item not found : ' + ' ');
            cb('item not found', null);
          }
        });
    }
    else {
      cb('inputs are not proper', null);
    }
  }


  /** findUniqueItemsByCompany method declaration */
  Item.remoteMethod("findUniqueItemsByCompany", {
    description: "findUniqueItemsByCompany",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/findUniqueItemsByCompany",
      verb: "POST"
    }
  });

  // item and tag cration

  function generate(n) {
    var add = 1, max = 12 - add;   // 12 is the min safe number Math.random() can generate without it starting to pad the end with zeros.

    if (n > max) {
      return generate(max) + generate(n - max);
    }

    max = Math.pow(10, n + add);
    var min = max / 10; // Math.pow(10, n) basically
    var number = Math.floor(Math.random() * (max - min + 1)) + min;

    return ("" + number).substring(add);
  }

  var uninput;
  /** createItem method : will crate items for given input or it will generate items
   * @constructor
   * @param {object} tagInput - input for tag to create
   * @param {Array} itemArrList - list of itemUniqueId's array
   * @param {String} uniqueId - generated uniqueId
   * @param {object} itemInput - input of the item
   * cb - callback will return the output
   */
  Item.createItem = function (input, cb) {
    uninput = input;
    var tag = server.models.Tag;
    var itemUniqueId;
    var uniqueId;
    let tagInput = {};
    tagInput = {
      tagName: input.tagName,
      tagDesc: input.tagDesc
    };
    var itemArrList = [];
    var inputArray = [];
    var lenn;
    winston.GenerateLog(2, ' createItem Method: Given input for Creating item is  : ' + input.toString() + ' ');
    /** tag to create for given input */
    tag.create(tagInput, function (tagErr, tagOut) {
      if (tagOut) {
        winston.GenerateLog(2, 'createItem Method: Successfully Tag created with name  : ' + tagOut.tagName + ' ');
        /** validate if uniqueCheckbox is exist it will create generated items */
        if (input.uniqueCheckbox) {
          var len = input.package ? 1 : 20;
          for (var i = 0; i < len; i++) {
            var maxLength = input.regexFormat;
            var srcHyphen = /(?<=([^-]*-){1}).*/.exec(maxLength);

            if (srcHyphen) {
              lenn = maxLength.toString().length;

              itemUniqueId = generate(parseInt(lenn) - 1);

              if (srcHyphen.index == 2) {
                uniqueId = itemUniqueId.replace(/(\d{1})/, "$1-");
                itemArrList.push(uniqueId);
              }
              if (srcHyphen.index == 3) {
                uniqueId = itemUniqueId.replace(/(\d{2})/, "$1-");
                itemArrList.push(uniqueId);
              }
              if (srcHyphen.index == 4) {
                uniqueId = itemUniqueId.replace(/(\d{3})/, "$1-");

                itemArrList.push(uniqueId);
              }
              if (srcHyphen.index == 5) {
                uniqueId = itemUniqueId.replace(/(\d{4})/, "$1-");
                itemArrList.push(uniqueId);
              }
              if (srcHyphen.index == 6) {
                uniqueId = itemUniqueId.replace(/(\d{5})/, "$1-");
                itemArrList.push(uniqueId);
              }

              var one = srcHyphen.index - 1;


              // uniqueId = itemUniqueId.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2$3");
            } else {
              lenn = maxLength.toString().length;
              itemUniqueId = generate(parseInt(lenn));
              // itemUniqueId = generate(maxLength.toString().length);
              itemArrList.push(itemUniqueId);
            }
            winston.GenerateLog(2, 'createItem Method: list of  generated Uniqueid  : ' + itemArrList.length + ' ');
            // var n = Math.floor(Math.random() * 10000000000);
            // var nn = n.toString();
            // itemUniqueId = nn.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2$3");
            // itemArrList.push(itemUniqueId);
          }

        }

        itemArrList = itemArrList.length > 0 ? itemArrList : input.itemArray;

        if (itemArrList.length > 0) {
          Number.prototype.padLeft = function (base, chr) {
            var len = String(base || 10).length - String(this).length + 1;
            return len > 0 ? new Array(len).join(chr || "0") + this : this;
          };

          for (var j = 0; j < itemArrList.length; j++) {
            /* var d = new Date(),
               dformat = [
                 d.getFullYear(),
                 (d.getMonth() + 1).padLeft(),
                 d.getDate().padLeft()
               ].join("-") +
                 " " + [
                   d.getHours().padLeft(),
                   d.getMinutes().padLeft(),
                   d.getSeconds().padLeft()
                 ].join(":");*/
            //convert the Date format to required DB date format
            //12/9/2019, 1:26:52 AM - Newyork format
            //2019-12-09 00:27:34 - required format
            var datetime = new Date().toLocaleString("en-US", { timeZone: "America/New_York" });
            var dateStr = moment(datetime, 'MM/DD/YYYY, HH:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
            var itemInput = {
              //    'itemId' : 1737206,
              tagId: tagOut.tagId,
              itemTypeId: input.itemType,
              userId: input.userId,
              transactionId: null,
              initTransactionId: null,
              currentOwner: input.userId,
              itemStatusId: 1,
              companyId: input.companyId,
              addItem: dateStr.toString(),
              itemUniqueId: itemArrList[j],
              reciever: input.reciever
            };
            inputArray.push(itemInput);
          }
          if (inputArray.length > 0) {
            /** to create multiple items */
            async.map(inputArray, createItems, function (itemError, itemOutput) {
              if (cleanArray(itemOutput).length > 0) {
                /** if transFlag is true it will create transactions for created items */
                if (input.transFlag) {
                  /** to create transactions */
                  createTransactions(input, cleanArray(itemOutput), function (
                    transErr,
                    transOut
                  ) {

                    if (cleanArray(transOut).length == 0) {
                      winston.GenerateLog(0, 'createItem Method: No Items created items ' + ' ');
                      cb("No Items created items", null);
                    } else if (cleanArray(transOut).length < inputArray.length) {
                      winston.GenerateLog(0, 'createItem Method: Only few items got created ' + ' ');
                      cb("Only few items got created", null);
                    }
                    else {
                      cb(null, cleanArray(itemOutput));
                    }
                  });
                } else {

                  if (cleanArray(itemOutput).length == 0) {
                    winston.GenerateLog(0, 'createItem Method: No Items created items ' + ' ');
                    cb("No Items created items", null);
                  } else if (cleanArray(itemOutput).length < inputArray.length) {
                    winston.GenerateLog(0, 'createItem Method: Only few items got created ' + ' ');
                    cb("Only few items got created", null);
                  }
                  else {
                    cb(null, cleanArray(itemOutput));
                  }
                }
              } else {
                winston.GenerateLog(0, 'createItem Method: Items are not created   : ' + ' ');
                cb("Items are not created", null);
              }
            });
          } else {
            winston.GenerateLog(0, 'createItem Method: Item has not created   : ' + ' ');
            cb("Item has not created", null);
          }

        } else {
          winston.GenerateLog(0, 'createItem Method: Atleast one uniqueId has to be present   : ' + ' ');
          cb("Atleast one uniqueId has to be present", null);
        }
      } else {
        winston.GenerateLog(0, 'createItem Method: Error while Creating tag  ' + tagErr);
        cb(tagErr, null);
      }
    });
  };
  /** createTransactions method will create transactions for created items */
  function createTransactions(input, itemOutput, Cbc) {
    var transArr = [];
    for (var k = 0; k < itemOutput.length; k++) {
      var transObj = {};
      /** transObje is a input object to create transaction */
      transObj = {
        reciever: input.reciever,
        sender: input.userId,
        nextTransactionId: 0,
        transStatusId: 2,
        // transactionTime: dformat,
        itemId: itemOutput[k].id,
        transMailFlag: 0,
        deletedFlag: 0,
        addedByAdminFlag: 1,
        // transSucceedTime: dformat,
        transComment: "Transaction during item status change by admin."
      };
      transArr.push(transObj);
    }
    async.map(transArr, createTrans, function (transError, transOutput) {
      Cbc(null, transOutput);
    });
  }

  /**  createTrans to create transactions*/
  function createTrans(obj, transCB) {
    var transcation = server.models.Transactions;
    var userModel = server.models.Users;
    userModel.findOne({ 'where': { 'and': [{ 'username': obj.reciever }, { companyId: uninput.companyId }] } }, function (
      userErr,
      userOut
    ) {
      if (userErr) {
        winston.GenerateLog(0, 'createItem Method: Error while fetching userinfo  ' + userErr + ' ');
      }
      var transObj = {};
      transObj = obj;
      transObj["reciever"] = userOut.id;
      transObj["addedByAdminFlag"] = userOut.userTypeId == 1 ? 1 : 0;

      transcation.create(transObj, function (err, out) {
        // console.log('ttttttttttttttttttttttttttttttttttt ', err, out);
        if (err) {
          // console.log('eeeeeeeeeeeeeeeeeeeeee ', err, transObj);
          winston.GenerateLog(0, 'createItem Method: Error while fetching transcation  ' + err + ' ');
          rollBack(transObj.itemId, null, function (rollErr, rollOut) {
            transCB(null, null);
          });

        } else {
          winston.GenerateLog(2, 'createItem Method: successfully transcation created with id ' + out.transactionId + ' ');
          updateItem(out, function (upErr, upResp) {
            transCB(null, out);
          })
        }
      });
    });
  }

  function rollBack(itemId, transactionId, cb) {
    if (transactionId) {
      var transactions = server.models.Transactions;
      transactions.destroyById(transactionId, function (transErr, transOut) {
        if (transErr) {
          cb(transErr, null)
        } else {
          var item = server.models.Item;
          Item.destroyById(itemId, function (itemErr, itemOut) {
            if (itemErr) {
              cb(itemErr, null)
            } else {
              cb(null, itemOut)
            }
          })
        }
      })
    }
    else {
      var item = server.models.Item;
      Item.destroyById(itemId, function (itemErr, itemOut) {

        if (itemErr) {
          cb(itemErr, null)
        } else {
          cb(null, itemOut)
        }
      })
    }
  }

  /** updateItem will udpate the item with statusId 2 and reciever and transaction details */
  function updateItem(obj, callBC) {
    Item.findOne({
      'where': {
        'itemId': obj.itemId
      }
    }, function (itemErr, itemOut) {

      var objj = {};
      objj = itemOut;
      objj['itemStatusId'] = 2;
      objj['currentOwner'] = obj.reciever;
      objj['transactionId'] = obj.transactionId;
      objj['initTransactionId'] = obj.transactionId;
      // objj['updatedDate'] = new Date();
      objj['updatedBy'] = obj.sender;
      var transaction = server.models.Transactions;
      /** updateAttributes method will update the record */
      itemOut.updateAttributes(objj, function (err, resp) {
        // console.log('uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu ', err, resp, objj);
        if (err) {
          rollBack(objj.itemId, objj.transactionId, function (rollErr, rollOut) {
            callBC(null, null);
          });
        } else {
          winston.GenerateLog(2, 'createItem Method: successfully item got updated with transactionId ' + obj.transactionId + ' ');
          callBC(null, resp)
        }

      })

    })
  }
  /**  createItems method : will create list of items */
  function createItems(obj, callBack) {
    Item.findOne({ 'where': { 'and': [{ 'itemUniqueId': obj.itemUniqueId }, { 'companyId': obj.companyId }] } }, function (uErr, uOut) {
      // Item.validateItemId(obj, function(error, output) {

      // })

      if (uOut == null) {
        if (uninput.uniqueCheckbox && uOut) {
          var itemUniqueId;
          var uniqueId;
          var len = 1;
          var lenn;
          for (var i = 0; i < len; i++) {
            var maxLength = uninput.regexFormat;

            var srcHyphen = /(?<=([^-]*-){1}).*/.exec(maxLength);
            if (srcHyphen) {
              lenn = maxLength.toString().length;
              itemUniqueId = generate(parseInt(lenn) - 1);
              if (srcHyphen.index == 2) {
                uniqueId = itemUniqueId.replace(/(\d{1})/, "$1-");
              }
              if (srcHyphen.index == 3) {
                uniqueId = itemUniqueId.replace(/(\d{2})/, "$1-");
              }
              if (srcHyphen.index == 4) {
                uniqueId = itemUniqueId.replace(/(\d{3})/, "$1-");
              }
              if (srcHyphen.index == 5) {
                uniqueId = itemUniqueId.replace(/(\d{4})/, "$1-");
              }
            } else {
              itemUniqueId = generate(maxLength.toString().length);
            }
            obj['itemUniqueId'] = itemUniqueId;
            // obj['updatedDate'] = new Date();
            obj['updatedBy'] = obj.sender;
          }
          Item.create(obj, function (itemErr, itemOut) {
            if (itemErr) {
              winston.GenerateLog(0, 'createItem Method: Error whilecreating item   : ' + itemErr + ' ');
            }
            winston.GenerateLog(2, 'createItem Method: successfully item crated with uniqueId  : ' + obj.itemUniqueId + ' ');
            callBack(null, itemOut);
          });
        } else {
          // obj['updatedDate'] = new Date();
          obj['updatedBy'] = obj.sender;
          Item.create(obj, function (itemErr, itemOut) {
            if (itemErr) {
              winston.GenerateLog(0, 'createItem Method: Error whilecreating item   : ' + itemErr + ' ');
            }
            winston.GenerateLog(2, 'createItem Method: successfully item crated with uniqueId  : ' + obj.itemUniqueId + ' ');
            // "index": {"unique": true},

            callBack(null, itemOut);
          });
        }

        // Item.create(obj, function (itemErr, itemOut) {

        //   callBack(null, itemOut);
        // });
      } else {
        callBack(null, null)
      }
    })

  }
  /** createItem method declaration */
  Item.remoteMethod("createItem", {
    description: "createItem",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/createItem",
      verb: "POST"
    }
  });

  var itemRelation = {
    include: [
      {
        relation: 'tag',
        scope: {
          fields: ['tagId', 'tagName', 'tagDesc']
        }
      },
      {
        relation: 'itemtype',
        scope: {
          fields: ['itemType', 'itemTypeId']
        }
      },
      {
        relation: 'itemstatus',
        scope: {
          fields: ['description', 'itemStatusId']
        }
      }, {
        relation: 'currentOwnerx',
        scope: {
          fields: ['userName', 'userFirstName', 'userLastName']
        }
      }
    ]
  }

  // RemoteMethod for getting item list
  var getTagIds = function (filter, fcb) {
    server.models.Tag.find(filter, function (err, tagResults) {
      if (err) fcb(err)
      else {
        var tagIds = []
        async.each(tagResults, function (tag, scb) {
          tagIds.push(tag.tagId)
          scb()
        }, function () {
          fcb(null, tagIds)
        })
      }
    })
  }
  var getQueryFromSubModel = function (tagName, tagDesc, itemUniqId, input, fcb) {
    var tagFilter = {
      where: {
        and: []
      }
    }
    if (tagName) {
      generatedQueryBasedOnType(tagName, ' ', function (err, filter) {
        tagFilter.where.and.push({ tagName: filter.where.tagName })
      })
    }
    if (tagDesc) {
      generatedQueryBasedOnType(tagDesc, ' ', function (err, filter) {
        tagFilter.where.and.push({ tagDesc: filter.where.tagDesc })
      })
    }

    getTagIds(tagFilter, function (err, tagIds) {
      if (err) fcb(err)
      else {
        var finalFilter = {
          where: {
            and: [
              { tagId: { inq: tagIds } },
              { companyId: input.companyId },
              { itemStatusId: 1 },
            ]
          }
        }
        if (itemUniqId) {
          generatedQueryBasedOnType(itemUniqId, '', function (err, filter) {
            finalFilter.where.and.push({ itemUniqueId: filter.where.itemUniqueId })
            finalFilter.where.and.push({ itemStatusId: 1 })
          })
        }
        fcb(null, finalFilter)
      }
    })
  }
  var generatedQueryBasedOnType = function (input, companyId, fcb) {
    var filter = {
      where: {
        companyId: companyId,
        itemStatusId: 1
      }
    }
    if (input.type == 'startsWith') {
      filter.where[input.field] = { like: input.value + '%' }
      fcb(null, filter)
    } else if (input.type == 'endsWith') {
      filter.where[input.field] = { like: '%' + input.value }
      fcb(null, filter)
    } else if (input.type == 'exactly') {
      filter.where[input.field] = { like: input.value }
      fcb(null, filter)
    } else {
      filter.where[input.field] = { like: '%' + input.value + '%' }
      fcb(null, filter)
    }
  }
  var generateSearchQuery = function (input, fcb) {
    var searchQuery = input.searchQuery
    if (searchQuery.itemUniqueId.value || searchQuery.tagDesc.value || searchQuery.tagName.value) {
      if (searchQuery.itemUniqueId.value && searchQuery.tagDesc.value && searchQuery.tagName.value) {
        getQueryFromSubModel(searchQuery.tagName, searchQuery.tagDesc, searchQuery.itemUniqueId, input, fcb)
      } else if (searchQuery.itemUniqueId.value && (searchQuery.tagDesc.value || searchQuery.tagName.value)) {
        getQueryFromSubModel(searchQuery.tagName, searchQuery.tagDesc, searchQuery.itemUniqueId, input, fcb)
      } else if (searchQuery.tagDesc.value && searchQuery.tagName.value) {
        getQueryFromSubModel(searchQuery.tagName, searchQuery.tagDesc, undefined, input, fcb)
      } else if (searchQuery.itemUniqueId.value) {
        generatedQueryBasedOnType(searchQuery.itemUniqueId, input.companyId, fcb)
      } else if (searchQuery.tagDesc.value) {
        getQueryFromSubModel(undefined, searchQuery.tagDesc, undefined, input, fcb)
      } else if (searchQuery.tagName.value) {
        getQueryFromSubModel(searchQuery.tagName, undefined, undefined, input, fcb)
      } else {
        var filter = {
          where: {
            and: [
              { companyId: input.companyId },
            ]
          },
          include: ['tag', 'itemtype', 'itemstatus'],
          skip: input.skip,
          limit: input.limit
        }

        fcb(null, filter)
      }
    } else {
      var filter = {
        where: {
          and: [
            { companyId: input.companyId },
            { itemStatusId: 1 }
          ]
        }, include: ['tag', 'itemtype', 'itemstatus'],
        skip: input.skip,
        limit: input.limit
      }

      fcb(null, filter)
    }
  }
  /** getItems method to fetch records as per input query
   * @constructor
   * cb - callback will handle the response
   */
  Item.getItems = function (input, cb) {
    if (input.companyId && typeof (input.skip) == "number" && typeof (input.limit) == "number") {
      /** generateSearchQuery method will return query for given input */
      generateSearchQuery(input, function (err, filter) {
        async.auto({
          itemListCount: function (scb) {
            Item.count(filter.where, function (err, count) {
              if (err) scb(err)
              else scb(null, count)
            })
          },
          itemsList: function (scb) {
            filter['limit'] = input.limit
            filter['skip'] = input.skip
            filter['include'] = itemRelation.include
            Item.find(filter, function (err, itemsList) {
              if (err) scb(err)
              else scb(null, itemsList)
            })
          }
        }, function (err, results) {
          if (err) cb(err)
          else cb(null, results)
        })
      })
    } else {
      cb('Required Felids Are missing...!', null)
    }
  };
  // end getting item list
  var generateQuery = function (input, fcb) {
    if (input.searchQuery.fromDate)
      input.searchQuery.fromDate = formatStartDate(input.searchQuery.fromDate)
    if (input.searchQuery.toDate)
      input.searchQuery.toDate = formatEndDate(input.searchQuery.toDate)

    if (input.searchQuery.itemUniqId && input.searchQuery.fromDate && input.searchQuery.toDate) {
      var filter = {
        where: {
          and: [{
            itemUniqueId: {
              like: input.searchQuery.itemUniqId
            }
          },
          {
            companyId: input.companyId
          },
          {
            addItem: {
              between: [new Date(input.searchQuery.fromDate).toISOString(), new Date(input.searchQuery.toDate).toISOString()]
            }
          }
          ],
        },
      }
      fcb(null, filter)
    } else if (input.searchQuery.itemUniqId) {
      var filter = {
        where: {
          and: [{
            itemUniqueId: {
              like: input.searchQuery.itemUniqId
            }
          },
          {
            companyId: input.companyId
          },
          ],
        }
      }
      fcb(null, filter)
    } else if (input.searchQuery.fromDate && input.searchQuery.toDate) {
      var filter = {
        where: {
          and: [{
            companyId: input.companyId
          },
          {
            addItem: {
              between: [new Date(input.searchQuery.fromDate).toISOString(), new Date(input.searchQuery.toDate).toISOString()]
            }
          }
          ]
        }
      }
      fcb(null, filter)
    } else {
      var filter = {
        where: {
          and: [{
            companyId: input.companyId
          },]
        }
      }
      fcb(null, filter)
    }
  }


  // end getting item list
  // RemoteMethod for getting item list
  /** getCheckoutItems method : to get checkout items for given input */
  Item.getCheckoutItems = function (input, cb) {
    // console.log(JSON.stringify(input))
    if (input.companyId && typeof (input.skip) == "number" && typeof (input.limit) == "number") {
      var relationObject = {
        include: ['tag', 'itemtype', 'itemstatus'],
        skip: input.skip,
        limit: input.limit
      }
      /** generateQuery method to form query for given input */
      generateQuery(input, function (err, filter) {
        filter = Object.assign(filter, relationObject)
        if (input.itemStatusId == 1) {
          filter.where.and.push({
            itemStatusId: 1
          })
        }
        /** find input method will return list of items as per query */
        Item.find(filter, function (err, itemsList) {
          if (err) cb(err)
          else cb(null, itemsList)
        })
      })
    } else {
      cb('Required Felids Are missing...!', null)
    }
  };

  /** getCheckoutItems method declaration */
  Item.remoteMethod("getCheckoutItems", {
    description: "To getCheckoutItems",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getCheckoutItems",
      verb: "POST"
    }
  });
  /** getItems method declaration */
  Item.remoteMethod("getItems", {
    description: "To getItems",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItems",
      verb: "POST"
    }
  });
  /** getItemsCount method : it will return count for given input
   * @constructor
   * @param {Object} filter - query for given input
   * cb - callback to handle the response
   */
  Item.getItemsCount = function (input, cb) {
    var filter = {
      companyId: input.companyId
    }
    if (input.itemStatusId)
      filter['itemStatusId'] = input.itemStatusId
    if (input.userId)
      filter['currentOwner'] = input.userId
    if (input.companyId) {
      /** count method will return number of items for given companyId, itemStatusId and currentOwner */
      Item.count(filter, function (itemsErr, itemsOut) {
        winston.GenerateLog(2, ' getItemsCount Method: Total Unique Case Binders  : ' + itemsOut + " ");
        cb(null, itemsOut)
      });
    } else {
      cb(new Error("CompanyId missed in GetItemsCount Method"), null);
    }
  };
  /** findexistance method : it will check whether item is exist for given itemUniqueId and companyId y giving count
   * @constructor
   * cb - callback will handle the response
   */
  Item.findexistance = function (input, cb) {
    if (input.itemUniqueId) {
      Item.count({
        itemUniqueId: input.itemUniqueId,
        companyId: input.companyId
      }, function (err, resp) {
        if (err) {
          winston.GenerateLog(0, ' Error while validating item uniqueid : ' + input.itemUniqueId);
          cb(err, null)
        } else {
          winston.GenerateLog(2, 'ItemUniqueId is valid : ' + input.itemUniqueId);
          cb(null, resp)
        }
      });
    } else {

      cb(new Error("required feilds missed"));
    }
  };
  /** validateitems method : it will return the cound for given itemUniqueId,companyId and  itemTypeId
   * @constructor
   * @callback - to handle the response
   */
  Item.validateitems = function (input, cb) {
    if (input) {
      Item.count({ itemUniqueId: input.itemUniqueId, companyId: input.companyId, itemTypeId: input.itemTypeId }, cb);
    } else {
      cb(new Error("required feilds missed"));
    }
  };
  /** validateItemId method : it will count the items for given input
   * @constructor
   * cb - callback will return the number for given itemUniqueid and companyId
   */
  Item.validateItemId = function (input, cb) {
    if (input) {
      Item.count({ itemUniqueId: input.itemUniqueId, companyId: input.companyId }, cb);
    } else {
      cb(new Error("required feilds missed"));
    }
  };
  /** validateItemId method declaration */
  Item.remoteMethod("validateItemId", {
    description: "get validateItemId",
    returns: {
      type: "object",
      root: true
    },
    accepts: [
      {
        arg: "data",
        type: "object",
        http: {
          source: "body"
        }
      }
    ],
    http: {
      path: "/validateItemId",

      verb: "POST"
    }
  });
  /** itemsReportconut method: will return count of items for given itemTypeId and companyId
   * @constructor
   * cb - callback will handle the response
   */
  Item.itemsReportconut = function (input, cb) {
    if (input.itemTypeId) {
      Item.count({
        itemTypeId: input.itemTypeId,
        companyId: input.companyId
      }, cb);
    } else {
      cb(new Error("required feilds missed"));
    }
  };
  /** getItemsCount method declaration */
  Item.remoteMethod("getItemsCount", {
    description: "get record count by companyId for pagination",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItemsCount",
      verb: "POST"
    }
  });
  /** itemsReportconut method declaration */
  Item.remoteMethod("itemsReportconut", {
    description: "get itemsReportconut",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/itemsReportconut",
      verb: "POST"
    }
  });
  /** findexistance method declaration */
  Item.remoteMethod("findexistance", {
    description: "get findexistance",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/findexistance",
      verb: "POST"
    }
  });
  /** validateitems method declaration */
  Item.remoteMethod("validateitems", {
    description: "get validateitems",
    returns: {
      type: "object",
      root: true
    },
    accepts: [
      {
        arg: "data",
        type: "object",
        http: {
          source: "body"
        }
      }
    ],
    http: {
      path: "/validateitems",
      verb: "POST"
    }
  });

  /** binderInventoryReport method : it will return the reports for given companyId and itemUniqueId
   * @constructor
   * @callback - to handle the response
   */
  Item.binderInventoryReport = function (input, cb) {
    if (input.itemUniqueId) {
      winston.GenerateLog(2, 'binderInventoryReport Method: Item Inventory with Id:' + input.itemUniqueId);
      enteredUId = input.itemUniqueId;
      /** find method to fetch items and transactions for given itemUniqueId and companyId with status 2 */
      Item.find({
        where: {
          itemUniqueId: input.itemUniqueId,
          companyId: input.companyId,
          itemStatusId: 2
        },
        include: {
          relation: 'transaction', // include the owner object
          scope: { // further filter the owner object
            order: 'transSucceedTime DESC',
            fields: ['reciever', 'transSucceedTime', 'sender', 'itemId'], // only show two fields
            include: [{ // include orders for the owner
              relation: 'recieverx',
              scope: { // further filter the owner object
                fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
              } // only show two fields
            }, { // include orders for the owner
              relation: 'senderx',
              scope: { // further filter the owner object
                fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
              } // only show two fields
            }]
          }
        },
      }, function (err, out) {
        if (out) {
          var finalArr = [];
          // console.log('OUT ' + JSON.stringify(out));
          var arr = [] = JSON.stringify(out);
          var xyz = [] = JSON.parse(arr);

          // console.log('XYZZZZZZZZZZZZZZZZZZZZZZ ' + JSON.stringify(xyz));
          if (xyz.length > 0) {
            if (xyz[0].transaction.length > 0) {
              winston.GenerateLog(2, 'binderInventoryReport Method: Number Of Transactions for Item Unique Id:' + input.itemUniqueId + ' are : ' + xyz[0].transaction.length);
              for (var i = 0; i < xyz[0].transaction.length; i++) {
                var obj = {};

                var datetime = xyz[0].transaction[i].transSucceedTime;
                // console.log('default ' + xyz[0].transaction);
                // console.log(new Date(datetime).toLocaleString());
                //Date formated for UI display
                var reqDate = new Date(datetime).toISOString().replace('Z', '').replace('T', '  ');
                var date = new Date(xyz[0].transaction[i].transSucceedTime);
                var month = (date.getMonth() + 1);
                var datee = date.getDate();
                /** modeling the object and pushing into the finalArr array. */
                obj['time'] = date.getTime();
                obj['i'] = i;
                obj['itemUniqueId'] = xyz[0].itemUniqueId;
                obj['senderName'] = (xyz[0].transaction[i].senderx) ? ((xyz[0].transaction[i].senderx.locationFlag == 0) ? xyz[0].transaction[i].senderx.userFirstName + ' ' + xyz[0].transaction[i].senderx.userLastName + ' (' + xyz[0].transaction[i].senderx.username + ')' : xyz[0].transaction[i].senderx.location + ' (' + xyz[0].transaction[i].senderx.username + ')'):null;
                obj['transactionSuccessTime'] = reqDate.toString();
                //obj['transactionSuccessTime'] = date.getFullYear() + '-' + ((month.toString().length == 1) ? ('0' + month) : month) + '-' + ((datee.toString().length == 1) ? ('0' +datee) : datee) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();
                obj['recieverName'] = (xyz[0].transaction[i].recieverx) ? ((xyz[0].transaction[i].recieverx.locationFlag == 0) ? xyz[0].transaction[i].recieverx.userFirstName + ' ' + xyz[0].transaction[i].recieverx.userLastName + ' (' + xyz[0].transaction[i].recieverx.username + ')' : xyz[0].transaction[i].recieverx.location + ' (' + xyz[0].transaction[i].recieverx.username + ')'):null;
                finalArr.push(obj)

              }
              /** compareArr will remove the duplicates of transactions */
              compareArr(finalArr, function (err, finalResp) {
                cb(null, finalResp)
              });

              // cb(null, finalArr)
            } else {
              winston.GenerateLog(2, 'binderInventoryReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
              cb(null, [])
            }
          } else {
            winston.GenerateLog(2, 'binderInventoryReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
            cb(null, [])
          }
        } else {
          if (err) {
            winston.GenerateLog(0, 'binderInventoryReport Method: Error while fetching inventory report of ItemUniqueId is' + input.itemUniqueId + ' ' + err);
          }
          winston.GenerateLog(2, 'binderInventoryReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
          cb('No transactions found for itemUniqueId', null)
        }

      });
    } else {
      winston.GenerateLog(0, 'binderInventoryReport Method: Item Unique Id cannot be null:');
      cb('Item Unique Id cannot be null:', null)
    }
  }
  var removeByAttr = function (arr, attr, value) {
    var j = arr.length;
    while (j--) {
      if (arr[j]
        && arr[j].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[j][attr] === value)) {
        arr.splice(j, 1);

      }
    }
    return arr;
  }

  function compareArr(finalArr, callB) {
    var one = [] = finalArr;
    var two = [] = finalArr;
    var three = [] = finalArr;
    one.forEach((e1) => two.forEach((e2) => {

      var firstDate = new Date(e1['transactionSuccessTime']);
      var secondDate = new Date(e2['transactionSuccessTime']);
      var diffMs = (firstDate - secondDate);
      var diff = (firstDate.getTime() - secondDate.getTime()) / 1000;
      // console.log('mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm ',  diff, e1['i'], e2['i']);
      if ((Math.abs(diff) < 50) && e2['i'] != e1['i']) {
        // removeByAttr(three, 'i', e2['i'])
        if (e2['i'] == 0 && e1['i']!=0) {
          removeByAttr(three, 'i', e1['i'])
        } else {
          removeByAttr(three, 'i', e2['i'])
        }

      }
    }
    ))
    callB(null, three)
  }

  function compareArr1(finalArr, callB) {
    var one = [] = finalArr;
    var two = [] = finalArr;
    var three = [] = finalArr;
    one.forEach((e1) => two.forEach((e2) => {
     
        var firstDate = new Date(e1['transactionSuccessTime']);
        var secondDate = new Date(e2['transactionSuccessTime']);
        var diffMs = (firstDate - secondDate);
        var diff = (firstDate.getTime() - secondDate.getTime()) / 1000;
        // if(e2['itemUniqueId'] == '872-2574361') {
          // console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ', finalArr);
        // }
        // console.log('eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ', e1['i'], e2['i'], e2['itemUniqueId'], e1['itemUniqueId'], diff);
        if ((Math.abs(diff) < 50) && e2['i'] != e1['i'] && e2['itemUniqueId'] == e1['itemUniqueId']) {
          // console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ', e2['i'], e2['itemUniqueId']);
          if (e2['i'] == 0 && e1['i']!=0) {
            removeByAttr(three, 'i', e1['i'])
          } else {
            removeByAttr(three, 'i', e2['i'])
          }
          
      }
     
    }
    ))
    // console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ', three);
    callB(null, three)
  }
  /** binderInventoryReport method declaration */
  Item.remoteMethod("binderInventoryReport", {
    description: "To binderInventoryReport",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/binderInventoryReport",
      verb: "POST"
    }
  });


  //===============================================================================================
  Item.bindersProcessedReport =  async function (input, cb) {

    var item = server.models.Item;
    var  filter = {
        where: {
          or: [
            {
              transSucceedTime: {
                between: [input.fromDate, input.toDate]
              },
              companyId: input.companyId,
              itemStatusId: 2,
              // order: 'transSucceedTime DESC',
            }
          ]
        }
      };

     var data =  await item.find({
        where: filter.where,
        "order" : 'addItem DESC',
        limit:5000,
        include: {
          relation: 'transaction', // include the owner object
          scope: { // further filter the owner object
            order: 'transSucceedTime DESC', 
            where: {
              transSucceedTime: {
                between: [input.fromDate, input.toDate]
              }
            },
            fields: ['reciever', 'transSucceedTime', 'sender', 'itemId', 'transactionId'], // only show two fields
            include: [{ // include orders for the owner
              relation: 'recieverx',
              scope: { // further filter the owner object
                fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
              } // only show two fields
            }, { // include orders for the owner
              relation: 'senderx',
              scope: { // further filter the owner object
                fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
              } // only show two fields
            }]
          }
        },
        // order : "addItem DESC"
      });
      // console.log(data);
      if (data) {
        var finalArr = [];
        var binderArr = [];
        var bindersArr = [];
        var arr = [] = JSON.stringify(data);
        var xyz = [] = JSON.parse(arr);
        winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'got total records of  '+xyz.length);
    if (xyz.length > 0) {

      if (xyz.length > 0) {
        winston.GenerateLog(2, 'bindersProcesssedReport Method: Number Of Transactions for Item Unique Id:' + input.itemUniqueId + ' are : ' + xyz[0].transaction.length);
        
        async.each(xyz, function (object, callBC) {
          
          if (object.transaction.length > 0) {
            binderArr = [];
            for (var i = 0; i < object.transaction.length; i++) {
              
              var obj = {};
              var datetime = object.transaction[i].transSucceedTime;
              // console.log(new Date(datetime).toLocaleString());
              //Date formated for UI display
              var reqDate = new Date(datetime).toISOString().replace('Z', '').replace('T', '  ');
              var date = new Date(object.transaction[i].transSucceedTime);
              var month = (date.getMonth() + 1);
              var datee = date.getDate();
              var addDate = new Date(object.addItem);
              obj['transactionId'] = object.transaction[i].transactionId;
              obj['time'] = date.getTime();
              obj['addDate'] = addDate.getTime();
              obj['itemId'] = object.itemId;
              obj['i'] = i;
              obj['itemUniqueId'] = object.itemUniqueId;
              obj['senderName'] = (object.transaction[i].senderx.locationFlag == 0) ? object.transaction[i].senderx.userFirstName + ' ' + object.transaction[i].senderx.userLastName + ' (' + object.transaction[i].senderx.username + ')' : object.transaction[i].senderx.location + ' (' + object.transaction[i].senderx.username + ')';
              obj['transactionSuccessTime'] = reqDate.toString();
              obj['recieverName'] = (object.transaction[i].recieverx.locationFlag == 0) ? object.transaction[i].recieverx.userFirstName + ' ' + object.transaction[i].recieverx.userLastName + ' (' + object.transaction[i].recieverx.username + ')' : object.transaction[i].recieverx.location + ' (' + object.transaction[i].recieverx.username + ')';
              finalArr.push(obj)
              binderArr.push(obj);
              winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'Processed records are   '+finalArr.length);
              if (i == (object.transaction.length - 1)) {
                compareArr1(binderArr, function (err, finalResp) {
                  Array.prototype.push.apply(bindersArr,finalResp); 
                });
                
              }
            }
           
            callBC();
          } 
        })
      winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'Final list of reports are   '+bindersArr.length);
        return Promise.resolve(bindersArr);
      } else {
        winston.GenerateLog(2, 'bindersProcessedReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
        return Promise.resolve([]);
      }
    } else {
      winston.GenerateLog(2, 'bindersProcessedReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
      return Promise.resolve([]);
    }
        
    } else {
        return Promise.resolve([]);
      }


  }
  //===============================================================================================
  Item.bindersProcessedReport1 = function (input, cb) {
    var filter = {};
    if (input.fromDate && input.toDate) {
      winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'given inputs are  '+input.fromDate +' '+ input.toDate);
      filter = {
        where: {
          or: [
            {
              transSucceedTime: {
                between: [input.fromDate, input.toDate]
              },
              companyId: input.companyId,
              itemStatusId: 2,
              // order: 'transSucceedTime DESC',
            }
          ]
        }
      };
    } else {
      filter = {
        where: {
          companyId: input.companyId,
          itemStatusId: 2
        }
      };
    }
    Item.find({
      where: filter.where,
      "order" : 'addItem DESC',
      include: {
        relation: 'transaction', // include the owner object
        scope: { // further filter the owner object
          order: 'transSucceedTime DESC', 
          where: {
            transSucceedTime: {
              between: [input.fromDate, input.toDate]
            }
          },
          fields: ['reciever', 'transSucceedTime', 'sender', 'itemId', 'transactionId'], // only show two fields
          include: [{ // include orders for the owner
            relation: 'recieverx',
            scope: { // further filter the owner object
              fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
            } // only show two fields
          }, { // include orders for the owner
            relation: 'senderx',
            scope: { // further filter the owner object
              fields: ['id', 'userFirstName', 'userLastName', 'username', 'locationFlag', 'location']
            } // only show two fields
          }]
        }
      },
      // order : "addItem DESC"
    }, function (err, out) {
      if (out) {
        
        var finalArr = [];
        var binderArr = [];
        var bindersArr = [];
        // console.log('OUT ' + JSON.stringify(out));
        var arr = [] = JSON.stringify(out);
        var xyz = [] = JSON.parse(arr);

        winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'got total records of  '+xyz.length);
        if (xyz.length > 0) {

          if (xyz.length > 0) {
            winston.GenerateLog(2, 'bindersProcesssedReport Method: Number Of Transactions for Item Unique Id:' + input.itemUniqueId + ' are : ' + xyz[0].transaction.length);
            
            async.each(xyz, function (object, callBC) {
              
              if (object.transaction.length > 0) {
                binderArr = [];
                for (var i = 0; i < object.transaction.length; i++) {
                  
                  var obj = {};
                  var datetime = object.transaction[i].transSucceedTime;
                  // console.log(new Date(datetime).toLocaleString());
                  //Date formated for UI display
                  var reqDate = new Date(datetime).toISOString().replace('Z', '').replace('T', '  ');
                  var date = new Date(object.transaction[i].transSucceedTime);
                  var month = (date.getMonth() + 1);
                  var datee = date.getDate();
                  var addDate = new Date(object.addItem);
                  // console.log('dd', addDate);
                  /** modeling the object and pushing into the finalArr array. */
                  obj['transactionId'] = object.transaction[i].transactionId;
                  obj['time'] = date.getTime();
                  obj['addDate'] = addDate.getTime();
                  obj['itemId'] = object.itemId;
                  obj['i'] = i;
                  obj['itemUniqueId'] = object.itemUniqueId;
                  obj['senderName'] = (object.transaction[i].senderx.locationFlag == 0) ? object.transaction[i].senderx.userFirstName + ' ' + object.transaction[i].senderx.userLastName + ' (' + object.transaction[i].senderx.username + ')' : object.transaction[i].senderx.location + ' (' + object.transaction[i].senderx.username + ')';
                  obj['transactionSuccessTime'] = reqDate.toString();
                  //obj['transactionSuccessTime'] = date.getFullYear() + '-' + ((month.toString().length == 1) ? ('0' + month) : month) + '-' + ((datee.toString().length == 1) ? ('0' +datee) : datee) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();
                  obj['recieverName'] = (object.transaction[i].recieverx.locationFlag == 0) ? object.transaction[i].recieverx.userFirstName + ' ' + object.transaction[i].recieverx.userLastName + ' (' + object.transaction[i].recieverx.username + ')' : object.transaction[i].recieverx.location + ' (' + object.transaction[i].recieverx.username + ')';
                  finalArr.push(obj)
                  binderArr.push(obj);
                  winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'Processed records are   '+finalArr.length);
                  if (i == (object.transaction.length - 1)) {
                    compareArr1(binderArr, function (err, finalResp) {
                      
                      // var finalR = [] = [{'id' : 1}, {'id' : 2}];
                      // var finalRR = []= [{'id' : 3}, {'id' : 4}];
                      Array.prototype.push.apply(bindersArr,finalResp); 
                      // console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ', bindersArr);
                    });
                    
                  }
                }
               
                callBC();
              } 
            })

            /** compareArr will remove the duplicates of transactions */
            // console.log('cccccccccccccccccccccccccccc ', finalArr);
            // compareArr1(finalArr, function (err, finalResp) {
            //   cb(null, finalResp)
            // });
          //   var sortedArray = bindersArr.sort(function(a, b) {
          //     return b.addDate - a.addDate 
          // });
          winston.GenerateLog(2, 'bindersProcessedReport   : ' + 'Final list of reports are   '+bindersArr.length);
            cb(null, bindersArr)
            // return new Promise((resolve, reject) => {
            //   resolve(bindersArr);
            // }); 

            
          } else {
            winston.GenerateLog(2, 'bindersProcessedReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
            cb(null, [])
          }
        } else {
          winston.GenerateLog(2, 'bindersProcessedReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
          cb(null, [])
        }
      } else {
        if (err) {
          winston.GenerateLog(0, 'bindersProcessedReport Method: Error while fetching inventory report of ItemUniqueId is' + input.itemUniqueId + ' ' + err);
        }
        winston.GenerateLog(2, 'bindersProcessedReport Method: No transactions found for itemUniqueId:' + input.itemUniqueId);
        cb('No transactions found ', null)
      }

    });

  }

  function getBinders(object, callBC) {
    var finalArr = []
    winston.GenerateLog(2, 'bindersProcessedReport Method: in getbinders:');
    if (object.transaction.length > 0) {
      for (var i = 0; i < object.transaction.length; i++) {
        var obj = {};
        var datetime = object.transaction[i].transSucceedTime;
        // console.log(new Date(datetime).toLocaleString());
        //Date formated for UI display
        var reqDate = new Date(datetime).toISOString().replace('Z', '').replace('T', '  ');
        var date = new Date(object.transaction[i].transSucceedTime);
        var month = (date.getMonth() + 1);
        var datee = date.getDate();
        /** modeling the object and pushing into the finalArr array. */
        obj['time'] = date.getTime();
        obj['i'] = i;
        obj['itemUniqueId'] = object.itemUniqueId;
        obj['senderName'] = (object.transaction[i].senderx.locationFlag == 0) ? object.transaction[i].senderx.userFirstName + ' ' + object.transaction[i].senderx.userLastName + ' (' + object.transaction[i].senderx.username + ')' : object.transaction[i].senderx.location + ' (' + object.transaction[i].senderx.username + ')';
        obj['transactionSuccessTime'] = reqDate.toString();
        //obj['transactionSuccessTime'] = date.getFullYear() + '-' + ((month.toString().length == 1) ? ('0' + month) : month) + '-' + ((datee.toString().length == 1) ? ('0' +datee) : datee) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();
        obj['recieverName'] = (object.transaction[i].recieverx.locationFlag == 0) ? object.transaction[i].recieverx.userFirstName + ' ' + object.transaction[i].recieverx.userLastName + ' (' + object.transaction[i].recieverx.username + ')' : object.transaction[i].recieverx.location + ' (' + object.transaction[i].recieverx.username + ')';
        finalArr.push(obj)
      }
      winston.GenerateLog(2, 'bindersProcessedReport Method: list of binders for given item uniqueId are:', finalArr.length);
      callBC(null, finalArr)
    } else {
      callBC(null, null)
    }
  }

  /** bindersProcessedReport method declaration */
  Item.remoteMethod("bindersProcessedReport", {
    description: "To bindersProcessedReport",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/bindersProcessedReport",
      verb: "POST"
    }
  });

  var dayStart = function (date) {
    var start = new Date(date);
    start.setUTCHours(0, 0, 0, 0);
    return start;
  }
  var dayEnd = function (date) {
    var end = new Date(date);
    end.setUTCHours(23, 59, 59, 999);
    return end;
  }
  var formatEndDate = function (date) {
    date = new Date(date)
    var tDate = date.getDate()
    date.setDate(tDate + 1)
    return dayEnd(date)
  }
  var formatStartDate = function (date) {
    date = new Date(date)
    return dayStart(date)
  }
  // Query Builder for item close

  var genrateItemCloseSearchQuery = function (input, fcb) {
    if (input.searchQuery.fromDate)
      input.searchQuery.fromDate = formatStartDate(input.searchQuery.fromDate)
    if (input.searchQuery.toDate)
      input.searchQuery.toDate = formatEndDate(input.searchQuery.toDate)

    if (input.searchQuery.itemUniqId && input.searchQuery.fromDate && input.searchQuery.toDate) {
      var filter = {
        where: {
          and: [{
            itemUniqueId: {
              like: input.searchQuery.itemUniqId
            }
          },
          {
            companyId: input.companyId
          },
          {
            currentOwner: input.userId
          },
          {
            itemStatusId: 2
          },
          {
            addItem: {
              between: [new Date(input.searchQuery.fromDate).toISOString(), new Date(input.searchQuery.toDate).toISOString()]
            }
          }
          ]
        }
      }
      fcb(null, filter)
    } else if (input.searchQuery.itemUniqId) {
      var filter = {
        where: {
          and: [{
            itemUniqueId: {
              like: input.searchQuery.itemUniqId
            }
          },
          {
            companyId: input.companyId
          },
          {
            currentOwner: input.userId
          },
          {
            itemStatusId: 2
          },
          ]
        }
      }
      fcb(null, filter)
    } else if (input.searchQuery.fromDate && input.searchQuery.toDate) {
      var filter = {
        where: {
          and: [{
            companyId: input.companyId
          },
          {
            currentOwner: input.userId
          },
          {
            itemStatusId: 2
          },
          {
            addItem: {
              between: [new Date(input.searchQuery.fromDate).toISOString(), new Date(input.searchQuery.toDate).toISOString()]
            }
          }
          ]
        }
      }
      fcb(null, filter)
    } else {
      var filter = {
        where: {
          and: [{
            companyId: input.companyId
          },
          {
            currentOwner: input.userId
          },
          {
            itemStatusId: 2
          }
          ]
        }
      }
      fcb(null, filter)
    }
  }
  // RemoteMethod for getting owned Items
  /** getItemsOwned method to return list of items for given userId and companyId
   * @constructor
   * cb - to handle response
   */
  Item.getItemsOwned = function (input, cb) {
    if (input.userId && input.companyId) {
      /** genrateItemCloseSearchQuery method to return query  */
      genrateItemCloseSearchQuery(input, function (err, filter) {
        var relationObject = {
          include: itemRelation.include,
          skip: input.skip,
          limit: input.limit
        }
        filter = Object.assign(filter, relationObject)
        /** find method will return list of items for given query */
        Item.find(filter, function (err, itemsList) {
          if (err) {
            winston.GenerateLog(0, ' Error while fetching getItemsOwned for userId : ' + input.userId + err + ' ');
            cb(err, null)
          }
          else {
            winston.GenerateLog(2, 'List of Items Owned is : ' + itemsList.length + ' ');
            cb(null, itemsList)
          }
        })
      })
    } else {
      cb("Required Fields are Missing", null)
    }
  };
  // End Owned Items




  var getItemsOwnedsearch2 = function (userId, text, feild, cb) {

    var feildQuery = {}
    if (feild) {
      feild = feild
      feildQuery[feild] = {
        like: text,
        options: "i"
      }
    }

    if (text == 'null' || text == undefined)
      var filter = {
        where: {
          and: [{
            currentOwner: userId
          }]

        },
        order: feild + ' ASC',
        fields: {
          itemId: true,
          tagId: true,
          itemTypeId: true,
          itemUniqueId: true,
          companyId: true,
          currentOwner: true
        }
      }
    else {
      var filter = {
        where: {
          and: [{
            currentOwner: userId
          }]
        },
        fields: {
          itemId: true,
          tagId: true,
          itemTypeId: true,
          itemUniqueId: true,
          companyId: true,
          currentOwner: true
        }
      }
      filter.where.and.push(feildQuery)
    }
    // var items = server.models.Item;
    Item.find(filter, function (err, response) {
      // server.models.Student.find({ where: { firstName: { like: text } } }, function (err, response) {
      if (err) cb(err);
      else {
        cb(null, response)
      }
    })
  }
  /** getItemsOwnedsearch method to return owned items for given query
  * @constructor
  * cb to return results for given search query
  */
  Item.getItemsOwnedsearch = function (obj, cb) {

    getItemsOwnedsearch2(obj.userId, obj.text, obj.feild, function (err, sIds) {

      if (err) {
        winston.GenerateLog(0, ' Error while Querying getItemsOwnedsearch2 : ' + err + ' ');
        cb(err);
      }
      else {
        // var relationObject = {
        //   include: itemRelation.include,
        //   skip: obj.skip,
        //   limit: obj.limit
        // }
        // filter = Object.assign(filter, relationObject)
        // Item.find(filter, function (err, itemsList) {
        //   if (err) cb(err)
        //   else cb(null, itemsList)
        // })
        /** find method will return list of items with tag details, itemType, itemStatus and currentowner details for given itemId */
        Item.find({
          where: {
            and: [{
              itemId: {
                inq: sIds.map(a => a.itemId)
              }
            }, {
              currentOwner: obj.userId
            }, {
              itemStatusId: 2
            }]
          }, include: [
            {
              relation: 'tag',
              scope: {
                fields: ['tagId', 'tagName', 'tagDesc']
              }
            },
            {
              relation: 'itemtype',
              scope: {
                fields: ['itemType', 'itemTypeId']
              }
            },
            {
              relation: 'itemstatus',
              scope: {
                fields: ['description', 'itemStatusId']
              }
            }, {
              relation: 'currentOwnerx',
              scope: {
                fields: ['userName', 'userFirstName', 'userLastName']
              }
            }
          ],
          skip: obj.skip,
          limit: obj.limit,
          order: "addItem DESC"
        }, function (err, out) {
          cb(null, out);

        });

        function subMenu(sub, cb) {
          var userInfo = server.models.Users;
          var itemTypes = server.models.ItemType;
          // var senderName =
          itemTypes.findOne({
            where: {
              id: sub.itemTypeId
            }
          }, function (err3, out3) {
            if (err3) {
              winston.GenerateLog(0, ' Error while Querying itemTypes : ' + err3 + ' ');
            }

            userInfo.findOne({
              where: {
                id: sub.currentOwner
              }
            }, function (err2, out2) {
              if (err2) {
                winston.GenerateLog(0, ' Error while Querying userInfo : ' + err2 + ' ');
              }
              var data = sub;
              data.currentOwnerName = out2.userName;
              data.itemStatusName = 'Owned';
              data.itemTypeName = out3.itemType;
              // data.receiverName = out2.userName;
              //   data.push({'senderName':out1.userName,'receiverName':out2.userName});


              cb(null, data);
            });
          });

        };
      }
    })
  }
  /** getItemsOwnedsearch method declaration */
  Item.remoteMethod('getItemsOwnedsearch', {
    description: 'getItemsOwnedsearch for dashboard',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getItemsOwnedsearch',
      verb: 'POST',
    },
  });
  /** getItemsOwnedCount method : will return the count of items for given userId and status 2
   * @constructor
   * cb - callback to handle response
   */
  Item.getItemsOwnedCount = function (input, cb) {

    Item.count({
      currentOwner: input.userId,
      itemStatusId: 2
    }, function (err, out) {

      if (err) {
        winston.GenerateLog(0, 'Error while querying getItemsOwnedCount method : ' + err + ' ');
      } else {
        winston.GenerateLog(2, 'ItemsOwnedCount with userId : ' + input.userId + " are: " + out + ' ');
      }
      cb(null, out)
    });


  };
  /** getItemsOwned Method declaration */
  Item.remoteMethod("getItemsOwned", {
    description: "To getItemsOwned",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItemsOwned",
      verb: "POST"
    }
  });
  /** getItemsOwnedCount method declaration */
  Item.remoteMethod("getItemsOwnedCount", {
    description: "To getItemsOwnedCount",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItemsOwnedCount",
      verb: "POST"
    }
  });
  /** getItemsBasedonFilterCount method : method to return count
   * @constructor
   * cb - to handle response
   */
  Item.getItemsBasedonFilterCount = function (input, cb) {
    var query = {}
    if (input.companyId)
      query['companyId'] = input.companyId
    if (input.userId)
      query['currentOwner'] = input.userId
    if (input.currentOwner)
      query['currentOwner'] = input.currentOwner
    if (input.itemStatusId && input.itemStatusId !== 'All')
      query['itemStatusId'] = input.itemStatusId
    /** count to return number of items for given companyId, userId and status */
    Item.count(query, cb);

  };
  /** getItemsBasedonFilterCount method declaration */
  Item.remoteMethod("getItemsBasedonFilterCount", {
    description: "To getItemsBasedonFilterCount",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItemsBasedonFilterCount",
      verb: "POST"
    }
  });


  Item.getItemsBasedonFilter = function (input, cb) {
    if (input.companyId) {
      var query = []
      if (input.companyId)
        query.push({
          companyId: input.companyId
        })
      if (input.userId)
        query.push({
          currentOwner: input.userId
        })
      if (input.currentOwner)
        query.push({
          currentOwner: input.currentOwner
        })

      if (input.itemStatusId && input.itemStatusId !== "All")
        query.push({
          itemStatusId: input.itemStatusId
        })

      if (query.length == 0)
        query.push({})

      Item.find({
        where: {
          and: query
        },
        include: ['itemtype', 'itemstatus', 'currentOwnerx'],
        skip: input.skip,
        limit: input.limit,
        order: "addItem DESC"
      }, cb);
    } else {
      cb('CompanyId is required ', null)
    }


  };


  /**getItemsBasedonFilter method declaration */
  Item.remoteMethod("getItemsBasedonFilter", {
    description: "To getItemsBasedonFilter",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/getItemsBasedonFilter",
      verb: "POST"
    }
  });

  // Get User Details

  function getUserDetails(input, userCB) {
    var users = server.models.Users;
    users.findOne({
      'where': {
        'id': input.id
      }
    }, function (userErr, userOut) {
      userCB(null, userOut)
    })

  }

  // get ItemType Details

  function getItemType(input, itemCB) {
    var itemTypes = server.models.ItemType;
    itemTypes.findOne({
      'where': {
        'itemTypeId': input.id
      }
    }, function (itemErr, itemOut) {
      itemCB(null, itemOut)
    })
  }


  // get ItemType Details

  function getTagDetails(input, tagCB) {
    var tag = server.models.Tag;
    tag.findOne({
      'where': {
        'tagId': input.id
      }
    }, function (tagErr, tagOut) {
      tagCB(null, tagOut)
    })
  }

  // Days Held

  function daysHeld(input, dCB) {
    var Difference_In_Time;
    var Difference_In_Days
    if (input.fromDate && input.toDate) {
      var date1 = new Date(input.fromDate);
      var date2 = new Date(input.toDate);
      var addItem = new Date(input.addItem);
      var firstDiff = date1.getTime() - addItem.getTime();
      var secondDiff = date2.getTime() - addItem.getTime();
      Difference_In_Time = (firstDiff <= 0) ? ((secondDiff >= 0) ? secondDiff : 0) : ((secondDiff >= 0) ? secondDiff : 0);
    } else {
      var date1 = new Date();
      var addItem = new Date(input.addItem);
      var firstDiff = date1.getTime() - addItem.getTime();
      Difference_In_Time = (firstDiff >= 0) ? firstDiff : 0
    }

    Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    dCB(null, Difference_In_Days);

  }


  // Time Held Report
  Item.timeHeldReport = function (input, cb) {
    if (input.userId) {
      var int = {};
      // if(input.fromDate && input.toDate) {
      //   int = {
      //     or: [
      //       {
      //         transactionTime: {
      //           between: [input.fromDate, input.toDate]
      //         },
      //         sender: input.userId,
  
      //       }
      //     ]
      //   }
      // } else {
        int = {
          'and': [{ 'itemUniqueId': (input.itemUniqueId!='')?input.itemUniqueId : undefined }, { 'companyId': input.companyId }, { 'currentOwner': (input.currentOwner!="")?input.currentowner:undefined }, { 'itemStatusId': 2 }, { 'itemTypeId': (input.itemTypeId!="")?parseInt(input.itemTypeId):undefined}]
        }
      // }
      
      Item.find({
        'where': int,
        include: ['currentOwnerx','itemTypex'],
      }, function (error, output) {
        var finalArr = [];
        if(output.length > 0) {
          async.each(output, function (object, callBC) {
            var itemObj = {} = JSON.stringify(object);
            var itemObject = JSON.parse(itemObj);
            itemObject['holder'] = itemObject['currentOwnerx']['userFirstName'] + ' ' + itemObject['currentOwnerx']['userLastName'] + ' (' + itemObject['currentOwnerx']['username'] + ')';
            itemObject['itemType'] = itemObject.itemTypex.itemType;
            if(input.fromDate && input.toDate) {
              itemObject['fromDate'] = input.fromDate;
              itemObject['toDate'] = input.toDate;
            }
            daysHeld(itemObject, function (daysErr, daysOut) {
              itemObject['daysHeld'] = daysOut;
              // delete itemObject.currentOwnerx
              delete itemObject.itemTypex
            });
           
            finalArr.push(itemObject)
            callBC();
          });
          cb(null, finalArr)
        } else {
          cb(null, [])
        }
        
      });
    
    }  else {
      cb('UserId is required!', null)
    }
  };
  /** timeHeldReport method: will return timeheld report for given input
   * @constructor
   * @param {Object} filter - input for report
   * @param {Object} ii - global input
   * cb - to handle response
   */
  var ii;
  Item.timeHeldReport1 = function (input, cb) {

    if (input.companyId) {
      ii = input;
      var filter = {};
      /** filter as query input */
      filter = {
        'and': [{ 'itemUniqueId': input.itemUniqueId }, { 'companyId': input.companyId }, { 'currentOwner': input.currentOwner }, { 'itemStatusId': 2 }, { 'itemTypeId': input.itemTypeId }]
      }
      /** find method will find the items for given query */
      Item.find({
        'where':
          filter
      }, function (itemErr, itemOut) {

        if (itemOut.length > 0) {
          var itemArr = [];
          /** checks for fromDate and toDate. if input has these 2 fields it will add them to item object */
          if (input.fromDate && input.toDate) {
            for (var i = 0; i < itemOut.length; i++) {
              var itemObj = {};
              itemObj = itemOut[i];
              itemObj['toDate'] = input.toDate;
              itemObj['fromDate'] = input.fromDate
              itemArr.push(itemObj)
            }
          }
          var finalArr = [];
          finalArr = (itemArr.length > 0) ? itemArr : itemOut
          async.map(finalArr, getDetails, function (error, output) {
            if (error) {
              winston.GenerateLog(0, 'Error while time held report ' + error + ' ');
            }
            winston.GenerateLog(2, 'List of time held report ' + output.length + ' ');
            cb(null, output)
          })

        } else {
          if (itemErr) {
            winston.GenerateLog(0, 'Error while querying Items ' + itemErr + ' ');
          }
          cb('Error while querying Items ', null)
        }

      })
    } else {
      cb('CompanyId is required ', null)
    }



  }
  /** getDetails method to fetch user, itemType and days held for given item */
  function getDetails(obj, cBC) {
    /** getUserDetails method to fetch user details */
    getUserDetails({
      'id': obj.currentOwner
    }, function (userErr, userOut) {
      if (userErr) {
        winston.GenerateLog(0, 'error while querying users' + userErr + ' ');
      }
      /** getItemType method to fetch itemType details */
      getItemType({
        'id': obj.itemTypeId
      }, function (itemErr, itemOut) {
        if (itemErr) {
          winston.GenerateLog(0, 'error while querying itemtypes' + itemErr + ' ');
        }
        /** daysHeld to be calculated based on inputs  */
        daysHeld(obj, function (daysErr, daysOut) {
          if (daysErr) {
            winston.GenerateLog(0, 'error while querying itemtypes' + daysErr + ' ');
          }
          var dObj = {}
          dObj = obj;
          dObj['holder'] = (userOut) ? (userOut.userFirstName + ' ' + userOut.userLastName + ' ( ' + userOut.username + ' )') : null;
          dObj['itemType'] = (itemOut) ? itemOut.itemType : null;
          dObj['daysHeld'] = daysOut;
          winston.GenerateLog(2, 'holder for given itemTypeId ' + dObj['holder'] + ' ');
          winston.GenerateLog(2, 'itemType for given itemTypeId ' + dObj['itemType'] + ' ');
          winston.GenerateLog(2, 'daysHeld for given itemTypeId ' + dObj['daysHeld'] + ' ');
          cBC(null, dObj)
        });
      });
    });
  }

  /** timeHeldReport method declaration */
  Item.remoteMethod('timeHeldReport', {
    description: 'timeHeldReport',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/timeHeldReport',
      verb: 'post',
    },
  });
  // User Tracking Report
  /** userTrackingReport method : 
   * @constructor
   * cb - callback to handle response
   */
  Item.userTrackingReport = function (input, cb) {
    if (input.userId) {
      var transactions = server.models.Transactions;
      /** find will fetch list of transactions for given userId */
      transactions.find({ 'where': { 'reciever': input.userId }, include: ['recieverx'] }, function (error, output) {
        if (output.length > 0) {
          winston.GenerateLog(2, 'List of User transactions for given userId ' + input.userId + ' are ' + cleanArray(output).length + ' ');
          async.map(output, getItemsList, function (itemErr, itemOut) {
            winston.GenerateLog(2, 'List of User tracking report for given userId ' + input.userId + ' are ' + cleanArray(itemOut).length + ' ');
            cb(null, itemOut)
          })
        } else {
          if (error) {
            winston.GenerateLog(0, 'Error while querying transactions for given userId ' + input.userId + ' is ' + error + ' ');
            cb('Error while querying transactions for given userId', null)
          } else {
            cb(null, [])
          }

        }
      })
    } else {
      cb('UserId is required!.', null)
    }

  }
  /** getItemList method : to fetch items, tagdetails, and userdetails*/
  function getItemsList(obj, cbc) {
    Item.findOne({
      'where': {
        'itemId': obj.itemId
      }
    }, function (itemErr, itemOut) {
      /** tag details to fetch with tagId for given itemid */
      getTagDetails({
        'id': itemOut.tagId
      }, function (tagErr, tagOut) {
        /** to fetch sender details for given userId */
        getUserDetails({
          'id': obj.sender
        }, function (userErr, userOut) {
          /** daysHeld to calculate the items between the dates */
          daysHeld(itemOut, function (dayeErr, daysOut) {
            var objj = {};
            objj = obj;
            objj['tagName'] = tagOut.tagName;
            objj['itemUniqueId'] = itemOut.itemUniqueId;
            objj['daysHeld'] = daysOut;
            objj['lastHeld'] = (userOut) ? (userOut.userFirstName + ' ' + userOut.userLastName + ' ( ' + userOut.username + ' )') : null;
            winston.GenerateLog(2, 'tagName ' + tagOut.tagName + ' ');
            winston.GenerateLog(2, 'itemUniqueId ' + itemOut.itemUniqueId + ' ');
            winston.GenerateLog(2, 'daysHeld ' + daysOut + ' ');
            winston.GenerateLog(2, 'lastHeld ' + objj['lastHeld'] + ' ');
            cbc(null, objj)
          })
        });
      })
    })
  }
  /** userTrackingReport method declaration */
  Item.remoteMethod('userTrackingReport', {
    description: 'userTrackingReport',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/userTrackingReport',
      verb: 'post',
    },
  });

  // Items sent by user report
  function getItemsBetweenDates(input, dCB) {

    var Difference_In_Time;
    var Difference_In_Days
    var date1 = new Date(xyz.fromDate);
    var date2 = new Date(xyz.toDate);
    var addItem = new Date(input.transactionTime);
    var firstDiff = date1.getTime() - addItem.getTime();
    var secondDiff = date2.getTime() - addItem.getTime();

    if (firstDiff <= 0 && secondDiff >= 0) {
      dCB(null, input)
    } else {
      dCB(null, null)
    }

  }

  Item.itemsSentReport = function (input, cb) {
    if (input.userId) {
      var int = {};
      if(input.fromDate && input.toDate) {
        int = {
          or: [
            {
              transSucceedTime: {
                between: [input.fromDate, input.toDate]
              },
              sender: input.userId,
  
            }
          ]
        }
      } else {
        int = {
              sender: input.userId,
        }
      }
      var transactions = server.models.Transactions;
      transactions.find({
        'where': int,
        include: ['senderx','recieverx',[{'itemRelations':'tag'}]],
        order: 'transSucceedTime DESC',
          //   include: [{ 
          //     relation: 'itemRelations',
          //   },
          //   {
          //   relation: 'senderx'}
          // ]
      }, function (error, output) {
        var finalArr = [];
        if(output.length > 0) {
          async.each(output, function (object, callBC) {
            var itemTypeObj = {} = JSON.stringify(object);
            var itemTypeObject = JSON.parse(itemTypeObj);
            var obj = itemTypeObject;
            obj['tagName'] = itemTypeObject['itemRelations']['tag']['tagName']
            obj['itemUniqueId'] = itemTypeObject['itemRelations']['itemUniqueId']
            obj['Reciever'] = itemTypeObject['recieverx']['userFirstName'] + ' ' + itemTypeObject['recieverx']['userLastName'] + ' (' + itemTypeObject['recieverx']['username'] + ')'
            delete obj['itemRelations']
            delete obj['recieverx']
            finalArr.push(obj);
            callBC();
          })
          cb(null, finalArr)
        } else {
          cb(null, [])
        }
       
      });
    
    }  else {
      cb('UserId is required!', null)
    }
  };
  var xyz = {};
  /** itemsSentReport method : to fetch the reports of a user based on Id
   * @constructor
   * @param {Object} xyz -- is a global input
   * cb - callback to handle reponse
   */
  Item.itemsSentReport1 = function (input, cb) {
    if (input.userId) {
      xyz = input;
      var transactions = server.models.Transactions;
      /** find method to fetch transactions for given sender */
      transactions.find({
        'where': {
          'sender': input.userId
        },
        include: ['senderx']
      }, function (error, output) {
        if (error) {
          winston.GenerateLog(0, 'Error while querying Transactions ' + error + ' ');
          cb('Error while querying Transactions', null)
        }
        if (output.length > 0) {
          async.map(output, getItemList, function (itemErr, itemOut) {
            winston.GenerateLog(2, 'List of items sent by user report are ' + cleanArray(itemOut).length + ' ');
            cb(null, cleanArray(itemOut));
          })
        } else {
          cb(null, null)
        }
      })
    } else {
      cb('UserId is required!', null)
    }

  }

  /** getItemList method : to handle user, tag details and also to fetch records in between the dates */
  function getItemList(obj, cbc) {
    /** findOne method to fetch item details based on itemId */
    Item.findOne({
      'where': {
        'itemId': obj.itemId
      }
    }, function (itemErr, itemOut) {
      if (itemErr) {
        winston.GenerateLog(0, 'Error while querying Item ' + itemErr + ' ');
      }
      /** getTagDetails to fetch tag details */
      getTagDetails({
        'id': itemOut.tagId
      }, function (tagErr, tagOut) {
        if (tagErr) {
          winston.GenerateLog(0, 'Error while querying Tag ' + tagErr + ' ');
        }
        /** getUserDetails to fetch reciever details */
        getUserDetails({
          'id': obj.reciever
        }, function (userErr, userOut) {
          if (userErr) {
            winston.GenerateLog(0, 'Error while querying users ' + userErr + ' ');
          }
          if (xyz.fromDate && xyz.toDate) {
            /** getItemsBetweenDates method to handle the records for given dates : it will fetch the records in between the dates */
            getItemsBetweenDates(obj, function (err, output1) {
              if (output1) {
                var objj = {};
                objj = obj;
                objj['tagName'] = tagOut.tagName;
                objj['itemUniqueId'] = itemOut.itemUniqueId;
                // objj['daysHeld'] = daysOut;
                objj['Reciever'] = (userOut) ? (userOut.userFirstName + ' ' + userOut.userLastName + ' ( ' + userOut.username + ' )') : null;
                winston.GenerateLog(2, 'tagName ' + tagOut.tagName + ' ');
                winston.GenerateLog(2, 'itemUniqueId ' + itemOut.itemUniqueId + ' ');
                winston.GenerateLog(2, 'Reciever ' + objj['Reciever'] + ' ');
                cbc(null, objj)
              } else {
                cbc(null, null)
              }

            })
          } else {
            var objj = {};
            objj = obj;
            objj['tagName'] = tagOut.tagName;
            objj['itemUniqueId'] = itemOut.itemUniqueId;
            // objj['daysHeld'] = daysOut;
            objj['Reciever'] = (userOut) ? (userOut.userFirstName + ' ' + userOut.userLastName + ' ( ' + userOut.username + ' )') : null;
            winston.GenerateLog(2, 'tagName ' + tagOut.tagName + ' ');
            winston.GenerateLog(2, 'itemUniqueId ' + itemOut.itemUniqueId + ' ');
            winston.GenerateLog(2, 'Reciever ' + objj['Reciever'] + ' ');
            cbc(null, objj)
          }
        });
      })
    })
  }
  /** itemsSentReport method declaration */
  Item.remoteMethod('itemsSentReport', {
    description: 'itemsSentReport',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/itemsSentReport',
      verb: 'post',
    },
  });
  /** itemsReport method : to fetch items
   * @constructor
   * cb - callback to handle response
   */

  Item.itemsReport = function (input, cb) {

    var data = [];
    /** if itemUniqueId not itemTypeId  */
    if (input.itemUniqueId && !input.itemTypeId) {
      /** find method to fetch items for given itemUniqueId and companyId including current owner details */
      Item.find({
        where: { and: [{ itemUniqueId: input.itemUniqueId }, { companyId: input.companyId }] }, include: ['currentOwnerx'],
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, itemMenu, function (asyncErr, asyncOut) {
              var ssdata = {
                itemArrList: asyncOut,
                totalItems: out.length
              }

              data.push({ "itemArrList": asyncOut, "noOfItems": out.length });
              winston.GenerateLog(2, 'List of itemsreport ' + asyncOut.length + ' ');
              cb(null, data);
            });
          } else {
            winston.GenerateLog(0, 'No Match found for items ' + ' ');
            var msg = { message: "no match found" };
            cb(msg, null);
          }
        });
      /** itemMenu method */
      function itemMenu(obj, cbc) {
        var transactions = server.models.Transactions;
        /** find method to fetch list of transactions for given itemId with order transSucceedTime  */
        transactions.find({ where: { itemId: obj.itemId }, order: "transSucceedTime DESC" }, function (err, transOut) {
          var checkTrans;
          if (transOut.length > 0) {
            winston.GenerateLog(2, 'List of transactions for given itemId ' + obj.itemId + ' are ' + transOut.length + ' ');
            if (transOut[transOut.length - 1].transSucceedTime != null) {
              checkTrans = transOut[transOut.length - 1].transSucceedTime;
            } else {
              checkTrans = 0;
            }
          }
          var data;
          var itemTypes = server.models.ItemType;
          /** findOne method to fetch one record of itemTypes with itemTypeId */
          itemTypes.findOne({ 'where': { 'itemTypeId': obj.itemTypeId } }, function (itemTypeErr, itemTypeOut) {
            /** getUserDetails to fetch current owner details */
            getUserDetails({
              'id': obj.currentOwner
            }, function (userErr, userOut) {

              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: 0

                }
                winston.GenerateLog(2, 'ItemTypes for itemStatus 1 ' + data.toString() + ' ');
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);

                  d = Math.round(c);
                }
                
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  dateCreated: checkTrans,
                  trackingItem: "YES",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                winston.GenerateLog(2, 'ItemTypes for itemStatus 2 or 5 ' + data.toString() + ' ');
                cbc(null, data);
              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);

                  d = Math.round(c);
                }


                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                winston.GenerateLog(2, 'ItemTypes for itemStatus 3 or 4 ' + data.toString() + ' ');
                cbc(null, data);
              } else {
                cb('error', null);
              }




              // var data = {
              //   itemType: objsub.itemTypeId,

              // }

              // async.map(out, lastbinderSubMenu, function (asyncErr, asyncOut) {

              // cb(null, data);
              // });
            });

          });
        });

      }

    }
    /** not itemUniqueId only itemTypeId  */
    else if (!input.itemUniqueId && input.itemTypeId) {
      /** find method to fetch list of items for given companyId, itemTypeId including current owner details */
      Item.find({
        where: { and: [{ companyId: input.companyId }, { itemTypeId: input.itemTypeId }] }, include: ['currentOwnerx'], skip: input.skip,
        limit: input.limit,
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, itemMenu, function (asyncErr, asyncOut) {
              var ssdata = {
                itemArrList: asyncOut,
                totalItems: out.length
              }

              /** data is an array. pushing list of items and length of items */
              data.push({ "itemArrList": asyncOut, "noOfItems": out.length });
              // data.push({ "itemArrList": asyncOut, "noOfItems": out.length ,"currentownerinfo": out});

              cb(null, data);
            });
          } else {
            var msg = { message: "no match found" };
            cb(msg, null);
          }
        });
      /** itemMenu method */
      function itemMenu(obj, cbc) {

        var transactions = server.models.Transactions;
        /** find method to fetch list of transactions for given itemId and with transSucceedTime with descending order */
        transactions.find({ where: { itemId: obj.itemId }, order: "transSucceedTime DESC" }, function (err, transOut) {
          var checkTrans;
          if (transOut.length > 0) {
            if (transOut[transOut.length - 1].transSucceedTime != null) {
              checkTrans = transOut[transOut.length - 1].transSucceedTime;
            } else {
              checkTrans = 0;
            }
          }
          var data;
          var itemTypes = server.models.ItemType;
          /** findOne method to fetch itemType for given itemTypeId */
          itemTypes.findOne({ 'where': { 'itemTypeId': obj.itemTypeId } }, function (itemTypeErr, itemTypeOut) {
            // var cuObj = {}
            // cuObj = obj.currentOwnerx;
            /** to fetch current owner details */
            getUserDetails({
              'id': obj.currentOwner
            }, function (userErr, userOut) {

              // });

              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolderDetails: userOut,
                  itemHolder: obj.currentOwner,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);
                  d = Math.round(c);
                }

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  dateCreated: checkTrans,
                  trackingItem: "YES",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);

                  d = Math.round(c);
                }
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                cbc(null, data);
              } else {
                cb('error', null);
              }
            });

          });
        });

      }
    }
    /** if both itemUniqueId and itemTypeId as the input */
    else if (input.itemUniqueId && input.itemTypeId) {
      /** find method to fetch list of items for both itemUniqueId and itemUniqueId for given companyId */
      Item.find({ where: { and: [{ itemUniqueId: input.itemUniqueId }, { companyId: input.companyId }, { itemTypeId: input.itemTypeId }] } }, function (
        err,
        out
      ) {
        if (out.length > 0) {
          async.map(out, itemMenu, function (asyncErr, asyncOut) {
            var ssdata = {
              itemArrList: asyncOut,
              totalItems: out.length
            }

            data.push({ "itemArrList": asyncOut, "noOfItems": out.length });

            cb(null, data);
          });
        } else {
          var msg = { message: "no match found" };
          cb(msg, null);
        }
      });
      /** itemMenu method */
      function itemMenu(obj, cbc) {
        var transactions = server.models.Transactions;
        /** find method to retun list of transactions for given itemId and descending order with transSucceedTime  */
        transactions.find({ where: { itemId: obj.itemId }, order: "transSucceedTime DESC" }, function (err, transOut) {
          var checkTrans;
          if (transOut.length > 0) {
            if (transOut[transOut.length - 1].transSucceedTime != null) {
              checkTrans = transOut[transOut.length - 1].transSucceedTime;
            } else {
              checkTrans = 0;
            }

          }
          var data;
          var itemTypes = server.models.ItemType;
          /** findOne method to fetch one record for given itemTypeId */
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {
            /** getUserDetails to fetch current owners info */
            getUserDetails({
              'id': obj.currentOwner
            }, function (userErr, userOut) {




              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);

                  d = Math.round(c);

                }


                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  dateCreated: checkTrans,
                  trackingItem: "YES",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var d = 0;
                if (transOut.length > 0) {
                  var a = new Date(transOut[0].transSucceedTime).getTime()

                  var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                  var c = a - b;
                  c = c / (24 * 60 * 60 * 1000);

                  d = Math.round(c);
                }


                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  dateCreated: checkTrans,
                  trackingItem: "NO",
                  itemHolder: obj.currentOwner,
                  itemHolderDetails: userOut,
                  daysinTracking: d

                }
                cbc(null, data);
              } else {
                cbc('error', null);
              }

            });

          });
        });

      }
    } else {
      var msg = {
        message: "input is not valid"
      };

      cb(msg, null);
      // return;
    }

  };

  /** itemsReport method declaration */
  Item.remoteMethod("itemsReport", {
    description: "To itemsReport",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/itemsReport",
      verb: "POST"
    }
  });


  /** itemCloseFilterReport method : to retun list of items that were closed
   * @constructor
   * cb- callback to handle response
   */
  Item.itemCloseFilterReport = function (input, cb) {


    /** if input has startDate, endDate not itemUniqueId, itemTypeId */
    if (input.startDate && input.endDate && !input.itemUniqueId && !input.itemTypeId) {


      // var a = new Date(input.startDate).getTime();
      // var b = new Date(input.endDate).getTime();

      var a = new Date(input.startDate);
      var b = new Date(input.endDate);
      a.setDate(a.getDate() - 1);
      b.setDate(b.getDate() + 1);
      /** finding list of items that were closed for given company in between dates */
      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            itemStatusId: 6
          }, {
            addItem: {
              gte: a
            }
          }, {
            addItem: {
              lte: b
            }
          }],
        }
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }
              winston.GenerateLog(2, 'List of  itemCloseFilterReport ' + data.closeList.length + ' ');

              cb(null, data);
            })
          } else {
            winston.GenerateLog(0, 'No records for itemCloseFilterReport ' + err + ' ');
          }

          // cb(null,out);


        });
      /** closedItemList method to return list of transactions associated to given items */
      function closedItemList(obj, cbc) {
        var itemTypes = server.models.ItemType;
        /** itemtype will return for given itemTypeId */
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {
          if (itemTypeErr) {
            winston.GenerateLog(0, 'Error while fetching itemTypes ' + itemTypeErr + ' ');
          }
          var checkTrans;
          if (itemTypeOut) {
            checkTrans = itemTypeOut.itemType;
          } else {
            checkTrans = 0;
          }

          var transactions = server.models.Transactions;
          /** one transaction will return */
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {

            if (err) {
              winston.GenerateLog(0, 'Error while fetching transactions ' + err + ' ');
            }

            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              if (userErr) {
                winston.GenerateLog(0, 'Error while fetching users ' + userErr + ' ');
              }
              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                // itemType:itemTypeOut.itemType,
                ItemType: checkTrans,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }
      /** if startDate, endDate and itemUniqueId are part of input not itemTypeId */
    } else if (input.startDate && input.endDate && input.itemUniqueId && !input.itemTypeId) {


      // var a = new Date(input.startDate).getTime();
      // var b = new Date(input.endDate).getTime();

      var a = new Date(input.startDate);
      var b = new Date(input.endDate);
      a.setDate(a.getDate() - 1);
      b.setDate(b.getDate() + 1);

      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            itemUniqueId: input.itemUniqueId
          }, {
            itemStatusId: 6
          }, {
            addItem: {
              gte: a
            }
          }, {
            addItem: {
              lte: b
            }
          }]
        }
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }

              cb(null, data);
            })
          } else {

            cb('error', null);
          }

          // cb(null,out);


        });


      function closedItemList(obj, cbc) {



        var itemTypes = server.models.ItemType;
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {


          var transactions = server.models.Transactions;
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {


            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                itemType: itemTypeOut.itemType,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }
    } else if (input.startDate && input.endDate && input.itemUniqueId && input.itemTypeId) {

      // var a = new Date(input.startDate);
      // var b = new Date(input.endDate);

      var a = new Date(input.startDate);
      var b = new Date(input.endDate);
      a.setDate(a.getDate() - 1);
      b.setDate(b.getDate() + 1);
      // Item.find({ where:{and: [{ itemUniqueId: input.itemUniqueId },{companyId:input.companyId},{itemTypeId:input.itemTypeId},{itemStatusId:6},{or:[{addItem: { gte: b }},{addItem: { lte: a }}]},{or:[{addItem: { gt: a }},{addItem: { lt: b }}]}]} }, function (
      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            itemUniqueId: input.itemUniqueId
          }, {
            itemTypeId: input.itemTypeId
          }, {
            itemStatusId: 6
          }, {
            addItem: {
              gte: a
            }
          }, {
            addItem: {
              lte: b
            }
          }]
        }
      }, function (

        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }

              cb(null, data);
            })
          } else {

            cb('error', null);
          }

          // cb(null,out);

        });

      function closedItemList(obj, cbc) {



        var itemTypes = server.models.ItemType;
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {


          var transactions = server.models.Transactions;
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {


            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                itemType: itemTypeOut.itemType,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }

    } else if (!input.startDate && !input.endDate && input.itemUniqueId && input.itemTypeId) {


      // Item.find({ where:{and: [{ itemUniqueId: input.itemUniqueId },{companyId:input.companyId},{itemTypeId:input.itemTypeId},{itemStatusId:6}]} }, function (
      //   err,
      //   out
      // ) {


      //     cb(null,out);

      //   });

      Item.find({
        where: {
          and: [{
            itemUniqueId: input.itemUniqueId
          }, {
            companyId: input.companyId
          }, {
            itemTypeId: input.itemTypeId
          }, {
            itemStatusId: 6
          }]
        }
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }

              cb(null, data);
            })
          }
          else {

            cb('error', null);
          }

          // cb(null,out);


        });

      function closedItemList(obj, cbc) {



        var itemTypes = server.models.ItemType;
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {
          var checkTrans;
          if (itemTypeOut) {
            checkTrans = itemTypeOut.itemType;
          } else {
            checkTrans = 0;
          }


          var transactions = server.models.Transactions;
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {


            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                // itemType:itemTypeOut.itemType,
                ItemType: checkTrans,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }


    } else if (!input.startDate && !input.endDate && input.itemUniqueId && !input.itemTypeId) {



      Item.find({
        where: {
          and: [{
            itemUniqueId: input.itemUniqueId
          }, {
            companyId: input.companyId
          }, {
            itemStatusId: 6
          }]
        }
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }

              cb(null, data);
            })
          }
          else if (err) {
            cb('Error while fetching items from item close filter report', null)
          }
          else {

            cb(null, []);
          }

          // cb(null,out);


        });

      function closedItemList(obj, cbc) {



        var itemTypes = server.models.ItemType;
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {
          var checkTrans;
          if (itemTypeOut) {
            checkTrans = itemTypeOut.itemType;
          } else {
            checkTrans = 0;
          }


          var transactions = server.models.Transactions;
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {


            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                // itemType:itemTypeOut.itemType,
                ItemType: checkTrans,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }


    }

    else if (!input.startDate && !input.endDate && !input.itemUniqueId && input.itemTypeId) {



      Item.find({
        where: {
          and: [{
            itemTypeId: input.itemTypeId
          }, {
            companyId: input.companyId
          }, {
            itemStatusId: 6
          }]
        }
      }, function (
        err,
        out
      ) {

          if (out.length > 0) {
            async.map(out, closedItemList, function (itemErr, itemOut) {
              var data = {
                closeList: itemOut,
                totalClosed: out.length
              }

              cb(null, data);
            })
          }
          else {

            cb('error', null);
          }

          // cb(null,out);


        });

      function closedItemList(obj, cbc) {



        var itemTypes = server.models.ItemType;
        itemTypes.findOne({
          'where': {
            'itemTypeId': obj.itemTypeId
          }
        }, function (itemTypeErr, itemTypeOut) {
          var checkTrans;
          if (itemTypeOut) {
            checkTrans = itemTypeOut.itemType;
          } else {
            checkTrans = 0;
          }


          var transactions = server.models.Transactions;
          transactions.findOne({
            where: {
              transactionId: obj.transactionId
            },
            order: "transSucceedTime DESC"
          }, function (err, transOut) {


            var checkReciever;

            if (transOut.reciever != null) {
              checkReciever = transOut.reciever;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }

              var data = {
                itemNumber: obj.itemUniqueId,
                // itemType:itemTypeOut.itemType,
                ItemType: checkTrans,
                dateClosed: transOut.transSucceedTime,
                lastHeldBy: heldInfo,
                daysinTracking: 0
                // totalClosed:out.length-1
              }

              cbc(null, data);
            });

          });

        });

      }
    }


    else {
      cb('error', null);
    }

    // cb(null,'check'+out);
  };

  /** itemCloseFilterReport method declaration */
  Item.remoteMethod("itemCloseFilterReport", {
    description: "To itemCloseFilterReport",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/itemCloseFilterReport",
      verb: "POST"
    }
  });



  Item.itemSearchReport1 = function (input, cb) {
    var filter =  {
      and: [{
        currentOwner: input.userId
      }, {
        itemUniqueId: input.itemUniqueId
      }, {
        companyId: input.companyId
      }, {
        itemTypeId: input.itemTypeId
      }]
    }
    Item.find({
      where:filter,
      include: ['currentOwnerx','itemTypex','transaction'],
    }, function(err, out) {
      if (out.length > 0) {
        var searchList = [];
        async.each(out, function (object, callBC) {
          var itemSrcObj = {} = JSON.stringify(object);
          var itemSrcObject = JSON.parse(itemSrcObj);
          var tracking ;
          if (itemSrcObject.itemStatusId == 2 || itemSrcObject.itemStatusId == 5) {
            var a = new Date(itemSrcObject.transaction[0].transSucceedTime).getTime()
            var b = new Date(itemSrcObject.transaction[itemSrcObject.transaction.length - 1].transSucceedTime).getTime()
            var c = a - b;
            c = c / (24 * 60 * 60 * 1000);
            var d = Math.round(c);
            d = Math.abs(d)
            tracking = d;
          }
          searchList.push({
            "itemType":itemSrcObject.itemTypex.itemType,
            "itemTotal":1,
            "itemNumber":itemSrcObject.itemUniqueId,
            "trackingItem":"YES",
            "itemHolder": (itemSrcObject.currentOwnerx) ? (itemSrcObject.currentOwnerx.locationFlag == 0) ? itemSrcObject.currentOwnerx.userFirstName + ' ' + itemSrcObject.currentOwnerx.userLastName + ' (' + itemSrcObject.currentOwnerx.username + ')' : itemSrcObject.currentOwnerx.location + ' (' + itemSrcObject.currentOwnerx.username + ')' : null,
            "daysinTracking":tracking+"days"
          })
          callBC();
        })
        cb(null, searchList)
      } else {
        cb(null,[])
      }
    })
  };

  Item.itemSearchReport = function (input, cb) {

    var data = [];

    // if(input.itemUniqueId != null  && input.itemTypeId != null && (input.userId == null || input.userId === undefined))
    if (input.itemUniqueId && input.itemTypeId && !input.userId) {


      Item.find({
        where: {
          and: [{
            itemUniqueId: input.itemUniqueId
          }, {
            companyId: input.companyId
          }, {
            itemTypeId: input.itemTypeId
          }]
        }
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, searchItemList, function (itemErr, itemOut) {
              var ssdata = {
                searchList: itemOut,
                noOfItems: out.length
              }
              // data.push(ssdata);

              data.push({ "searchList": itemOut, "noOfItems": out.length });
              winston.GenerateLog(2, 'List of search reports are ' + itemOut.length + ' ');
              cb(null, data);
            })


          } else {
            winston.GenerateLog(0, 'Error while querying Item ' + err + ' ');
          }

        });


      function searchItemList(obj, cbc) {
        var transactions = server.models.Transactions;
        transactions.find({
          where: {
            itemId: obj.itemId
          },
          order: "transSucceedTime DESC"
        }, function (err, transOut) {
          if (err) {
            winston.GenerateLog(0, 'Error while fetching transactions ' + err + ' ');
          }
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {
            if (itemTypeErr) {
              winston.GenerateLog(0, 'Error while fetching itemTypes ' + itemTypeErr + ' ');
            }

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              if (userErr) {
                winston.GenerateLog(0, 'Error while fetching Users ' + userErr + ' ');
              }
              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                winston.GenerateLog(2, 'items search for status 1 ' + data.toString() + ' ');
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);
                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                winston.GenerateLog(2, 'items search for status 2 or 5 ' + data.toString() + ' ');
                cbc(null, data);

              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                winston.GenerateLog(2, 'items search for status 3 or 4 or 6 ' + data.toString() + ' ');
                cbc(null, data);
              } else {
                winston.GenerateLog(0, 'error ' + ' ');
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    } else if (input.itemUniqueId && input.itemTypeId && input.userId) {

      Item.find({
        where: {
          and: [{
            currentOwner: input.userId
          }, {
            itemUniqueId: input.itemUniqueId
          }, {
            companyId: input.companyId
          }, {
            itemTypeId: input.itemTypeId
          }]
        }
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, searchItemList, function (itemErr, itemOut) {
              var ssdata = {
                searchList: itemOut,
                noOfItems: out.length
              }
              // data.push(ssdata);
              data.push({ "searchList": itemOut, "noOfItems": out.length });
              cb(null, data);
            })


          }

        });


      function searchItemList(obj, cbc) {

        var transactions = server.models.Transactions;
        transactions.find({
          where: {
            itemId: obj.itemId
          },
          order: "transSucceedTime DESC"
        }, function (err, transOut) {
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);

              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);
              } else {
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    } else if (!input.itemUniqueId && input.itemTypeId && !input.userId) {
      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            itemTypeId: input.itemTypeId
          }]
        }
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, searchItemList, function (itemErr, itemOut) {
              var sssata = {
                searchList: itemOut,
                noOfItems: out.length
              }
              // data.push(ssdata);
              data.push({ "searchList": itemOut, "noOfItems": out.length });
              cb(null, data);
            })


          }

        });


      function searchItemList(obj, cbc) {

        var transactions = server.models.Transactions;
        transactions.find({ where: { itemId: obj.itemId }, order: "transSucceedTime DESC" }, function (err, transOut) {
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({ 'where': { 'itemTypeId': obj.itemTypeId } }, function (itemTypeErr, itemTypeOut) {

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            }
            else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({ 'where': { 'id': checkReciever } }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              }
              else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                cbc(null, data);
              }
              else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);

              }
              else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);
              }
              else {
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    }

    else if (input.itemUniqueId && !input.itemTypeId && !input.userId) {
      Item.find({ where: { and: [{ companyId: input.companyId }, { itemUniqueId: input.itemUniqueId }] } }, function (
        err,
        out
      ) {
        if (out.length > 0) {
          async.map(out, searchItemList, function (itemErr, itemOut) {
            var sssata = {
              searchList: itemOut,
              noOfItems: out.length
            }
            // data.push(ssdata);
            data.push({ "searchList": itemOut, "noOfItems": out.length });
            cb(null, data);
          })


        }

      });


      function searchItemList(obj, cbc) {

        var transactions = server.models.Transactions;
        transactions.find({
          where: {
            itemId: obj.itemId
          },
          order: "transSucceedTime DESC"
        }, function (err, transOut) {
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);

              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d + 'days'

                }
                cbc(null, data);
              } else {
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    } else if (!input.itemUniqueId && input.userId && !input.itemTypeId) {
      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            currentOwner: input.userId
          }, {
            itemTypeId: { neq: null }
          }]
        }
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, searchItemList, function (itemErr, itemOut) {
              var ssdata = {
                searchList: itemOut,
                // data.push(searchList);
                noOfItems: itemOut.length
              }
              // data.push(itemOut);
              data.push({ "searchList": itemOut, "noOfItems": out.length });

              cb(null, data);
            })


          }

        });


      function searchItemList(obj, cbc) {

        var transactions = server.models.Transactions;
        transactions.find({
          where: {
            and: [
              { itemId: obj.itemId }, { reciever: input.userId }
            ]
          },
          order: "transSucceedTime DESC"
        }, function (err, transOut) {
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d

                }
                cbc(null, data);

              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d

                }
                cbc(null, data);
              } else {
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    }
    else if (!input.itemUniqueId && input.userId && input.itemTypeId) {
      Item.find({
        where: {
          and: [{
            companyId: input.companyId
          }, {
            currentOwner: input.userId
          }, {
            itemTypeId: input.itemTypeId
          }]
        }
      }, function (
        err,
        out
      ) {
          if (out.length > 0) {
            async.map(out, searchItemList, function (itemErr, itemOut) {
              var ssdata = {
                searchList: itemOut,
                // data.push(searchList);
                noOfItems: itemOut.length
              }
              // data.push(itemOut);
              data.push({ "searchList": itemOut, "noOfItems": out.length });

              cb(null, data);
            })


          }

        });


      function searchItemList(obj, cbc) {

        var transactions = server.models.Transactions;
        transactions.find({
          where: {
            and: [
              { itemId: obj.itemId }, { reciever: input.userId }
            ]
          },
          order: "transSucceedTime DESC"
        }, function (err, transOut) {
          var data;
          var checkReciever;
          var itemTypes = server.models.ItemType;
          itemTypes.findOne({
            'where': {
              'itemTypeId': obj.itemTypeId
            }
          }, function (itemTypeErr, itemTypeOut) {

            if (obj.currentOwner != null) {
              checkReciever = obj.currentOwner;
            } else {
              checkReciever = 0;
            }


            var users = server.models.Users;
            users.findOne({
              'where': {
                'id': checkReciever
              }
            }, function (userErr, userOut) {

              var heldInfo;
              if (userOut) {
                heldInfo = userOut.userFirstName + ' ' + userOut.userLastName + ' (' + userOut.userName + ')';
              } else {
                heldInfo = null;

              }


              if (obj.itemStatusId == 1) {
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "NO",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: 0

                }
                cbc(null, data);
              } else if (obj.itemStatusId == 2 || obj.itemStatusId == 5) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);
                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d

                }
                cbc(null, data);

              } else if (obj.itemStatusId == 3 || obj.itemStatusId == 4 || obj.itemStatusId == 6) {
                var a = new Date(transOut[0].transSucceedTime).getTime()

                var b = new Date(transOut[transOut.length - 1].transSucceedTime).getTime()

                var c = a - b;
                c = c / (24 * 60 * 60 * 1000);

                var d = Math.round(c);

                data = {
                  itemType: itemTypeOut.itemType,
                  itemTotal: 1,
                  itemNumber: obj.itemUniqueId,
                  // dateCreated:transOut[transOut.length-1].transSucceedTime,
                  trackingItem: "YES",
                  // itemHolder:out.currentOwner,
                  itemHolder: heldInfo,
                  daysinTracking: d

                }
                cbc(null, data);
              } else {
                cb('error', null);
              }


            });
          });

        });


        // var itemTypes = server.models.ItemType;
        //   itemTypes.findOne({'where' : {'itemTypeId' : obj.itemTypeId}}, function(itemTypeErr, itemTypeOut) {


        //   var transactions = server.models.Transactions;




        // });

      };


    }
    else {
      cb('error', null);
    }



  };

  Item.remoteMethod("itemSearchReport", {
    description: "To itemSearchReport",
    returns: {
      type: "object",
      root: true
    },
    accepts: [{
      arg: "data",
      type: "object",
      required: true,
      http: {
        source: "body"
      }
    }],
    http: {
      path: "/itemSearchReport",
      verb: "POST"
    }
  });




  function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
    }
    return newArray;
  }

};
