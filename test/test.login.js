'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

describe(' Loading the website :', function () {

    // Companies Method 
    it('Companies GET returns 200', function (done) {

        var url = 'http://localhost:3010/api/Companies';
        var request = require('request');

        request({
            url: url,
            method: 'GET',
            json: true,


        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    

    // Login Method 
    it('Login POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/login';
        var request = require('request');
        var input = {"companyId":73,"username":"azzukb","password":"123456"};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input


        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // Forgot Password

    it('forgotPassword POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/forgotPassword';
        var request = require('request');
        var input ={"companyId":73,"username":"azzukb"}
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input


        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
});