'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = require('chai').expect;
chai.use(chaiHttp);

describe(' My Dashboard Page  :', function () {
    // getItemReceivedCount Method 
    it('getItemReceivedCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/getItemReceivedCount';
        var request = require('request');
        var input = {"userId":8096727};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getItemsOwnedCount
    it('getItemsOwnedCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsOwnedCount';
        var request = require('request');
        var input = {"userId":8096727};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

     //getItemSentCount
     it('getItemSentCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/getItemSentCount';
        var request = require('request');
        var input = {"userId":8096727};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // getListOfUsers

    it('getListOfUsers POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/getListOfUsers';
        var request = require('request');
        var input = {"companyId":73};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemsOwned

    it('getItemsOwned POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsOwned';
        var request = require('request');
        var input = {"searchQuery":{},"companyId":73,"userId":8096727,"skip":0,"limit":7};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //searchItemReceived
    it('searchItemReceived POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/searchItemReceived';
        var request = require('request');
        var input = {"userId":8096727,"skip":0,"limit":7,"text":"833-1807928","feild":"itemUniqueId"};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // getItemsOwnedsearch

    it('getItemsOwnedsearch POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsOwnedsearch';
        var request = require('request');
        var input = {"userId":8096727,"skip":0,"limit":7,"text":"833-1807928","feild":"itemUniqueId"};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemSentsearch
    it('getItemSentsearch POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/getItemSentsearch';
        var request = require('request');
        var input = {"userId":8096727,"skip":0,"limit":7,"text":"833-1807928","feild":"itemUniqueId"};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //sendItemTransaction

    // it('sendItemTransaction POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/sendItemTransaction';
    //     var request = require('request');
    //     var input = {"reciever":8096729,"sender":8096727,"nextTransactionId":"","transStatusId":1,"itemIds":["833-1807920"],"transMailFlag":0,"deletedFlag":0,"addedByAdminFlag":0,"transComments":["Transaction during item status change by admin."],"companyId":73,"userId":8096727};

    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    //getItemSent
    it('getItemSent POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/getItemSent';
        var request = require('request');
        var input = {"userId":8096727,"skip":0,"limit":7};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //cancelSentItem
    // it('cancelSentItem POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/cancelSentItem';
    //     var request = require('request');
    //     var input = [];
    //     input = [{"transactionId":9048874,"reciever":8096729,"sender":8096727,"nextTransactionId":0,"transStatusId":1,"transactionTime":"Thu Dec 19 2019 09:20:31 GMT+0530 (India Standard Time)","itemId":9020696,"transMailFlag":0,"deletedFlag":0,"addedByAdminFlag":0,"transSucceedTime":"Thu Dec 19 2019 09:20:31 GMT+0530 (India Standard Time)","transComment":"Transaction during item status change by admin.","senderName":"azzukb","receiverName":"khabul","itemUniqueId":"833-1807920","itemStatusName":"Pending"}]

    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    //acknowledgeRecieveItem

    // it('acknowledgeRecieveItem POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/acknowledgeRecieveItem';
    //     var request = require('request');
    //     var input = [];
    //     input = [{"transactionId":9048875,"reciever":8096729,"sender":8096727,"nextTransactionId":0,"transStatusId":1,"transactionTime":"Thu Dec 19 2019 10:00:52 GMT+0530 (India Standard Time)","itemId":9020696,"transMailFlag":0,"deletedFlag":0,"addedByAdminFlag":0,"transSucceedTime":"Thu Dec 19 2019 10:00:52 GMT+0530 (India Standard Time)","transComment":"Transaction during item status change by admin.","senderName":"azzukb","receiverName":"khabul","itemUniqueId":"833-1807920","itemStatusName":"Ready"}];

    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    //getItemReceived

    it('getItemReceived POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Transactions/getItemReceived';
        var request = require('request');
        var input = {};
        input = {"userId":8096727,"skip":0,"limit":7};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

   
});