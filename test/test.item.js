// 'use strict';
// var chai = require('chai');
// var chaiHttp = require('chai-http');
// var server = require('../server/server');
// var should = chai.should();
// var assert = require('assert');
// var expect = require('chai').expect;
// var request = require('request');
// chai.use(chaiHttp);

// describe(' Items Model Methods :', function () {

//     // getItemList Method to get List of Items assosicated to company
//     it('getItemList POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'skip': 0,
//             'limit': 7,
//             'searchQuery': { "itemUniqueId": "833-1807928" }

//         };

//         var url = 'http://localhost:3010/api/Items/getItems';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemsCount get count of Items for given CompanyId and itemStatusId or currentOwner


//     it('getItemsCount POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'itemStatusId': 2,
//             'userId': 8096727
//         };

//         var url = 'http://localhost:3010/api/Items/getItemsCount';
//         var request = require('request');



//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //binderInventoryReport for given companyId and itemUniqueId

//     it('binderInventoryReport POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'itemUniqueId': "833-1807928"
//         };

//         var url = 'http://localhost:3010/api/Items/binderInventoryReport';
//         var request = require('request');



//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //createItem for entered uniqueId's

//     // createItem for generated uniqueId's

//     // getItemsOwned for given userId and CompanyId
//     it('getItemsOwned POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'userId': 8096727,
//             'skip': 0,
//             'limit': 7
//         };

//         var url = 'http://localhost:3010/api/Items/getItemsOwned';
//         var request = require('request');



//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemsOwnedCount for given userId

//     it('getItemsOwnedCount POST returns 200', function (done) {
//         var input = {
//             'userId': 8096727,
//         };

//         var url = 'http://localhost:3010/api/Items/getItemsOwnedCount';
//         var request = require('request');



//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemsOwnedsearch
//     //  insert proper inputs
//     it('getItemsOwnedsearch POST returns 200', function (done) {
//         var input = {
//             'userId': 8096727,
//             'text': 'OPO',
//             'feild': 78
//         };

//         var url = 'http://localhost:3010/api/Items/getItemsOwnedsearch';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //findUniqueItems  for given itemUniqueId and userId
//     it('findUniqueItems POST returns 200', function (done) {
//         var input = {
//             'userId': 8096727,
//             'itemUniqueId': '833-1807928'
//         };

//         var url = 'http://localhost:3010/api/Items/findUniqueItems';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // findUniqueItemsByCompany for given companyId and itemUniqueId

//     it('findUniqueItemsByCompany POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'itemUniqueId': '833-1807928'
//         };

//         var url = 'http://localhost:3010/api/Items/findUniqueItemsByCompany';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //getItemsBasedonFilter for given companyId and itemStatusId or current owner

//     it('getItemsBasedonFilter POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'currentOwner': 8096727,
//             'itemStatusId': 2
//         };

//         var url = 'http://localhost:3010/api/Items/getItemsBasedonFilter';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // itemsReport for given itemUniqueId or itemTypeId

//     it('itemsReport POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'itemUniqueId': '833-1807928',
//             'itemTypeId': 2
//         };

//         var url = 'http://localhost:3010/api/Items/itemsReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });
//     //itemsReportconut for given companyId and itemTypeId

//     it('itemsReportconut POST returns 200', function (done) {
//         var input = {
//             'companyId': 73,
//             'itemTypeId': 2
//         };

//         var url = 'http://localhost:3010/api/Items/itemsReportconut';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // userTrackingReport for given userId

//     it('userTrackingReport POST returns 200', function (done) {
//         var input = {
//             'userId': 8096727
//         };

//         var url = 'http://localhost:3010/api/Items/userTrackingReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // itemsSentReport for given userId
//     // Test for given fromDate and toDate

//     it('itemsSentReport POST returns 200', function (done) {
//         var input = {
//             'userId': 8096727
//         };

//         var url = 'http://localhost:3010/api/Items/itemsSentReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //  timeHeldReport for given itemUniqueId
//     // check also for currentOwner and itemTypeId

//  it('timeHeldReport POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73
//         };

//         var url = 'http://localhost:3010/api/Items/timeHeldReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //itemSearchReport for given itemUniqueId and companyId
//         // also test for itemTypeId
//     it('itemSearchReport POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73
//         };

//         var url = 'http://localhost:3010/api/Items/itemSearchReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // itemCloseFilterReport for given itemUniqueId
//     // also check for itemTypeId and fromDate or toDate
//     it('itemCloseFilterReport POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73
//         };

//         var url = 'http://localhost:3010/api/Items/itemCloseFilterReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // itemsReport for given itemUniqueId and companyId
//     // also check for itemTypeId

//     it('itemsReport POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73
//         };

//         var url = 'http://localhost:3010/api/Items/itemsReport';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // findexistance for given companyId and itemUniqueId
//     it('findexistance POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73
//         };

//         var url = 'http://localhost:3010/api/Items/findexistance';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // validateitems for given companyId , itemUniqueId and itemTypeId

//     it('validateitems POST returns 200', function (done) {
//         var input = {
//             'itemUniqueId': "833-1807928",
//             "companyId" : 73,
//             'itemTypeId' : 2
//         };

//         var url = 'http://localhost:3010/api/Items/validateitems';
//         var request = require('request');
//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });
// // validateItemId

// it('validateItemId POST returns 200', function (done) {
//     var input = {
//         'itemUniqueId': "833-1807928",
//         "companyId" : 73        
//     };

//     var url = 'http://localhost:3010/api/Items/validateItemId';
//     var request = require('request');
//     request({
//         url: url,
//         method: 'POST',
//         json: true,
//         body: input,

//     }, function (err, response, body) {
//         if (err) {
//             return err;
//         }
//         console.log('response.statusCode', response.statusCode);
//         expect(response.statusCode).to.equal(200);

//         done();
//     });
// });
// });