// 'use strict';
// var chai = require('chai');
// var chaiHttp = require('chai-http');
// var server = require('../server/server');
// var should = chai.should();
// var assert = require('assert');
// var expect = require('chai').expect;
// var request = require('request');
// chai.use(chaiHttp);

// describe(' Transactions Model Methods :', function () {

//     //sendItemTransaction for given sender and reciever
//     it('sendItemTransaction POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'itemIds': ['833-1807928'],
//             'reciever': 2763,
//             'sender': 2763,
//             'addedByAdminFlag' : 1

//         };

//         var url = 'http://localhost:3000/api/Transactions/sendItemTransaction';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemReceived for given userId

//     it('getItemReceived POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//             'skip': 0,
//             'limit': 7,

//         };

//         var url = 'http://localhost:3000/api/Transactions/getItemReceived';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemReceivedCount for given userId

//     it('getItemReceivedCount POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//         };

//         var url = 'http://localhost:3000/api/Transactions/getItemReceivedCount';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemSent for given userId

//     it('getItemSent POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//             'skip': 0,
//             'limit': 7
//         };

//         var url = 'http://localhost:3000/api/Transactions/getItemSent';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemSentsearch for given userId

//     it('getItemSentsearch POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//             'text': 'OPO',
//             'feild': 78
//         };

//         var url = 'http://localhost:3000/api/Transactions/getItemSentsearch';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     // getItemSentCount for given userId

//     it('getItemSentCount POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//         };

//         var url = 'http://localhost:3000/api/Transactions/getItemSentCount';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });
//     //cancelSentItem
//     //acknowledgeRecieveItem
//     //searchItemReceived

//     it('searchItemReceived POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//             'text': 'OPO',
//             'feild': 78
//         };

//         var url = 'http://localhost:3000/api/Transactions/searchItemReceived';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //closeSelectedItems

//     //checkoutSelectedItems
//     it('checkoutSelectedItems POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'itemIds': ['833-1807928'],
//             'reciever': 2763,
//             'sender': 2763,
//             'addedByAdminFlag' : 1
//         };

//         var url = 'http://localhost:3000/api/Transactions/checkoutSelectedItems';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

//     //assignItemTransaction
//     //changeItemStatusTransaction
//     //GetTrancationsReports
// // check for from and to Date
//     it('GetTrancationsReports POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//         };

//         var url = 'http://localhost:3000/api/Transactions/GetTrancationsReports';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });


//     // createTransaction
//     //getdatausingtagid

//     it('getdatausingtagid POST returns 200', function (done) {

//         var input = {
//             'companyId': 73,
//             'userId': 2763,
//             'tagId' : 73,
            
//         };

//         var url = 'http://localhost:3000/api/Transactions/getdatausingtagid';
//         var request = require('request');

//         request({
//             url: url,
//             method: 'POST',
//             json: true,
//             body: input,

//         }, function (err, response, body) {
//             if (err) {
//                 return err;
//             }
//             console.log('response.statusCode', response.statusCode);
//             expect(response.statusCode).to.equal(200);

//             done();
//         });
//     });

// });