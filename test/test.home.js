'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

describe(' Home Page  :', function () {
    // getLocationOrUserCode Method 
    it('getCompanyMessages POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Messages/getCompanyMessages';
        var request = require('request');
        var input = {"companyId":73,"status":1};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemsCount
    it('getItemsCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsCount';
        var request = require('request');
        var input = {"companyId":73};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
});