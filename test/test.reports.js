'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = require('chai').expect;
chai.use(chaiHttp);
describe(' Reports Module  :', function () {
    it('itemTypeCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/ItemTypes/itemTypeCount';
        var request = require('request');
        var input = { "companyId": 73 };
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //itemTypeReport
    it('itemTypeReport POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/ItemTypes/itemTypeReport';
        var request = require('request');
        var input = { "companyId": 73, "skip": 0, "limit": 7 };
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //itemsReportconut for given companyId and itemTypeId

    it('itemsReportconut POST returns 200', function (done) {
        var input = { "companyId": 73, "itemTypeId": 274 };

        var url = 'http://localhost:3010/api/Items/itemsReportconut';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // itemsReport for given itemUniqueId or itemTypeId

    it('itemsReport POST returns 200', function (done) {
        var input = {"companyId":73,"itemTypeId":274,"skip":0,"limit":7};
        var url = 'http://localhost:3010/api/Items/itemsReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //mailRoomReport
    it('mailRoomReport POST returns 200', function (done) {
        var input = {"fromDate":"2019-12-01","toDate":"2019-12-31","userId":8096727,"companyId":73};
        var url = 'http://localhost:3010/api/ItemTypes/mailRoomReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //GetTrancationsReports

    it('GetTrancationsReports POST returns 200', function (done) {
        var input = {"UserId":8096729};
        var url = 'http://localhost:3010/api/Transactions/GetTrancationsReports';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //userTrackingReport

    it('userTrackingReport POST returns 200', function (done) {
        var input = {"userId":"8096729"};
        var url = 'http://localhost:3010/api/Items/userTrackingReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //itemsReport
    it('itemsReport POST returns 200', function (done) {
        var input = {"companyId":73,"itemUniqueId":"833-1807923","itemTypeId":"274"};
        var url = 'http://localhost:3010/api/Items/itemsReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    // itemCloseFilterReport
    it('itemCloseFilterReport POST returns 200', function (done) {
        var input = {"companyId":73,"itemUniqueId":"833-1807920","itemTypeId":"274"};
        var url = 'http://localhost:3010/api/Items/itemCloseFilterReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //itemSearchReport
    it('itemSearchReport POST returns 200', function (done) {
        var input = {"itemUniqueId":"833-1807928","companyId":73,"itemTypeId":"274"};
        var url = 'http://localhost:3010/api/Items/itemSearchReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //timeHeldReport
    it('timeHeldReport POST returns 200', function (done) {
        var input = {"itemUniqueId":"833-1807928","companyId":73,"itemStatusId":2,"userId":8096729,"itemTypeId":"274"};
        var url = 'http://localhost:3010/api/Items/timeHeldReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //itemsSentReport
    it('itemsSentReport POST returns 200', function (done) {
        var input = {"userId":8096727};
        var url = 'http://localhost:3010/api/Items/itemsSentReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

});