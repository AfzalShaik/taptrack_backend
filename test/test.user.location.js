'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

describe(' User/Location BarCode :', function () {

    // getLocationOrUserCode Method 
    it('GetLocationBarcodes POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/getLocationOrUserCode';
        var request = require('request');
        var input = {"uniqueId":"4937421","locationFlag":1};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // getLocationOrUserCode Method 
    it('GetUserBarcodes POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/getLocationOrUserCode';
        var request = require('request');
        var input = {"uniqueId":"4937421","locationFlag":0,"companyId":73}

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
});