'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

describe(' Easy Scanning  :', function () {
    // getLocationOrUserCode Method 
    it('getUserName POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/getUserName';
        var request = require('request');
        var input = { "username": "azzukb", "uniqueId": "4937421" };

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // Transactions/createTransaction Method 
    // it('createTransaction POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/createTransaction';
    //     var request = require('request');
    //     var input = { "reciever": 8096729, "sender": 8096727, "nextTransactionId": "", "transStatusId": 1, "itemIds": ["833-1807928"], "transMailFlag": 0, "deletedFlag": 0, "addedByAdminFlag": 1, "companyId": 73 };

    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });
});