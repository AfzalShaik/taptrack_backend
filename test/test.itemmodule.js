'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = require('chai').expect;
chai.use(chaiHttp);

describe(' Items Module  :', function () {
  
    //ItemTypes

    it('ItemTypes GET returns 200', function (done) {

        var url = 'http://localhost:3010/api/ItemTypes';
        var request = require('request');
        request({
            url: url,
            method: 'GET',
            json: true
           
        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemType

    it('ItemTypes POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/ItemTypes/getItemType';
        var request = require('request');
        var input ={"companyId":73};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // //itemValidation
    // it('itemValidation POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Companies/itemValidation';
    //     var request = require('request');
    //     var input ={"itemUniqueId":"833-1807921","companyId":73};
    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    //createItem

    // it('createItem POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Items/createItem';
    //     var request = require('request');
    //     var input ={"tagName":"OFFH","tagDesc":"ODP","one":"833-1807922","regexFormat":"999-9999999","itemType":274,"companyId":73,"userId":8096727,"itemArray":["833-1807922"]};
    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    //getItems
    it('getItems POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItems';
        var request = require('request');
        var input ={"companyId":73,"itemStatusId":1,"skip":0,"limit":7,"searchQuery":{"itemUniqueId":{"type":"startsWith","field":"itemUniqueId"},"tagName":{"type":"startsWith","field":"tagName"},"tagDesc":{"type":"startsWith","field":"tagDesc"}}};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //checkoutSelectedItems

    // it('checkoutSelectedItems POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/checkoutSelectedItems';
    //     var request = require('request');
    //     var input ={"nextTransactionId":"","transStatusId":1,"itemIds":["833-1807922"],"transMailFlag":0,"deletedFlag":0,"addedByAdminFlag":0,"transComment":"string"};
    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
        
    // });
    //closeSelectedItems
    // it('closeSelectedItems POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Transactions/closeSelectedItems';
    //     var request = require('request');
    //     var input ={"nextTransactionId":"","transStatusId":1,"itemIds":["833-1807922"],"transMailFlag":0,"deletedFlag":0,"addedByAdminFlag":0,"transComment":"string"};
    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input,

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
        
    // });

    //getCheckoutItems

    it('getCheckoutItems POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getCheckoutItems';
        var request = require('request');
        var input ={"companyId":73,"skip":0,"limit":7,"itemStatusId":1,"searchQuery":{"itemUniqId":""}};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
        
    });

    //getItemsCount
    it('getItemsCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsCount';
        var request = require('request');
        var input ={"companyId":73,"itemStatusId":1};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
        
    });

    

    //getItemsBasedonFilterCount
    it('getItemsBasedonFilterCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsBasedonFilterCount';
        var request = require('request');
        var input = {};
        input = {"companyId":73,"itemStatusId":"All"};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemStatusList
    it('getItemStatusList POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/ItemStatuses/getItemStatusList';
        var request = require('request');
        var input = {};
        input = {"type":""};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    //getItemsBasedonFilter
    it('getItemsBasedonFilter POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/getItemsBasedonFilter';
        var request = require('request');
        var input = {};
        input = {"companyId":73,"itemStatusId":"All","skip":0,"limit":7};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //findUniqueItemsByCompany
    it('findUniqueItemsByCompany POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Items/findUniqueItemsByCompany';
        var request = require('request');
        var input = {};
        input = {"itemUniqueId":"833-1807928","userId":8096727,"companyId":73};

        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    

});
