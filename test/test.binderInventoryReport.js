'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);


describe(' Binder Inventory Report :', function () {

    // getCompanyDetails Method 
    it('getCompanyDetails POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Companies/getCompanyDetails';
        var request = require('request');
        var input = {"uniqueId":"4937421"};
        request({
            url: url,
            method: 'POST',
            json: true,
            body : input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

    // findexistance for given companyId and itemUniqueId
    it('findexistance POST returns 200', function (done) {
        var input = {
            'itemUniqueId': "833-1807928",
            "companyId" : 73
        };

        var url = 'http://localhost:3010/api/Items/findexistance';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

        //binderInventoryReport for given companyId and itemUniqueId

    it('binderInventoryReport POST returns 200', function (done) {
        var input = {
            'companyId': 73,
            'itemUniqueId': "833-1807928"
        };

        var url = 'http://localhost:3010/api/Items/binderInventoryReport';
        var request = require('request');
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input,

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
});