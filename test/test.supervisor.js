'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = require('chai').expect;
chai.use(chaiHttp);
describe(' Supervisor Module  :', function () {
    it('poendinggetusersCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/poendinggetusersCount';
        var request = require('request');
        var input = {"companyId":73};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getpendingusers
    it('getpendingusers POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/getpendingusers';
        var request = require('request');
        var input = {"companyId":73,"skip":0,"limit":7};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getscarchuserspending
    it('getscarchuserspending POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/getscarchuserspending';
        var request = require('request');
        var input = {"companyId":73,"itemStatusId":1,"skip":0,"limit":7,"searchQuery":{"username":{"type":"startsWith","field":"username","value":"ameena"},"userFirstName":{"type":"startsWith","field":"userFirstName"},"userLastName":{"type":"startsWith","field":"userLastName"}}};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getusersCount
    it('getusersCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/getusersCount';
        var request = require('request');
        var input = {"companyId":73};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getusers
    it('getusers POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/getusers';
        var request = require('request');
        var input = {"companyId":73,"skip":0,"limit":7};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getscarchusers
    it('getscarchusers POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes/getscarchusers';
        var request = require('request');
        var input = {"companyId":73,"skip":0,"limit":7,"searchQuery":{"username":{"type":"startsWith","field":"username","value":"azzukb"},"userFirstName":{"type":"startsWith","field":"userFirstName"},"userLastName":{"type":"startsWith","field":"userLastName"}}};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //inviteEmail
    it('inviteEmail POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Users/inviteEmail';
        var request = require('request');
        var input = {"toWhom":"afzal.shaik@yitsol.com","userId":8096729};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //getCompanyMessages
    it('getCompanyMessages POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Messages/getCompanyMessages';
        var request = require('request');
        var input = {"companyId":73,"skip":0,"limit":7};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
     //getCompanyMessagesCount
     it('getCompanyMessagesCount POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Messages/getCompanyMessagesCount';
        var request = require('request');
        var input = {"companyId":73,"status":1};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    //Messages
    it('Messages POST returns 200', function (done) {

        var url = 'http://localhost:3010/api/Messages';
        var request = require('request');
        var input = {"messageName":"Intelligence Eng","messageDesc":"Test","messageType":"","companyId":73,"userId":8096727,"superAdminId":1};
        request({
            url: url,
            method: 'POST',
            json: true,
            body: input

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });

});