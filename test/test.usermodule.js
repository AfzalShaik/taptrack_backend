'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = require('chai').expect;
chai.use(chaiHttp);
describe(' Users Module  :', function () {
    //UserTypes
    it('UserTypes GET returns 200', function (done) {

        var url = 'http://localhost:3010/api/UserTypes';
        var request = require('request');
        request({
            url: url,
            method: 'GET',
            json: true

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
//States
    it('States GET returns 200', function (done) {

        var url = 'http://localhost:3010/api/States';
        var request = require('request');
        request({
            url: url,
            method: 'GET',
            json: true

        }, function (err, response, body) {
            if (err) {
                return err;
            }
            expect(response.statusCode).to.equal(200);

            done();
        });
    });
    // Create Users
    // it('Users POST returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Users';
    //     var request = require('request');
    //     var input = {"companyName":"HUD Atanta HOC","uniqueId":"4937421","username":"ameenaa","password":"123456","confirmPassword":"123456","userTypeId":"1","userFirstName":"Ameena","userLastName":"Shaik","UserStreet1":"Hyderabad","userCity":"Hyderabad","stateId":"6","userZip":"98239","email":"afzal.shaik@yitsol.com","userMobile":"8383838383","companyId":73,"toolTipFlag":0,"userApproved":0,"userMI":"","userPrefix":"","userStatus":0,"locationFlag":0,"userType":"Admin"};
    //     request({
    //         url: url,
    //         method: 'POST',
    //         json: true,
    //         body: input

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });

    // Update Users
    // it('Users PATCH returns 200', function (done) {

    //     var url = 'http://localhost:3010/api/Users';
    //     var request = require('request');
    //     var input = {"id":8096727,"companyId":73,"location":"hyd","stateId":1,"toolTipFlag":1,"userApproved":1,"userCity":"hyd","email":"afzal@gmail.com","userFirstName":"azzu","userLastName":"kb","userMI":"az","userMobile":"8877887788","userPrefix":"1","userStatus":1,"UserStreet1":"ps","UserStreet2":"ps","UserStreet3":"pss","userTypeId":1,"userZip":5544,"locationFlag":0,"realm":null,"username":"azzukb","emailVerified":null,"companyName":"HUD Atanta HOC","userType":"Admin","password":"123456","confirmPassword":"123456"};
    //     request({
    //         url: url,
    //         method: 'PATCH',
    //         json: true,
    //         body: input

    //     }, function (err, response, body) {
    //         if (err) {
    //             return err;
    //         }
    //         expect(response.statusCode).to.equal(200);

    //         done();
    //     });
    // });
});
