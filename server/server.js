/**
*-------------------------------------------------------------
* Copyright (c) 2019 YITSOL.
* All Rights Reserved
* --------------------------------------------------------------
*/
/**
* @FileName : server.js
* @Module : ITI_OID_Services 
* @CreationDate :  7 Mar 19
* @Description : Loopback Services, Server.Js.
* @Author : YITSOL
* @ModifiedBy : YITSOL
* @Version : 1.0
*/


'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');
var app = module.exports = loopback();
var path = require('path');
var cors = require('cors');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:1234');
//   res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });
app.use(cors());
app.start = function () {
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      // var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s', baseUrl);
    }
  });
};


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;
  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
//From Nest
var http = require('http');
var https = require('https');
var path = require('path');

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var session = require('express-session');
var openurl = require('openurl');
const helmet = require('helmet');
// Change for production apps.
// This secret is used to sign session ID cookies.
var SUPER_SECRET_KEY = 'keyboard-cat';





//var app = express();
app.use(helmet());
app.use(cookieParser(SUPER_SECRET_KEY));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
  secret: SUPER_SECRET_KEY,
  resave: false,
  saveUninitialized: false
}));

// app.use(express.static(path.join(__dirname, 'public')));







