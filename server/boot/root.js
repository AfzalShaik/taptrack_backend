'use strict';

module.exports = function (server) {
  //show password reset form
  var ttUser = server.models.Users;
  var router = server.loopback.Router();
  var bodyParser = require('body-parser');
  const fs = require("fs");
  const path = require("path");
  var winston = require('../../winstonsockets');
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(bodyParser.json())

//get User model from the express app
var UserModel = server.models.Users;

router.post('/api/login', function (req, res) {		

  //parse user credentials from request body
  const userCredentials = {
    "username": req.body.username,
    "companyId" : req.body.companyId,
    "password": req.body.password
  }
    UserModel.findOne({ 'where': {'and' : [{ 'username': req.body.username }, { 'companyId': req.body.companyId },{'password': req.body.password},{locationFlag : 0},{userApproved : 1},{userStatus : 1}]}, 'include': ['companyIdRelations'] }, function (userErr, userOut) {
      if (userErr) {
        winston.GenerateLog(0, 'Login Failed');
        res.status(401).json({"error": "login failed"});
        return;
      } else if(userOut) {
        server.models.AccessToken.create({userId:userOut.id}, function(err, resp) {
          winston.GenerateLog(2, 'User Logged In with Id:' + userOut.id);
          res.json({
            "id": resp.id,
            "ttl": resp.ttl,
            "created" : new Date(),
            "userId" : userOut.id,
            "userDetails" : userOut
          });
        })
      } else {
        winston.GenerateLog(0, 'Login Failed');
        res.status(401).json({"error": "login failed"});
      }
      
  })

    //transform response to only return the token and ttl
   
  // });
});


  server.use(router);
};