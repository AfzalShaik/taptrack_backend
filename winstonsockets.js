module.exports = {
  GenerateLog: function (Levels, Messages) {
    var winston = require('winston');
    require('winston-daily-rotate-file');
    // const logger = winston.createLogger({
    //   transports: [
    //     new winston.transports.Console(),
    //     new winston.transports.File({ filename: '../logs/WinstonLogs.log' , maxsize: 10000000})
    //   ]
    // });

    path = require('path'),
      transports = []

    transports.push(
      new winston.transports.DailyRotateFile({
        name: 'taptrack.%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        filename : './logs/taptrack.%DATE%.log',
        // filename: path.join(__dirname, 'logs', 'taptrack.%DATE%.log'),
        zippedArchive: true,
        maxSize: '20m',
        // maxFiles: '14d'
      })
    )
    new winston.transports.Console()
    var logger = winston.createLogger({ transports: transports })

    if (Levels == 0) {
      logger.error({
        'time': new Date(),
        'error': Messages
      });
    }
    if (Levels == 2) {
      logger.info({
        'time': new Date(),
        'message': Messages
      });
    }


  }
}
